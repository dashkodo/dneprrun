{strip}
	<h1>{$item.name}</h1>
	{* Хлебные крошки *}
	{include file="breadcrumbs.tpl"}
	{* Контент *}
	<div{if $query} class="full_item"{/if}>
		{$item.full_cont}
	</div>
{/strip}