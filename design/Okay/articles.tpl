﻿{strip}
{* Заголовок *}
<h1>{$name}</h1>
{* Хлебные крошки *}
{include file="breadcrumbs.tpl"}
{* Подробное описание элемента *}
{if $item}
<strong class="datetime">{$item.datetime}</strong>
<div class="full_item">
  {$item.full_cont}
  {if $item.foreign_register}
  <div class="clear"></div>
  <a href="{$item.foreign_register}" target="_blank" class="button fleft">Зарегистрироваться</a>
  <div class="clear"></div>
  {/if}
</div>
{include file="articles/results.tpl" scope=parent}

{if $item.register}
{include file="articles/register.tpl" scope=parent}
{/if}
{include file="articles/members.tpl" scope=parent}


{else}
{if empty($queries[1])}
<div class="full_item">
  {$module.full_cont}
</div>
{/if}
{* Краткие описания элементов *}
{if count($items)}
<div class="short_items">
  {foreach $items as $item}
  <div class="short_item{if !$item.passed} future{/if}">
    {if $item.pic}
    <a class="pic" href="{$host}{$language_prefix}{$module.frontend}/{$item.url}">
      <div class="table">
        <div class="tc">
          <img src="{$item.pic}" alt="{$item.name}">
        </div>
      </div>
    </a>
    {/if}
    <div>
      <a class="link" href="{$host}{$language_prefix}{$module.frontend}/{$item.url}">{$item.name}</a>
      {$item.short_cont}
    </div>
    <div class="clear"></div>
  </div>
  {/foreach}
</div>
{* Пагинация *}
{include file="paginator.tpl"}
{else}
<center>{$l.articles2}</center>
{/if}
{/if}
{/strip}