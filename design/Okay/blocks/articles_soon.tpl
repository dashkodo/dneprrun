{if $items_soon}
    {foreach $items_soon as $item_soon}
	<div class="soon_event">
		<a href="{$host}{$language_prefix}{$run_block.frontend}/{$item_soon.url}"><h3>{$item_soon.name}</h3></a>
		<strong class="datetime">{$item_soon.datetime}</strong>
		{if $item_soon.pic}
			<div class="image">
				<img src="{$item_soon.pic}" alt="$item_soon.name">
			</div>
		{/if}
		{$item_soon.short_cont}
        {if $item_soon.foreign_register}
            <div class="clear"></div>
			<a href="{$item_soon.foreign_register}" target="_blank" class="button fleft">Зарегистрироваться</a>
		{/if}
		<div class="clear"></div>
		{if $item_soon.register}
			<a href="{$host}{$language_prefix}{$run_block.frontend}/{$item_soon.url}#register" class="button fleft">Зарегистрироваться</a>
		{/if}
		<div class="clear"></div>
	</div>
    {/foreach}
{else}
	Ожидаются<br />
{/if}
{*if $item_soon}
	<div class="soon_event">
		<a href="{$host}{$language_prefix}{$run_block.frontend}/{$item_soon.url}"><h3>{$item_soon.name}</h3></a>
		<strong class="datetime">{$item_soon.datetime}</strong>
		{if $item_soon.pic}
			<div class="image">
				<img src="{$item_soon.pic}" alt="$item_soon.name">
			</div>
		{/if}
		{$item_soon.short_cont}
        {if $item_soon.foreign_register}
            <div class="clear"></div>
			<a href="{$item_soon.foreign_register}" target="_blank" class="button fleft">Зарегистрироваться</a>
		{/if}
		<div class="clear"></div>
		{if $item_soon.register}
			<a href="{$host}{$language_prefix}{$run_block.frontend}/{$item_soon.url}#register" class="button fleft">Зарегистрироваться</a>
		{/if}
		<div class="clear"></div>
	</div>
{else}
	Ожидаются
{/if*}