<div class="block">
	{foreach $block_items as $block_item}
    	<div class="news_item{if $block_item@last} no_bord{/if}">
			<div class="datetime">{$block_item.datetime}</div>
			<a href="{$host}{$language_prefix}{$run_block.frontend}/{$block_item.url}">{$block_item.name}</a>
        </div>
	{/foreach}
	<a class="all" href="{$host}{$language_prefix}{$run_block.frontend}">{$l.news1}</a>
</div>