<div class="block">
	{foreach $block_items as $block_item}
    	<div class="news_item no_bord">
			<a href="{$host}{$language_prefix}{$run_block.frontend}/{$block_item.url}">{$block_item.name}</a>
        </div>
	{/foreach}
</div>