{strip}
	<h1>{$name}</h1>
	{if count($cats)}
        <div class="links">
            {function name=categories_tree}
        		{if $cats}
            		<ul>
                		{foreach $cats as $c}
            				<li>
            					<a href="#" data-category="{$c.id}" class="show_subs">{$c.name|escape}</a>
            					{categories_tree cats=$c.subcategories}
                                {if $c.items}
                                    <ul class="links_items">
                                        {foreach $c.items as $i}
                                            <li>
                                                <a href="{$i.url}" target="_blank">{$i.name}</a>
                                            </li>
                                        {/foreach}
                                    </ul>
                                {/if}
            				</li>
                		{/foreach}
            		</ul>
        		{/if}
    		{/function}
    		{categories_tree cats=$cats}
        </div>
	{/if}
    <br />
    {$module.full_cont}
{/strip}