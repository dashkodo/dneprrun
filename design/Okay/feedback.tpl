{strip}
	{* Заголовок *}
	<h1>{$module.name}</h1>
	{* Хлебные крошки *}
	{include file="breadcrumbs.tpl"}
	{* Подробное описание модуля *}
    <div class="full_item feed">
        {if $module.full_cont}
            <div>{$module.full_cont}</div>
            <div class="clear" style="height:5px"></div>
        {/if}
        {if $module.settings.ph1 || $module.settings.ph2 || $module.settings.ph3}
            <div style="float:left;margin-right:5px;">Контактный телефон:</div>
            <div style="overflow:hidden">
                {if $module.settings.ph1}
                    <div><b>+38 {$module.settings.ph1_code} {$module.settings.ph1}</b></div>
                {/if}
                {if $module.settings.ph2}
                    <div><b>+38 {$module.settings.ph2_code} {$module.settings.ph2}</b></div>
                {/if}
                {if $module.settings.ph3}
                    <div><b>+38 {$module.settings.ph3_code} {$module.settings.ph3}</b></div>
                {/if}
            </div>
            <div class="clear" style="height:5px"></div>
        {/if}
        {if $module.settings.mail}
            <div>e-mail: <a href="mailto:{$module.settings.mail}">{$module.settings.mail}</a></div>
            <div class="clear" style="height:5px"></div>
        {/if}
    </div>
	<form method="post" class="feedback">
		<strong>{$l.feedback1}</strong>
        <div class="req_txt">{$l.feedback2}</div>
        
		{if $ok}<div class="ok">{$l.feedback3}</div>{/if}
        
		<div>{$l.feedback4} <span>*</span></div>
		<input name="feedback[f_1]" type="text" value="{$feedback.f_1}" data-required="name">
        
		<div>{$l.feedback5}</div>
		<input name="feedback[f_2]" type="text" value="{$feedback.f_2}">
        
        <div>{$l.feedback6}</div>
		<input name="feedback[f_3]" type="text" value="{$feedback.f_3}">
        
        <div>{$l.feedback7} <span>*</span></div>
		<input name="feedback[f_4]" type="text" value="{$feedback.f_4}" data-required="mail">
        
		<div>{$l.feedback8} <span>*</span></div>
		<textarea spellcheck="false" name="feedback[f_5]" data-required="text">{$feedback.f_3}</textarea>
        
		{if $settings.captcha}
        	<img class="captcha" src="{$host}captcha.jpg" align="middle">
			<div class="captcha">{$l.feedback9} <span>*</span></div>
			<input type="text"{if !$captcha} class="error"{/if}name="captcha" data-required="captcha">
            <div class="clear"></div>
		{/if}
        <div class="red"{if !$captcha} style="display: block"{/if}>{$l.feedback11}</div>
		<input type="submit" class="button" value="{$l.feedback10}">
	</form>
{/strip}
