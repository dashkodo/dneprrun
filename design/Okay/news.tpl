{strip}
	{* Заголовок *}
	<h1>{$name}</h1>
	{* Хлебные крошки *}
	{include file="breadcrumbs.tpl"}
	{* Подробное описание элемента *}
	{if $item}
		<div class="full_item">
			{$item.full_cont}
		</div>
	{else}
		{* Краткие описания элементов *}
		{if count($items)}
			<div class="short_items">
				{foreach $items as $item}
					<div class="short_item">
                    	{if $item.pic}
							<a class="pic" href="{$host}{$language_prefix}{$module.frontend}/{$item.url}">
                            	<div class="table">
                                    <div class="tc">
                                        <img src="{$item.pic}" alt="{$item.name}">
                                    </div>
                                </div>
							</a>
						{/if}
                        <div>
                            <a class="link" href="{$host}{$language_prefix}{$module.frontend}/{$item.url}">{$item.name}</a>
                            {$item.short_cont}
                        </div>
                        <div class="clear"></div>
					</div>
				{/foreach}
			</div>
			{* Пагинация *}
			{include file="paginator.tpl"}
		{else}
			<center>{$l.news2}</center>
		{/if}
	{/if}
{/strip}