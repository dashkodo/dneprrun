﻿{strip}
<!DOCTYPE html>
<html>
  <head>
    <meta charset="{$smarty.const.CHARSET}">
    <title>{$title}</title>
    {if $desc}
    <meta name="description" content="{$desc}">
    {/if}
    {if $key}
    <meta name="keywords" content="{$key}">
    {/if}
    {if $settings.google_verification}
    <meta name="google-site-verification" content="{$settings.google_verification}">
    {/if}
    {if $settings.yandex_verification}
    <meta name="yandex-verification" content="{$settings.yandex_verification}">
    {/if}
    {include "string:`$settings.google_analytics|strip|regex_replace:'/({|})/s':' \\1 '`"}
    {include "string:`$settings.yandex_metrika|strip|regex_replace:'/({|})/s':' \\1 '`"}
    <meta name="viewport" content="width=device-width">
    <link href="{$design}favicon.ico" rel="shortcut icon">
    <link href='http://fonts.googleapis.com/css?family=Russo+One&subset=latin,cyrillic,latin-ext' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="{$design}style.css">
    <script>
      var host = '{$host}', language_prefix = '{$language_prefix}';
    </script>
    <!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
    <script src="{$design}jquery-1.7.2.min.js"></script>
    <script src="{$design}jquery.easing.1.3.js"></script>
    <script src="{$design}fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
    <script src="{$design}fancybox/jquery.fancybox-1.3.4.pack.js"></script>
    <link rel="stylesheet" href="{$design}fancybox/jquery.fancybox-1.3.4.css" media="screen" />
    <script src="{$design}bxslider/jquery.bxslider.min.js"></script>
    <link rel="stylesheet" href="{$design}bxslider/jquery.bxslider.css" media="screen" />
    <script src="{$design}script.js"></script>
  </head>
  <body>
    <div class="wrapper">
      <header>
        <a class="logo" href="{$host}{$language_prefix}">{* <img src="{$design}img/logo.png" alt="Главная"> *}
          <img src="{$design}img/logo.jpg" alt="{$settings.sitename_ru}" title="{$settings.sitename_ru}" />
        </a>
        {$b3}
      </header>
      <nav>
        <table>
          <tr>
            {foreach $m1 as $menu}
            <td>
              {if $menu.level == 0}
              <div class="fm">
                <a href="{$menu.link}" class="{if $menu@index == 0}first{/if}{if $menu.active} active{/if}">{$menu.name}</a>
                {if $menu.id == 3}
                {$b6}
                {/if}
              </div>
              {/if}
            </td>
            {/foreach}
          </tr>
        </table>
      </nav>
      <div class="banner">
        <img src="{$design}img/banner.jpg" alt="{$title}">
      </div>
      <table class="main">
        <tr>
          {if count($l_blocks)}
          <td class="left">
            {if !$user}
            <div id="sitereg" class="panel panel-info">
              <h3 class="panel-heading">Авторизация</h3>
              <div class="panel-body">
                <div id="uLogin" data-ulogin="display=panel;fields=first_name,last_name,email,photo_big;providers=vkontakte,facebook;hidden=;redirect_uri="></div>
              </div>
            </div>
            {literal}
            <script>$("#uLogin").attr("data-ulogin", $("#uLogin").attr("data-ulogin") +  encodeURIComponent(location.toString()))</script>
            <script src="//ulogin.ru/js/ulogin.js"></script>
            {/literal}

            {else}
            <div class="panel panel-info">
              <h3 class="panel-heading">Профиль</h3>
              <div class="panel-body">
                <div class="profile-userpic">
                  <img src="{$profile['picture']}" class="img-responsive" alt="" />
                </div>


                Добро пожаловать,<br/> {$user['name']} {if $profile['club']} ({$profile['club']}){/if}.
                <form method="post">
                  <input name="exit" type="hidden" value="">
                  <input class="button" value="Выйти" type="submit">
                  <a class="button" href="/profile">Мой профиль</a>
                </form>

              </div>
            </div>

            {/if}

            {foreach $l_blocks as $l_block}
            {if $l_block.disp_name}
            <h3>{$l_block.name}</h3>
            {/if}
            {include "string:`$l_block.full_cont|strip`"}
            {/foreach}
            {literal}

            <script type="text/javascript" src="//vk.com/js/api/openapi.js?116"></script>
            <!-- VK Widget -->
            <script type="text/javascript">
              VK.Widgets.Group("vk_groups", {mode: 0, width: "220", height: "247", color1: 'FFFFFF', color2: '000000', color3: '#05436e'}, 55652394);
            </script>
            {/literal}
          </td>
          {/if}
          <td class="content min-height">
            {foreach $t_blocks as $t_block}
            {if $t_block.disp_name}
            <h3>{$t_block.name}</h3>
            {/if}
            {include "string:`$t_block.full_cont|strip`"}
            {/foreach}
            {if $query || !$items_soon}
            {include file="$tpl"}
            {/if}
            <br /><br />
            {foreach $b_blocks as $b_block}
            {if $b_block.disp_name}
            <h3>{$b_block.name}</h3>
            {/if}
            {include "string:`$b_block.full_cont|strip`"}
            {/foreach}
            &nbsp;
          </td>
          {if count($r_blocks)}
          <td class="right">
            {foreach $r_blocks as $r_block}
            {if $r_block.disp_name}
            <h3>{$r_block.name}</h3>
            {/if}
            {include "string:`$r_block.full_cont|strip`"}
            {/foreach}
          </td>
          {/if}
        </tr>
      </table>
      <footer>
        <div class="copy">
          <b>dneprrun.dp.ua</b>
        </div>
        <div class="fmenu">
          {foreach $m1 as $menu}
          <a href="{$menu.link}"{if $menu.active} class="active"{/if}>{$menu.name}</a>
          {/foreach}
        </div>
        {$b4}
      </footer>
    </div>
   
  </body>
</html>
{/strip}