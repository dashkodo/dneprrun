$(document).ready(function() {
	// Слайдер
	if ($('.slider').size()) {
		$('.slider').bxSlider({
			mode: 'horizontal', // horizontal, vertical, fade
			controls: false,
			pager: true,
			auto: true,
			speed: 700,
			pause: 3000,
			autoHover: true,
			easing: 'easeInOutBack'
		});
	}
	// Всплывашка
	if ($('[rel=fancybox]').size()) {
		$('[rel=fancybox]').fancybox({
			titlePosition: 'outside', // over, outside
			titleFormat: function(title) {
				return '<span id="fancybox-title-over">' + title + '</span>';
			}
		});
	}
	$('form').live('submit', function(e) {
		var $this, $form=$(this), error = false, email_pattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
		$('.red,.member_exist').hide();
		$form.find('[data-required]').each(function() {
			$this = $(this);
			$this.removeClass('error-border');
			switch ($this.attr('data-required')) {
			case 'mail':
				if (!email_pattern.test($this.val().trim())) {
					$this.addClass('error-border');
					error = true;
				}
				break;
			default:
				if (!$this.val().trim()) {
					$this.addClass('error-border');
					error = true;
				}
			}
		});
		if (error) {
			$('.red').slideDown();
			return false;
		} else {
			$('.red').hide();
			if($form.hasClass('ajax')){
                e.preventDefault();
				$.post(window.location, $form.serialize(), function(ans){
				    console.log(ans);
                    if(ans.indexOf('member_exist') != -1){
						$(ans).slideDown(300);
					}else if(ans){
                        $(ans).slideDown(300).delay(5000).slideUp(300,function(){
                            $form.trigger('reset');
                        });
						if($(ans).closest('form').find('.captcha').size()){
                           $(ans).closest('form').find('.captcha').removeClass('active').removeClass('error').children('input').val(''); 
                        }
                    }
                });
				return false;
			}
		}
	});
	
	//dates
	function month(m){
		m = parseInt(m);
		switch(m){
			case 1:
				return 'января';
			case 2:
				return 'февраля';
			case 3:
				return 'марта';
			case 4:
				return 'апреля';
			case 5:
				return 'мая';
			case 6:
				return 'июня';
			case 7:
				return 'июля';
			case 8:
				return 'августа';
			case 9:
				return 'сентября';
			case 10:
				return 'октября';
			case 11:
				return 'ноября';
			case 12:
				return 'декабря';
		}
	}
	
	if($('.datetime').size()){
		var tmp = [], dates = $('.datetime');
		for(var i=0;i<dates.length;i++){
			tmp = $(dates[i]).text().split('-');
			$(dates[i]).html(tmp[0] + '&nbsp;' + month(tmp[1]) + '&nbsp;' + tmp[2]);
		}
	}
    
    $('.event_members li:even').addClass('even');
    $('.show_members').click(function(e){
        e.preventDefault();
        $('.event_members').slideToggle();
    });
	$('.show_volunteers').click(function(e){
        e.preventDefault();
        $('.event_volunteers').slideToggle();
    });
    $('.links .show_subs').live('click',function(){
        $(this).nextAll('ul').slideToggle(300);
        return false;
    });
});


// Name: Footer Down v.0.3
// Autor: Victor Sulimov
// Todo: Fixed height
var footer_down =  function() {
	var main = $('.main');
	var footer = $('footer');
	var min_height = $('.min-height');
	var window_height = $(window).height();
	var main_top = main.position().top;
	var footer_height = footer.outerHeight(true);
	min_height.css('height', window_height - footer_height - main_top);
}
$(document).ready(footer_down);
$(window).load(footer_down);
$(window).resize(footer_down);