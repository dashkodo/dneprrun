{strip}
	{if $breadcrumbs}
		<div class="breadcrumbs">
			<a href="{$host}{$language_prefix}">Главная</a>
			{foreach $breadcrumbs as $breadcrumb}
				{$l.breadcrumbs1}
                {if $breadcrumb@last}
                	<span>{$breadcrumb.name}</span>
                {else}
					<a href="{$host}{$language_prefix}{$breadcrumb.url}">{$breadcrumb.name}</a>
                {/if}
			{/foreach}
		</div>
	{/if}
{/strip}