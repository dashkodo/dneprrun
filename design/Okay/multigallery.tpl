{strip}
	<h1>{$name}</h1>
	{* Хлебные крошки *}
	{include file="breadcrumbs.tpl"}
	{if count($cats)}
		<div class="gallery_items">
			{foreach $cats as $c}
                {if $c@index is div by 3}<div class="clear"></div>{/if}
                <a href="{$host}{$language_prefix}{$module.frontend}/{$c.url}" class="fleft{if $c@index is div by 3} no_marg{/if}">
                    <span class="pic">
                        {if $c.pic}
							<img src="{$c.pic}" alt="{$c.name}">
                        {/if}
                    </span>
                    <span class="link">{$c.name}</span>
                </a>
			{/foreach}
            <div class="clear"></div>
		</div>
	{elseif count($items)}
		<div class="gallery_items">
			{foreach $items as $item}
                {if $item@index is div by 3}<div class="clear"></div>{/if}
                <a href="{$item.opic}" rel="fancybox" class="fleft{if $item@index is div by 3} no_marg{/if}" title="{$item.name}">
                    <span class="pic">
                        <img src="{$item.mpic}" alt="{$item.name}">
                    </span>
                    <span class="link">{$item.name}</span>
                </a>
			{/foreach}
		</div>
		{* Пагинация *}
		{include file="paginator.tpl"}
	{else}
		Раздел пуст
	{/if}
{/strip}