{if count($events)}
{foreach $events as $event}

<div class="event_items">
  <h3>{$event.event.distance}</h3>

  <ul>
    <li>
      <span>ФИО</span>
      <span>Город</span>
      <span>Клуб</span>
      <span>Год рождения</span>
      <span>Результат</span>
    </li>
    {foreach $event.results as $result}
    <li>
      <span>{if $result.member.name}{$result.member.name}{else}{$result.member_mail}{/if}</span>
      <span>{$result.member.city}</span>
      <span>{$result.member.club}</span>
      <span>{if $result.member.born>0}{date('Y',strtotime($result.member.born))}{/if}</span>
      <span>{$result.result}</span>
    </li>
    {/foreach}
  </ul>
</div>
{/foreach}
{/if}
