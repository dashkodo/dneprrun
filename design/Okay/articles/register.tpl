<form method="post" class="feedback register_event ajax" id="register">
  {if !$user}
  <!--
  <a href="#sitereg" class="button">Войти на сайт для регистрации</a>
  -->
  
  <h3>Я хочу участвовать</h3>
  <div class="req_txt">Поля отмеченные
    <span>*</span> обязательны к заполнению!</div>

  <div>Учавствую как
    <span>*</span></div>
  <select name="event[volunteer]" data-required="volunteer">
    <option value="0">Участник</option>
    <option value="1">Волонтер</option>
  </select>
  {if $profile}
  <input name="event[name]" type="hidden" value="{$user.name}" data-required="name">
  <input name="event[mail]" type="text" value="{$user.mail}" data-required="mail">
  {else}
  <div>Ваши ФИО
    <span>*</span></div>
  <input name="event[name]" type="text" value="{$feedback.f_1}" data-required="name">

  <div>Дата рождения
    <span>*</span></div>
  <input name="event[born]" type="text" placeholder="{date('d.m.Y')}" data-required="born">

  <div>Город
    <span>*</span></div>
  <input name="event[city]" type="text" value="{$feedback.f_3}"  data-required="city">

  <div>Клуб</div>
  <input name="event[club]" type="text" value="{$feedback.f_3}">

  <div>Телефон
    <span>*</span></div>
  <input name="event[phone]" type="text" value="{$feedback.f_3}" data-required="phone">

  <div>E-mail
    <span>*</span></div>
  <input name="event[mail]" type="text" value="{$feedback.f_4}" data-required="mail">

  {/if}

  {if $item.distance}

  {if count($item.distance) == 1}
  <input type="hidden" name="event[distance]" value="{$item.distance[0]}" data-required="distance">
  <div>
    Дистанция: {$item.distance[0]}   
  </div> 
  <br/>
  {else}
  <div>
    Дистанция
    <span>
      *
    </span>
  </div>
  <select name="event[distance]" data-required="distance">
    <option value=" ">
      Не выбрана
    </option>
    {foreach $item.distance as $d}
    <option value="{$d}">
      {$d}
    </option>
    {/foreach}
  </select>
  {/if}
  {/if}

  {if $settings.captcha}
  <img class="captcha" src="{$host}captcha.jpg" align="middle">
  <div class="captcha">{$l.feedback9}
    <span>*</span></div>
  <input type="text"{if !$captcha} class="error"{/if}name="captcha" data-required="captcha">
  <div class="clear"></div>
  {/if}
  <div class="red">Заполните верно обязательные поля</div>
  <div class="member_exist">Участник с таким e-mail уже зарегистрирован на это мероприятие</div>
  <div class="success">Вы успешно зарегистрированы на мероприятие</div>
  <input name="event[event_id]" type="hidden" value="{$item.id}">
  <input type="submit" class="button" value="Зарегистрироваться">

  
  {elseif strpos(json_encode($members),$user.mail) !== false}

  <h3>
    Вы уже зарегистрированы на данное мероприятие
  </h3>
  {else}
  <h3>
    Я хочу участвовать
  </h3>
  <div class="req_txt">
    Поля отмеченные
    <span>
      *
    </span>обязательны к заполнению!
  </div>

  <div>
    Учавствую как
    <span>
      *
    </span>
  </div>
  <select name="event[volunteer]" data-required="volunteer">
    <option value="0">
      Участник
    </option>
    <option value="1">
      Волонтер
    </option>
  </select>
  {if $profile}
  <input name="event[name]" type="hidden" value="{$user.name}" data-required="name">
  <input name="event[mail]" type="hidden" value="{$user.mail}" data-required="mail">
  {else}


  <div>
    Ваши ФИО
    <span>
      *
    </span>
  </div>
  <input name="event[name]" type="text" value="{$feedback.f_1}" data-required="name">
  <div>
    E-mail
    <span>
      *
    </span>
  </div>
  <input name="event[mail]" type="text" value="{$feedback.f_4}" data-required="mail">
  {/if}

  <div>
    Дата рождения
    <span>
      *
    </span>
  </div>
  <input name="event[born]" type="text" placeholder="{date('d.m.Y')}" data-required="born">

  <div>
    Город
    <span>
      *
    </span>
  </div>
  <input name="event[city]" type="text" value="{$feedback.f_3}"  data-required="city">

  <div>
    Клуб
  </div>
  <input name="event[club]" type="text" value="{$feedback.f_3}">

  <div>
    Телефон
    <span>
      *
    </span>
  </div>
  <input name="event[phone]" type="text" value="{$feedback.f_3}" data-required="phone">

  
  
  {if $item.distance}

  {if count($item.distance) == 1}
  <input type="hidden" name="event[distance]" value="{$item.distance[0]}" data-required="distance">
  <div>
    Дистанция: {$item.distance[0]}   
  </div> 
  <br/>
  {else}
  <div>
    Дистанция
    <span>
      *
    </span>
  </div>
  <select name="event[distance]" data-required="distance">
    <option value=" ">
      Не выбрана
    </option>
    {foreach $item.distance as $d}
    <option value="{$d}">
      {$d}
    </option>
    {/foreach}
  </select>
  {/if}
  {/if}

  {if $settings.captcha}
  <img class="captcha" src="{$host}captcha.jpg" align="middle">
  <div class="captcha">
    {$l.feedback9}
    <span>
      *
    </span>
  </div>
  <input type="text"{if !$captcha} class="error"{/if}name="captcha" data-required="captcha">
  <div class="clear">
  </div>
  {/if}
  <div class="red">
    Заполните верно обязательные поля
  </div>
  <div class="member_exist">
    Участник с таким e-mail уже зарегистрирован на это мероприятие
  </div>
  <div class="success">
    Вы успешно зарегистрированы на мероприятие
  </div>
  <input name="event[event_id]" type="hidden" value="{$item.id}">
  <input type="submit" class="button" value="Зарегистрироваться">
  {/if}
</form>
<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script src="https://code.jquery.com/ui/1.11.1/jquery-ui.js">
</script>
<script src="{$design}jquery.mask.min.js">
</script>
{literal}
<script>
  $(function()
    {
      //$('[name="event[born]"]').datepicker();
      $('[name="event[born]"]').mask('00.00.0000');
      $('[name="event[phone]"]').mask('(000)-000-00-00');
    });


  $( "#register" ).submit(function( event )
    {
      ga('send', 'event', 'run', 'registration')
    });
</script>
{/literal}
