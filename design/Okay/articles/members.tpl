{if count($members)}
<a href="#" class="show_members button">Зарегистрированные участники</a>
<div class="event_members">
  <ul>
    <li>
      <span>ФИО</span>
      <span>Дистанция</span>
      <span>Город</span>
      <span>Клуб</span>
      <span>Год рождения</span>
      <span>Оплата</span>
    </li>
    {foreach $members as $member}
    <li>
      <span>{$member.name}</span>
      <span>{$member.distance}</span>
      <span>{$member.city}</span>
      <span>{$member.club}</span>
      <span>{if $member.born>0}{date('Y',strtotime($member.born))}{/if}</span>
      <span style="text-align:center;">{if $member.paid}<img src="{$design}img/ok.png" alt="оплачено" title="оплачено" />{/if}</span>
    </li>
    {/foreach}
  </ul>
</div>
{/if}

{if count($volunteers)}
<div>
  <a href="#" class="show_volunteers button">Зарегистрированные волонтеры</a>
  <div class="event_volunteers">
    <ul>
      <li>
        <span>ФИО</span>
        <span>Дистанция</span>
        <span>Город</span>
        <span>Клуб</span>
        <span>Год рождения</span>
        <span>Подтвержден</span>
      </li>
      {foreach $volunteers as $member}
      <li>
        <span>{$member.name}</span>
        <span>{$member.distance}</span>
        <span>{$member.city}</span>
        <span>{$member.club}</span>
        <span>{if $member.born>0}{date('Y',strtotime($member.born))}{/if}</span>
        <span style="text-align:center;">{if $member.paid}<img src="{$design}img/ok.png" alt="оплачено" title="оплачено" />{/if}</span>
      </li>
      {/foreach}
    </ul>
  </div>
</div>
{/if}