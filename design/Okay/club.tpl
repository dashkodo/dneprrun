{strip}
{* Заголовок *}
<h1>
  {$name}
</h1>
{* Хлебные крошки *}
{include file="breadcrumbs.tpl"}
{* Подробное описание элемента *}

{if $item}
<div class="full_item">
  {$item.full_cont}
</div>

<div class="center">
  <br />
  <a href="{$host}{$language_prefix}{$module.frontend}/enter" class="button">
    Стать членом клуба
  </a>
</div>

{elseif $queries[1] == 'enter'}
<div class="full_item">
  {$module.full_cont2}
</div>
<form method="post" class="feedback register_member" id="register">
  <br /><br />
  <h3>
    Я хочу попасть в клуб
  </h3>
  <div class="req_txt">
    Поля отмеченные
    <span>
      *
    </span>обязательны к заполнению!
  </div>

  <div>
    Ваши ФИО
    <span>
      *
    </span>
  </div>
  <input name="member[name]" type="text" value="" data-required="name">

  <div>
    Email
    <span>
      *
    </span>
  </div>
  <input name="member[email]" type="text" placeholder="" data-required="email">

  <div>
    Телефон
    <span>
      *
    </span>
  </div>
  <input name="member[phone]" type="text" value="" data-required="phone">


  <div>
    О себе
    <span>
      *
    </span>
  </div>
  <input name="member[about]" type="text" value=""  data-required="about">

  <div>
    Беговые планы на будущее
  </div>
  <input name="member[plan]" type="text" >
  <div>
    Беговые достижения
  </div>
  <input name="member[achievement]" type="text" >



  {if $settings.captcha}
  <img class="captcha" src="{$host}captcha.jpg" align="middle">
  <div class="captcha">
    {$l.feedback9}
    <span>
      *
    </span>
  </div>
  <input type="text"{if !$captcha} class="error"{/if}name="captcha" data-required="captcha">
  <div class="clear">
  </div>
  {/if}
  <div class="red">
    Заполните верно обязательные поля
  </div>
  <div class="member_exist">
    Участник с таким e-mail уже зарегистрирован
  </div>
  <div class="success">
    Спасибо за обращение. Мы скоро с вами свяжемся
  </div>
  <input type="submit" class="button" value="Оставить заявку">
</form>
<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script src="https://code.jquery.com/ui/1.11.1/jquery-ui.js">
</script>
<script src="{$design}jquery.mask.min.js">
</script>
{literal}
<script>
  $(function()
    {
      //$('[name="member[born]"]').datepicker();
      $('[name="member[born]"]').mask('00.00.0000');
      $('[name="member[phone]"]').mask('(000)-000-00-00');
    });
</script>
{/literal}
{elseif $queries[1] == 'members'}
members


<div class="center">
  <br />
  <a href="{$host}{$language_prefix}{$module.frontend}/enter" class="button">
    Стать членом клуба
  </a>
</div>

{* Краткие описания элементов *}
{/if}
{/strip}