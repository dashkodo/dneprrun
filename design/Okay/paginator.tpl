{strip}
	{if $paginator}
		{assign 'link' "`$host``$language_prefix``$module.frontend`/"}
		{if $cat.url}
			{assign 'link' "`$link``$cat.url`/"}
		{/if}
		{if $brand.url}
			{assign 'link' "`$link``$brand.url`/"}
		{/if}
		<div class="paginator">
			{if $current_page!=1}<a class="prew no-active" href="{$link}{$current_page-1}">&lt;&lt;</a>{/if}
			{for $page=1 to $paginator}
                {if $page==$current_page}
                	<b>{$page}</b>
                {else}
                	<a href="{$link}{$page}">{$page}</a>
                {/if}
			{/for}
			{if $current_page!=$paginator}<a class="next no-active"href="{$link}{$current_page+1}">&gt;&gt;</a>{/if}
		</div>
	{/if}
{/strip}