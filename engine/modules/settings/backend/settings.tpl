{strip}
	<div class="menu-bg">
		<menu>
			<a{if $queries[2] == ''} class="active"{/if} href="{$host}admin/{$module.backend}">Глобальные</a>
			{if $module.access_module.set_seo}
				<a{if $queries[2] == 'seo'} class="active"{/if} href="{$host}admin/{$module.backend}/seo">SEO</a>
			{/if}
			{if $group.id == 1}
				<a{if $queries[2] == 'languages'} class="active"{/if} href="{$host}admin/{$module.backend}/languages">Языковые настройки</a>
			{/if}
		</menu>
	</div>
	<div id="main">
		{if $queries[2] == ''}
			<div class="block setting">
				<form method="post" enctype="multipart/form-data">
					<div class="string">
						<span class="left">
							Название сайта
							<span class="help">
								<div>Название сайта</div>
							</span>
						</span>
						<span class="right">
							{$sitename_="sitename_`$current_language`"}
							<input name="settings[{$sitename_}]" value="{$settings.$sitename_}" type="text" class="input">
						</span>
					</div>
					<div class="string">
						<span class="left">
							Email администратора
							<span class="help">
								<div>Можно ввести несколько адресов почты через запятую</div>
							</span>
						</span>
						<span class="right">
							<input name="settings[admin_mail]" value="{$settings.admin_mail}" type="text" class="input">
						</span>
					</div>
					<div class="string">
						<span>
							<div class="check-box{if $settings.site_close} on{/if}">
								<input name="settings[site_close]" value="{$settings.site_close}" type="hidden">
							</div>
							Закрыть на профилактику
							<span class="help">
								<div></div>
							</span>
						</span>
					</div>
					{if $group.id == 1}
						<div class="string">
							<span>
								<div class="check-box{if $settings.debag} on{/if}">
									<input name="settings[debag]" value="{$settings.smarty_debag}" type="hidden">
								</div>
								Включить панель debag
							</span>
						</div>
					{/if}
					<div class="string">
						<span class="left">
							Формат даты
							<span class="help">
								<div>Синтаксис идентичен функции <a href="http://www.php.net/date" target="_blank">date()</a> языка PHP</div>
							</span>
						</span>
						<span class="right">
							<input name="settings[date_format]" value="{$settings.date_format}" type="text" class="input" style="width:220px;">
							{$date}
						</span>
					</div>
					<div class="string">
						<span class="left">
							Часовой пояс
						</span>
						<span class="right">
							<select name="settings[time_zone]" class="input" style="width:238px;">
								<option value="-11"{if $settings.time_zone == -11} selected{/if}>GMT - 11</option>
								<option value="-10"{if $settings.time_zone == -10} selected{/if}>GMT - 10</option>
								<option value="-9"{if $settings.time_zone == -9} selected{/if}>GMT - 9</option>
								<option value="-8"{if $settings.time_zone == -8} selected{/if}>GMT - 8</option>
								<option value="-7"{if $settings.time_zone == -7} selected{/if}>GMT - 7</option>
								<option value="-6"{if $settings.time_zone == -6} selected{/if}>GMT - 6</option>
								<option value="-5"{if $settings.time_zone == -5} selected{/if}>GMT - 5</option>
								<option value="-4"{if $settings.time_zone == -4} selected{/if}>GMT - 4</option>
								<option value="-3"{if $settings.time_zone == -3} selected{/if}>GMT - 3</option>
								<option value="-2"{if $settings.time_zone == -2} selected{/if}>GMT - 2</option>
								<option value="-1"{if $settings.time_zone == -1} selected{/if}>GMT - 1</option>
								<option value="0"{if $settings.time_zone == 0} selected{/if}>GMT (по Гринвичу)</option>
								<option value="1"{if $settings.time_zone == 1} selected{/if}>GMT + 1</option>
								<option value="2"{if $settings.time_zone == 2} selected{/if}>GMT + 2</option>
								<option value="3"{if $settings.time_zone == 3} selected{/if}>GMT + 3</option>
								<option value="4"{if $settings.time_zone == 4} selected{/if}>GMT + 4</option>
								<option value="5"{if $settings.time_zone == 5} selected{/if}>GMT + 5</option>
								<option value="7"{if $settings.time_zone == 7} selected{/if}>GMT + 7</option>
								<option value="9"{if $settings.time_zone == 9} selected{/if}>GMT + 9</option>
								<option value="10"{if $settings.time_zone == 10} selected{/if}>GMT + 10</option>
								<option value="12"{if $settings.time_zone == 12} selected{/if}>GMT + 12</option>
							</select>
							{$time} ({$time_zone})
						</span>
					</div>
					<div class="string">
						<span class="left">
							Дизайн
						</span>
						<span class="right">
							<select name="settings[design]" style="width:238px;" class="input">
								{foreach $designs as $design}
									<option value="{$design}" {if $settings.design == $design} selected{/if}>{$design}</option>
								{foreachelse}
									<option value="">Дизайна нет</option>
								{/foreach}
							</select>
						</span>
					</div>
					<div class="string">
						<span class="left">
							Главная страница
							<span class="help">
								<div>Выбор страницы для отображения на главной странице</div>
							</span>
						</span>
						<span class="right">
							<select name="settings[home_page]" class="input" style="width:345px;">
								{foreach $contents as $content}
									<option value="{$content.url}"{if $content.url==$settings.home_page} selected{/if}>{$content.name}</option>
								{/foreach}
							</select>
						</span>
					</div>
					<div class="string">
						<span class="left">
							Главная страница<br>администрирования
							<span class="help">
								<div>Выбор страницы для отображения на главной странице администрирования</div>
							</span>
						</span>
						<span class="right">
							<select name="settings[admin_page]" style="width:238px;" class="input">
								{foreach $permitted_modules as $permitted_module}
									{if $permitted_module.name}
										<option value="{$permitted_module.backend}"{if $permitted_module.backend==$settings.admin_page} selected{/if}>{$permitted_module.name}</option>
									{/if}
								{/foreach}
							</select>
						</span>
					</div>
					<div class="string">
						<span class="left">
							Настройка капчи
						</span>
						<span class="right">
							<select name="settings[captcha]" style="width:238px;" class="input">
								<option value="0"{if $settings.captcha==0} selected{/if}>Выключена</option>
								<option value="1"{if $settings.captcha==1} selected{/if}>Выключена для зарегистрированных</option>
								<option value="2"{if $settings.captcha==2} selected{/if}>Включена</option>
							</select>
						</span>
					</div>
					<div class="string">
						<span class="center">
							<input class="button" type="submit" value="Сохранить">
						</span>
					</div>
				</form>
			</div>
		{elseif $queries[2] == 'seo'}
			<form method="post" enctype="multipart/form-data">
				<div class="block setting">
					<div class="string">
						<span class="left">
							Настройки "title"
							<span class="help">
								<div>Можно использовать шаблоны:<br> <b>%title%</b> - Title конечного элемента (страницы, новости, статьи или продукта) и <br> <b>%sitename%</b> - название сайта (задаётся в <a href="{$host}admin/settings">глобальных настройках сайта</a>)</div>
							</span>
						</span>
						<span class="right">
							<input name="settings[title_format]" value="{$settings.title_format}" type="text" class="input">
						</span>
					</div>
					<div class="string">
						<span class="left">
							Title
							<span class="help">
								<div>Будет отображаться везде где нет содержимого "Title"</div>
							</span>
						</span>
						<span class="right">
							{$title_="title_`$current_language`"}
							<input name="settings[{$title_}]" value="{$settings.$title_}" type="text" class="input">
						</span>
					</div>
					<div class="string">
						<span class="left">
							Description
							<span class="help">
								<div>Будет отображаться везде где нет содержимого "Description"</div>
							</span>
						</span>
						<span class="right">
							{$desc_="desc_`$current_language`"}
							<input name="settings[{$desc_}]" value="{$settings.$desc_}" type="text" class="input">
						</span>
					</div>
					<div class="string">
						<span class="left">
							Keywords
							<span class="help">
								<div>Будет отображаться везде где нет содержимого "Keywords"</div>
							</span>
						</span>
						<span class="right">
							{$key_="key_`$current_language`"}
							<input name="settings[{$key_}]" value="{$settings.$key_}" type="text" class="input">
						</span>
					</div>
					<div class="string">
						<span class="left">
							Google Verification
						</span>
						<span class="right">
							<input name="settings[google_verification]" value="{$settings.google_verification}" type="text" class="input">
						</span>
					</div>
					<div class="string">
						<span class="left">
							Yandex Verification
						</span>
						<span class="right">
							<input name="settings[yandex_verification]" value="{$settings.yandex_verification}" type="text" class="input">
						</span>
					</div>
					<div class="string">
						<span class="left">
							Google Аnalytics
						</span>
						<span class="right">
							<textarea name="settings[google_analytics]" class="input" rows="2">{$settings.google_analytics}</textarea>
						</span>
					</div>
					<div class="string">
						<span class="left">
							Яндекс.Метрика
						</span>
						<span class="right">
							<textarea name="settings[yandex_metrika]" class="input" rows="2">{$settings.yandex_metrika}</textarea>
						</span>
					</div>
					<div class="string">
						<span class="center">
							<input class="button" type="submit" value="Сохранить">
						</span>
					</div>
				</div>
			</form>
		{elseif $queries[2] == 'languages'}
			<form method="post" enctype="multipart/form-data">
				<div class="block setting">
					<div class="string">
						<span class="left">
							Выберите язык
						</span>
						<span class="right">
							<select name="settings[languages][]" data-placeholder=" " multiple class="chzn-select input" tabindex="8">

								{foreach $languages as $key=>$language_name}
									{if $settings['default_language'] == $key}
										<option value="{$key}" class="trr" selected>{$language_name}</option>
										{$rel=$language_name@index}
									{else}
										<option value="{$key}"{if in_array($key, $settings.languages)} selected{/if}>{$language_name}</option>
									{/if}
								{/foreach}
							</select>
							<style>
								.search-choice-close[rel='{$rel}'] {
									display: none !important;
								}
							</style>
						</span>
					</div>
					{if $settings.languages}
						<div class="string">
							<span class="left">
								Язык по умолчанию
							</span>
							<span class="right">
								<select name="settings[default_language]"  style="width:238px;" class="input">
									{foreach $settings.languages as $language}
										<option value="{$language}"{if $language == $settings.default_language} selected{/if}>{$languages.{$language}}</option>
									{/foreach}
								</select>
							</span>
						</div>
					{/if}
					<div class="string">
						<span class="center">
							<input class="button" type="submit" value="Сохранить">
						</span>
					</div>
				</div>
			</form>
		{/if}
		<div class="clear"></div>
	</div>
{/strip}