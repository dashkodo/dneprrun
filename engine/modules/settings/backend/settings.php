<?php

if (!defined('PARENT_FILE')) {
	die('Running banned');
}

if (!empty($_POST['settings'])) {
	foreach ($_POST['settings'] as $key => $value) {
		replace_row('settings', array('name' => $key, 'value' => $value));
	}
	redirect();
}
if (empty($queries[2])) {
	$smarty->assign('date', date($settings['date_format']));
	$smarty->assign('time_zone', date_default_timezone_get());
	$smarty->assign('time', date('G:i:s'));
	foreach (glob(ROOT_PATH . 'design/*', GLOB_MARK | GLOB_ONLYDIR) as $design_path) {
		if (mb_check_encoding($design_path)) {
			$smarty->append('designs', basename($design_path));
		}
	}
	$smarty->assign('contents', select_rows('content-' . $current_language, '`disp`=1', 'sort, id DESC'));
	$smarty->assign('title', 'Глобальные настройки');
} elseif ($queries[2] == 'languages') {
	$languages = array();
	foreach (glob(LANG_PATH . '*.ini') as $language_path) {
		$key = pathinfo($language_path, PATHINFO_FILENAME);
		$value = parse_ini_file($language_path, true);
		if (!empty($value['language_name'])) {
			$languages[$key] = $value['language_name'];
		}
	}
	$smarty->assign('languages', $languages);
	$smarty->assign('title', 'Языковые настройки');
} elseif ($queries[2] == 'backup') {
	$smarty->assign('title', 'Настройки бэкапа');
} elseif ($queries[2] == 'seo') {
	$smarty->assign('title', 'SEO настройки');
}
?>