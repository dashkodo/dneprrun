{strip}
	<div class="menu-bg">
		<menu>
			<a{if $queries[2]!='settings' && $queries[2]!='comments'} class="active"{/if} href="{$host}admin/{$module.backend}">{$module.name}</a>
			{if $module.settings.comments}
				<a{if $queries[2]=='comments'} class="active"{/if} href="{$host}admin/{$module.backend}/comments">Коментарии{if $module.comments} ({$module.comments}){/if}</a>
			{/if}
			<a{if $queries[2]=='settings'} class="active"{/if} href="{$host}admin/{$module.backend}/settings">Настройки</a>
		</menu>
	</div>
	<div id="main">
		{if !$queries[2]}
			<div id="sidebar">
				<div class="block pages">
					{foreach $items as $i}
						<div class="page-link{if $i.id == $item.id} active{/if}">
							<a href="{$host}admin/{$module.backend}/edit/{$i.id}">{$i.name}</a>
							<div class="toolbar">
								<a href="{$host}admin/{$module.backend}/delete/{$i.id}" class="confirm" title="Удалить">
									<img src="{$design}delete-page.png" alt="X" title="Удалить">
								</a>
							</div>
						</div>
					{foreachelse}
						<a>Доступных страниц нет</a>
					{/foreach}
				</div>
			</div>
			{if $settings.default_language == $current_language}
				<div id="content">
					<div class="block setting">
						<div class="string">
							<span class="center">
								<a class="button" href="{$host}{$query}/edit">Добавить новость</a>
							</span>
						</div>
					</div>
				</div>
			{/if}
		{elseif $queries[2]=='add' || $queries[2]=='edit'}
			<div id="sidebar">
				<div class="block pages">
					{foreach $items as $i}
						<div class="page-link{if $i.id == $item.id} active{/if}">
							<a href="{$host}admin/{$module.backend}/edit/{$i.id}">{$i.name}</a>
							<div class="toolbar">
								<a href="{$host}admin/{$module.backend}/delete/{$i.id}" class="confirm" title="Удалить">
									<img src="{$design}delete-page.png" alt="X" title="Удалить">
								</a>
							</div>
						</div>
					{foreachelse}
						<a>Доступных страниц нет</a>
					{/foreach}
				</div>
			</div>
			{if $item.id || $settings.default_language == $current_language}
				<div id="content">
					<div class="block setting">
						<form method="post" enctype="multipart/form-data">
							{if $settings.default_language == $current_language}
								<div class="string">
									<span class="left">
										<div class="check-box{if $item.disp} on{/if}">
											<input name="item[disp]" value="{$item.disp}" type="hidden">
										</div>
										Активность
									</span>
									<span class="right">
										<input class="input" type="text" name="item[datetime]" value="{$item.datetime}" style="width:240px;">
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										Сортировка
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										<input class="input" type="text" name="item[sort]" value="{$item.sort}" style="width:100px;">
									</span>
								</div>
							{/if}
							<div class="string">
								<span class="left">
									Название
								</span>
								<span class="right">
									<div id="module-name">
										<input class="translit input" type="text" name="item[name]" value="{$item.name}" placeholder="Название новости">
										{if $settings.default_language == $current_language && $item.url}
											<div id="switch-translit">
												Обновить<br>URL
												<input type="hidden" id="url" value="">
											</div>
										{/if}
									</div>
								</span>
							</div>
							<div class="string">
								<span class="left">
									Изображение
								</span>
								<span class="right">
									<input name="item[pic]" type="file" class="input" style="width:240px;">
									{if $item.pic}
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										<a href="{$host}data/{$module.backend}/{$item.pic}?{$rand}" rel="photo">
											<img src="{$host}data/{$module.backend}/{$item.pic}?{$rand}" style="max-width:58px;max-height:40px;vertical-align:middle;">
										</a>
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										<a href="{$host}{$query}/delete-pic/{$item.pic}" class="del confirm" title="Удалить"></a>
									{/if}
								</span>
							</div>
							<div class="string">
								<span class="left">
									<a href="#" id="seo">SEO</a>
								</span>
							</div>
							<div class="seo-cont">
								{if $settings.default_language == $current_language}
									<div class="string">
										<span class="left">
											URL
										</span>
										<span class="right">
											<input name="item[url]" value="{$item.url}" type="text" class="url input"{if !$item.url} id="url"{/if}>
										</span>
									</div>
								{/if}
								<div class="string">
									<span class="left">
										Title
									</span>
									<span class="right">
										<input name="item[title]" value="{$item.title}" class="input" type="text">
									</span>
								</div>
								<div class="string">
									<span class="left">
										Description
									</span>
									<span class="right">
										<textarea name="item[desc]" class="input" rows="2">{$item.desc}</textarea>
									</span>
								</div>
								<div class="string">
									<span class="left">
										Keywords
									</span>
									<span class="right">
										<textarea name="item[key]" class="input" rows="2">{$item.key}</textarea>
									</span>
								</div>
							</div>
							<div class="string">
								<span>
									Краткий текст<br>
									<textarea class="redactor" name="item[short_cont]">{$item.short_cont}</textarea>
								</span>
							</div>
							<div class="string">
								<span>
									Полный текст<br>
									<textarea class="redactor" name="item[full_cont]">{$item.full_cont}</textarea>
								</span>
							</div>
							{*<div class="string">
								<span class="left">
									Рекомендуемые
								</span>
								<span class="right">
									<select name="item[related][]" data-placeholder=" " multiple class="chzn-select input" tabindex="8">
										<option value="0" selected></option>
										{foreach $items as $related}
											{if $item.id != $related.id}
												<option value="{$related.id}"{if is_array($item.related) && in_array($related.id, $item.related)} selected{/if}>{$related.name}</option>
											{/if}
										{/foreach}
									</select>
								</span>
							</div>*}
							<!--div class="string">
								<span class="left">
									Tags
								</span>
								<span class="right">
									<input name="item[tags]" value="{$item.tags}" class="input" type="text">
								</span>
							</div-->
							<div class="string">
								<span class="center">
									<input class="button" type="submit" value="{if $item.id}Обновить{else}Добавить{/if}">
								</span>
							</div>
						</form>
					</div>
				</div>
			{/if}
		{elseif $queries[2]=='settings'}
			<div class="block setting">
				<form method="post" enctype="multipart/form-data">
					<div class="string">
						<span class="left">
							Название модуля
						</span>
						<span class="right">
							<div id="module-name">
								<input class="translit input" type="text" name="module[name]" value="{$module.name}" placeholder="Название модуля">
								<div id="switch-translit">
									Обновить<br>URL
									<input type="hidden" id="url" value="">
								</div>
							</div>
						</span>
					</div>
					<div class="string">
						<span class="left">
							Картинка
						</span>
						<span class="right">
							<input name="module[pic]" type="file" class="input" style="width:240px;">
							{if $module.pic}
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<a href="{$host}data/{$module.backend}/{$module.pic}?{$rand}" rel="photo">
									<img src="{$host}data/{$module.backend}/{$module.pic}?{$rand}" style="max-width:58px;max-height:40px;vertical-align:middle;">
								</a>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<a href="{$host}{$query}/delete-pic/{$module.pic}" class="del confirm" title="Удалить"></a>
							{/if}
						</span>
					</div>
					<div class="string">
						<span class="left">
							<a href="#" id="seo">SEO</a>
						</span>
					</div>
					<div class="seo-cont">
						{if $settings.default_language == $current_language}
							<div class="string">
								<span class="left">
									URL
								</span>
								<span class="right">
									<input name="module[frontend]" value="{$module.frontend}" type="text" class="url input">
								</span>
							</div>
						{/if}
						<div class="string">
							<span class="left">
								Title
							</span>
							<span class="right">
								<input name="module[title]" value="{$module.title}" class="input" type="text">
							</span>
						</div>
						<div class="string">
							<span class="left">
								Description
							</span>
							<span class="right">
								<textarea name="module[desc]" class="input" rows="2">{$module.desc}</textarea>
							</span>
						</div>
						<div class="string">
							<span class="left">
								Keywords
							</span>
							<span class="right">
								<textarea name="module[key]" class="input" rows="2">{$module.key}</textarea>
							</span>
						</div>
					</div>
					<div class="string">
						<span>
							Описание<br>
							<textarea class="redactor" name="module[full_cont]">{$module.full_cont}</textarea>
						</span>
					</div>
					{if $settings.default_language == $current_language}
						<div class="string">
							<span class="left">
								Количество элементов<br>на страницу
							</span>
							<span class="right">
								<input name="settings[amount_to_page]" value="{$module.settings.amount_to_page}" type="text" class="input" style="width:100px;">
							</span>
						</div>
						<div class="string">
							<span class="left">
								Количество элементов<br>в блоке
							</span>
							<span class="right">
								<input name="settings[amount_to_block]" value="{$module.settings.amount_to_block}" type="text" class="input" style="width:100px;">
							</span>
						</div>
						{if $module.access_module.set_comment}
							<div class="string">
								<span class="left">
									<div class="check-box{if $module.settings.comments} on{/if}">
										<input name="settings[comments]" value="{$module.settings.comments}" type="hidden">
									</div>
									Комментирование
								</span>
							</div>
							<div class="string">
								<span class="left">
									<div class="check-box{if $module.settings.comments_moderation} on{/if}">
										<input name="settings[comments_moderation]" value="{$module.settings.comments_moderation}" type="hidden">
									</div>
									Модерация комментариев
								</span>
							</div>
							<div class="string">
								<span class="left">
									<div class="check-box{if $module.settings.comments_unregistered} on{/if}">
										<input name="settings[comments_unregistered]" value="{$module.settings.comments_unregistered}" type="hidden">
									</div>
									Разрешить добавлять комментарии незарегистрированным пользователям
								</span>
							</div>
						{/if}
					{/if}
					<div class="string">
						<span class="center">
							<input class="button" type="submit" value="Сохранить">
						</span>
					</div>
				</form>
			</div>
		{elseif $queries[2]=='comments'}
			{if $queries[3]=='edit' || $queries[4]=='edit'}
				<div class="block setting">
					<form method="post" enctype="multipart/form-data">
						<div class="string">
							<span class="center">
								{foreach $items as $item}
									{if $item.id == $comment.pid}
										{$item.name}
									{/if}
								{/foreach}
							</span>
						</div>
						<div class="string">
							<span class="left">
								<div class="check-box{if $comment.disp} on{/if}">
									<input name="comment[disp]" value="{$comment.disp}" type="hidden">
								</div>
								{if $module.settings.comments_moderation}
									Одобрено
								{else}
									Просмотрено
								{/if}
							</span>
							<span class="right">
								<input class="input" type="text" name="comment[datetime]" value="{$comment.datetime}" style="width:240px;">
							</span>
						</div>
						<div class="string">
							<span class="left">
								Ник
							</span>
							<span class="right">
								<input name="comment[name]" value="{$comment.name}" class="input" type="text">
							</span>
						</div>
						<div class="string">
							<span class="left">
								E-mail
							</span>
							<span class="right">
								<input name="comment[email]" value="{$comment.email}" class="input" type="text">
							</span>
						</div>
						<div class="string">
							<span class="left">
								Коментарии
							</span>
							<span class="right">
								<textarea name="comment[comment]" class="input" rows="2">{$comment.comment}</textarea>
							</span>
						</div>
						<div class="string">
							<span class="center">
								<input class="button" type="submit" value="Сохранить">
							</span>
						</div>
					</form>
				</div>
			{else}
				<div class="block setting">
					<div class="string">
						<span class="center">
							<a class="button{if !$queries[3]} active{/if}" href="{$host}admin/{$module.backend}/comments">Новые</a>
							&nbsp;&nbsp;&nbsp;&nbsp;
							<a class="button{if $queries[3] == all} active{/if}" href="{$host}admin/{$module.backend}/comments/all">Показать все</a>
						</span>
					</div>
					{if count($comments)}
						<form method="post" enctype="multipart/form-data">
							{foreach $items as $item}
								<div class="string">
									<span class="center">
										{$item.name}
									</span>
								</div>
								{foreach $comments as $comment}
									{if $item.id == $comment.pid}
										<div class="string">
											<span class="center">
												<div class="check-box{if $comment.disp} on{/if}">
													<input name="comments[{$comment.id}][disp]" value="{$comment.disp}" type="hidden">
												</div>
												{if $module.settings.comments_moderation}
													Одобрено
												{else}
													Просмотрено
												{/if}
												&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
												Ник
												&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
												<input class="input" type="text" name="comments[{$comment.id}][name]" value="{$comment.name}" style="width:240px;">
												&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
												E-mail
												&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
												<input class="input" type="text" name="comments[{$comment.id}][email]" value="{$comment.email}" style="width:240px;">
												<a href="{$host}{$query}/delete/{$comment.id}" class="del confirm" title="Удалить"></a>
												<a href="{$host}{$query}/edit/{$comment.id}" class="edit" title="Редактировать"/></a>
											</span>
										</div>
									{/if}
								{/foreach}
							{/foreach}
							<div class="string">
								<span class="center">
									<input class="button" type="submit" value="Сохранить">
								</span>
							</div>
						</form>
					{else}
						<div class="string">
							<span class="center">
								Комментариев нет
							</span>
						</div>
					{/if}
				</div>
			{/if}
		{/if}
		<div class="clear"></div>
	</div>
{/strip}