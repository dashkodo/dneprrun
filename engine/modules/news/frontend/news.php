<?php

if (!defined('PARENT_FILE')) {
	die('Running banned');
}

if (isset($queries[1])) {
	if ($item = select_row($module['backend'] . '-' . $current_language, false, '`url`="' . escape_string($queries[1]) . '" AND `disp`=1')) {
		if ($item['pic']) {
			$item['pic'] = $host . 'data/' . $module['backend'] . '/' . $item['pic'];
		}
		$item['datetime'] = datetime($item['datetime']);
		array_shift($item['related']);
		if (count($item['related'])) {
			$relateds = array();
			foreach ($item['related'] as $related) {
				$related = select_row($module['backend'] . '-' . $current_language, $related);
				$related['datetime'] = datetime($related['datetime']);
				$relateds[] = $related;
			}
			$item['related'] = $relateds;
		}
		$smarty->assign('item', $item);
		set_seo($item['name'], $item['title'], $item['desc'], $item['key']);
	} elseif (intval($queries[1])) {
		$current_page = (int) $queries[1];
	} else {
		error_404();
	}
} else {
	$current_page = 1;
}


if ($current_page) {
	if ($module['settings']['amount_to_page']) {
		$from = ($current_page - 1) * $module['settings']['amount_to_page'];
		$items = select_rows($module['backend'] . '-' . $current_language, 'disp=1', 'sort, id DESC', $from . ', ' . $module['settings']['amount_to_page']);
	} else {
		$items = select_rows($module['backend'] . '-' . $current_language, 'disp=1', 'sort, id DESC');
	}
	if (count($items)) {
		foreach ($items as &$item) {
			if ($item['pic']) {
				$item['pic'] = $host . 'data/' . $module['backend'] . '/' . $item['pic'];
			}
			$item['datetime'] = datetime($item['datetime']);
		}
		$smarty->assign('items', $items);
		if ($current_year) {
			$items_count = count_rows($module['backend'] . '-' . $current_language, 'disp=1 AND datetime  LIKE "' . $current_year . '%"');
		} else {
			$items_count = count_rows($module['backend'] . '-' . $current_language, 'disp=1');
		}
		if ($module['settings']['amount_to_page'] < $items_count) {
			$paginator = ceil($items_count / $module['settings']['amount_to_page']);
			$smarty->assign('paginator', $paginator);
		}
	} else {
		//error_404();
	}
}
$smarty->assign('current_page', $current_page);
$smarty->assign('name', $seo['name']);
?>