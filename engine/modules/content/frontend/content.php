<?php

if (!defined('PARENT_FILE')) {
	die('Running banned');
}

if (isset($module['item'])) {
	if ($module['item']['pic']) {
		$module['item']['pic'] = $host . 'data/' . $module['backend'] . '/' . $module['item']['pic'];
	}
	$smarty->assign('item', $module['item']);
	set_seo($module['item']['name'], $module['item']['title'], $module['item']['desc'], $module['item']['key']);
} elseif ($queries[0] == $module['frontend'] && $queries[1]) {
	redirect($host . $queries[1]);
}
?>