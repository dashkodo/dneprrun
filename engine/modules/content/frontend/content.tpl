{strip}
	<h1>{$item.name}</h1>
	{* Хлебные крошки *}
	{include file="breadcrumbs.tpl"}
	{* Контент *}
	<div>
		{$item.full_cont}
	</div>
{/strip}