{strip}
	<div class="menu-bg">
		<menu>
        	{if $module.access_module.set_page}
				<a{if $queries[2] == '' || intval($queries[2])} class="active"{/if} href="{$host}admin/{$module.backend}">{$module.name}</a>
            {/if}
            {if $module.access_module.set_block}
				<a{if $queries[2] == 'blocks'} class="active"{/if} href="{$host}admin/{$module.backend}/blocks">Блоки</a>
            {/if}
		</menu>
	</div>
	<div id="main">
		{if ($queries[2] == '' || intval($queries[2])) && $module.access_module.set_page}
			<div id="sidebar">
				<div class="block pages">
					{foreach $pages as $item}
						<div class="page-link{if $item.id == $page.id} active{/if}">
							<a href="{$host}admin/{$module.backend}/{$item.id}">{$item.name}</a>
							<div class="toolbar">
								<a href="{$host}admin/{$module.backend}/delete/{$item.id}" class="confirm" title="Удалить">
									<img src="{$design}delete-page.png" alt="X" title="Удалить">
								</a>
							</div>
						</div>
					{foreachelse}
						<a>Доступных страниц нет</a>
					{/foreach}
				</div>
			</div>
			<div id="content">
				{if $settings.default_language == $current_language || $page.id}
					<form method="post" enctype="multipart/form-data">
						<div class="block setting">
							{if $settings.default_language == $current_language}
								<div class="string">
									<span class="left">
										<div class="check-box{if $page.disp} on{/if}">
											<input name="page[disp]" value="{$page.disp}" type="hidden">
										</div>
										Активность
									</span>
									<span class="right">
										<input class="input" type="text" name="page[datetime]" value="{$page.datetime}" style="width:240px;">
									</span>
								</div>
							{/if}
							<div class="string">
								<span class="left">
									Название
								</span>
								<span class="right">
									<div id="module-name">
										<input class="translit input" type="text" name="page[name]" value="{$page.name}" placeholder="Название страницы">
										{if $settings.default_language == $current_language && $page.url}
											<div id="switch-translit">
												Обновить<br>URL
												<input type="hidden" id="url" value="">
											</div>
										{/if}
									</div>
								</span>
							</div>
							<div class="string">
								<span class="left">
									Картинка
								</span>
								<span class="right">
									<input name="page[pic]" type="hidden" value="{$page.pic}">
									<input name="page[pic]" type="file" class="input" style="width:240px;">
									{if $page.pic}
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										<a href="{$host}data/{$module.backend}/{$page.pic}?{$rand}" rel="photo">
											<img src="{$host}data/{$module.backend}/{$page.pic}?{$rand}" style="max-width:58px;max-height:40px;vertical-align:middle;">
										</a>
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										<a href="{$host}{$query}/delete-pic/{$page.pic}" class="del confirm" title="Удалить"></a>
									{/if}
								</span>
							</div>
							<div class="string">
								<span class="left">
									<a href="#" id="seo">SEO</a>
								</span>
							</div>
							<div class="seo-cont">
								{if $settings.default_language == $current_language}
									<div class="string">
										<span class="left">
											URL
											<span class="help">
												<div class="br4"></div>
											</span>
										</span>
										<span class="right">
											<input name="page[url]" value="{$page.url}" type="text" class="url input"{if !$page.url} id="url"{/if}>
										</span>
									</div>
								{/if}
								<div class="string">
									<span class="left">
										Title
									</span>
									<span class="right">
										<input name="page[title]" value="{$page.title}" class="input" type="text">
									</span>
								</div>
								<div class="string">
									<span class="left">
										Keywords
									</span>
									<span class="right">
										<textarea name="page[key]" class="input" rows="2">{$page.key}</textarea>
									</span>
								</div>
								<div class="string">
									<span class="left">
										Description
									</span>
									<span class="right">
										<textarea name="page[desc]" class="input" rows="2">{$page.desc}</textarea>
									</span>
								</div>
								<!--div class="string">
									<span class="left">
										Tags
									</span>
									<span class="right">
										<input name="page[tags]" value="{$page.tags}" class="input" type="text">
									</span>
								</div-->
							</div>
							<div class="string">
								<span>
									Текст<br>
									<textarea class="redactor" name="page[full_cont]">{$page.full_cont}</textarea>
								</span>
							</div>
							<div class="string">
								<span class="center">
									<input class="button" type="submit" value="Окай">
								</span>
							</div>
						</div>
					</form>
				{/if}
			</div>
		{elseif $queries[2] == 'blocks' && $module.access_module.set_block}
			{if $queries[3]=='add' || $queries[3]=='edit'}
				<div class="block setting">
					<form method="post" enctype="multipart/form-data">
						{if $settings.default_language == $current_language}
							<div class="string">
								<span class="left">
									<div class="check-box{if $block.disp} on{/if}">
										<input name="block[disp]" value="{$block.disp}" type="hidden">
									</div>
									Активность
								</span>
								<span class="right">
									Сортировка
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									<input name="block[sort]" value="{$block.sort}" type="text" class="input" style="width: 100px;">
								</span>
							</div>
						{/if}
						<div class="string">
							<span class="left">
								Заголовок
							</span>
							<span class="right">
								<input name="block[name]" value="{$block.name}" class="input" type="text" placeholder="Новый блок" style="width:560px;">
								{if $settings.default_language == $current_language}
									&nbsp;&nbsp;
									Отображать
									&nbsp;&nbsp;
									<div class="check-box{if $block.disp_name} on{/if}">
										<input name="block[disp_name]" value="{$block.disp_name}" type="hidden">
									</div>
								{/if}
							</span>
						</div>
						<div class="string">
							<span class="left">
								{if count($file_blocks)}
									<label>
										<input type="radio" class="cont" name="block[type]" value="0"{if $block.type==0} checked{/if}>
										Контент
									</label>
									<br>
									<label>
										<input type="radio" class="file" name="block[type]" value="1"{if $block.type==1} checked{/if}>
										Блок из файла
									</label>
								{else}
									Контент
									<input type="hidden" name="block[type]" value="0">
								{/if}
							</span>
							<span class="right cont"{if $block.type==1} style="display: none;"{/if}>
								<textarea name="block[full_cont]" class="redactor">{$block.full_cont}</textarea>
							</span>
							<span class="right file"{if $block.type==0} style="display: none;"{/if}>
								<select name="block[file]" class="input" style="width:238px;">
									<option value="">Выберите файл</option>
									{foreach $file_blocks as $file_block}
										<option value="{$file_block.file}"{if $block.file==$file_block.file} selected{/if}>{$file_block.name}</option>
									{/foreach}
								</select>
							</span>
						</div>
						{if $settings.default_language == $current_language && $module.access_module.set_show}
							<div class="string">
								<span class="left">
									Показывать
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									<select name="block[show]" class="input" style="width:238px;">
										<option value="0"{if $block.show == 0} selected{/if}>Везде</option>
										<option value="1"{if $block.show == 1} selected{/if}>Только на выбранных</option>
										<option value="2"{if $block.show == 2} selected{/if}>Везде кроме выбранных</option>
									</select>
								</span>
								<span class="right">
									Расположение
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									<select name="block[pos]" class="input" style="width:238px;">
										<option value="0"{if $block.pos == 0} selected{/if}>Слева</option>
										<option value="1"{if $block.pos == 1} selected{/if}>Сверху</option>
										<option value="2"{if $block.pos == 2} selected{/if}>Снизу</option>
										<option value="3"{if $block.pos == 3} selected{/if}>Справа</option>
										<option value="4"{if $block.pos == 4} selected{/if}>Произвольно</option>
									</select>
									{if $block.pos == 4}
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										<input value="{ldelim}$n{$block.id}{rdelim}, {ldelim}$b{$block.id}{rdelim}" class="input" style="width:200px;" type="text">
									{/if}
								</span>
							</div>
							<div class="string">
								<span class="left">
									&nbsp;&nbsp;&nbsp;
									<select name="block[to_show][]" class="input" style="width:345px;" size="15" multiple{if $block.show == 0} disabled{/if}>
										{foreach $frontends as $frontend}
											<option value="{$frontend.id}"{if in_array($frontend.id, $block.to_show)} selected{/if}>{$frontend.name}</option>
											{if count($frontend.items)}
												{foreach $frontend.items as $item}
													<option value="{$frontend.id}i{$item.id}"{if in_array("`$frontend.id`i`$item.id`", $block.to_show)} selected{/if}>&nbsp;&nbsp;&nbsp;{$item.name}</option>
												{/foreach}
											{elseif count($frontend.categories)}
												{foreach $frontend.categories as $category}
													<option value="{$frontend.id}c{$category.id}"{if in_array("`$frontend.id`c`$category.id`", $block.to_show)} selected{/if}>&nbsp;&nbsp;&nbsp;{$category.name|indent:$category.level:'&nbsp;&nbsp;&nbsp;'}</option>
												{/foreach}
											{/if}
										{/foreach}
									</select>
								</span>
							</div>
						{/if}
						<div class="string">
							<span class="center">
								<input class="button" type="submit" value="Сохранить">
							</span>
						</div>
					</form>
				</div>
			{else}
				<div class="block setting">
					<div class="string">
						{if $settings.default_language == $current_language}
							<span class="center">
								<a href="{$host}{$query}/add" class="button">Добавить блок</a>
							</span>
						{/if}
					</div>
					<form method="post" enctype="multipart/form-data">
						{if count($blocks)}
							<div class="dnd">
								{foreach $blocks as $block}
									<div class="string" id="{$block.id}">
										<span>
											<span class="handle"></span>
											<div class="check-box{if $block.disp} on{/if}">
												<input name="blocks[{$block.id}][disp]" value="{$block.disp}" type="hidden">
											</div>
											Название
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											<input name="blocks[{$block.id}][name]" value="{$block.name}" class="input" type="text" style="width:360px;">
											{if $settings.default_language == $current_language && $module.access_module.set_show}
												&nbsp;&nbsp;&nbsp;
												Расположение
												&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
												<select name="blocks[{$block.id}][pos]" class="input" style="width:180px;">
													<option value="0"{if $block.pos == 0} selected{/if}>Слева</option>
													<option value="1"{if $block.pos == 1} selected{/if}>Сверху</option>
													<option value="2"{if $block.pos == 2} selected{/if}>Снизу</option>
													<option value="3"{if $block.pos == 3} selected{/if}>Справа</option>
													<option value="4"{if $block.pos == 4} selected{/if}>Произвольно</option>
												</select>
											{/if}
											<a href="{$host}{$query}/delete/{$block.id}" class="del confirm" title="Удалить"></a>
											<a href="{$host}{$query}/edit/{$block.id}" class="edit" title="Редактировать"></a>
										</span>
									</div>
								{/foreach}
							</div>
						{else}
							<div class="string">
								<span class="center">
									Блоков нет
								</span>
							</div>
						{/if}
						{if count($blocks)}
							<div class="string">
								<span class="center">
									<input class="button" type="submit" value="Окай">
								</span>
							</div>
						{/if}
					</form>
				</div>
			{/if}
		{/if}
		<div class="clear"></div>
	</div>
{/strip}