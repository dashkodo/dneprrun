<?php

if (!defined('PARENT_FILE')) {
	die('Running banned');
}

query("CREATE TABLE IF NOT EXISTS `" . $module['backend'] . '-' . $current_language . "` (
   `id` int(11) not null auto_increment,
   `disp` int(11) default '1',
   `url` text,
   `name` text,
   `title` text,
   `desc` text,
   `key` text,
   `tags` text,
   `full_cont` text,
   `datetime` datetime,
   `pic` text,
   `sort` int(11) default '0',
   PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;");

query("CREATE TABLE IF NOT EXISTS `blocks-" . $current_language . "` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `disp` int(11) DEFAULT '1',
  `name` text,
  `disp_name` int(11) DEFAULT '1',
  `type` int(11) DEFAULT '0',
  `full_cont` text,
  `file` text,
  `pos` int(11) DEFAULT '0',
  `show` int(11) DEFAULT '0',
  `to_show` text,
  `sort` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;");


$upload_path = upload_path($module['backend']);

if (empty($queries[2])) {
	$smarty->assign('page', array('disp' => 1, 'datetime' => $datetime));
	$smarty->assign('title', 'Новая страница');
	if (!empty($_POST['page'])) {
		if (!empty($_POST['page']['name']) && !empty($_POST['page']['url'])) {
			$id = insert_row($module['backend'] . '-' . $current_language, $_POST['page']);
			if ($_FILES['page']['size']['pic']) {
				$img = uniqid() . '.';
				$img .= pathinfo($_FILES['page']['name']['pic'], PATHINFO_EXTENSION);
				$img = strtolower($img);
				if (move_uploaded_file($_FILES['page']['tmp_name']['pic'], $upload_path . $img)) {
					update_row($module['backend'] . '-' . $current_language, array('pic' => $img), $id);
				}
			}
			if ($id) {
				redirect('/' . $id);
			}
		}
		$smarty->assign('page', $_POST['page']);
	}
} elseif (intval($queries[2])) {
	$id = (int) $queries[2];
	if ($queries[3] == 'delete-pic') {
		unlink($upload_path . $queries[4]);
		update_row($module['backend'] . '-' . $current_language, array('pic' => null), $id);
		redirect(-1);
	}
	if (!empty($_POST['page'])) {
		if (!empty($_POST['page']['name'])) {
			update_row($module['backend'] . '-' . $current_language, $_POST['page'], $id);
			if ($settings['default_language'] == $current_language) {
				foreach ($settings['languages'] as $language) {
					if ($language != $current_language) {
						update_row($module['backend'] . '-' . $language, array(
							'url' => $_POST['page']['url'],
							'disp' => $_POST['page']['disp'],
							'datetime' => $_POST['page']['datetime']
								), $id);
					}
				}
			}
			if ($_FILES['page']['size']['pic']) {
				$img = uniqid() . '.';
				$img .= pathinfo($_FILES['page']['name']['pic'], PATHINFO_EXTENSION);
				$img = strtolower($img);
				if (move_uploaded_file($_FILES['page']['tmp_name']['pic'], $upload_path . $img)) {
					update_row($module['backend'] . '-' . $current_language, array('pic' => $img), $id);
				}
			}
			redirect();
		}
	}
	if (!select_row($module['backend'] . '-' . $current_language, $id)) {
		$copy = select_row($module['backend'] . '-' . $settings['default_language'], $id);
		unset($copy['pic']);
		insert_row($module['backend'] . '-' . $current_language, $copy);
	}
	$smarty->assign('page', select_row($module['backend'] . '-' . $current_language, $id));
	$smarty->assign('title', 'Редактирование страницы');
} elseif ($queries[2] == 'delete') {
	$id = (int) $queries[3];
	if ($settings['languages']) {
		foreach ($settings['languages'] as $language) {
			$item = select_row($module['backend'] . '-' . $language, $id);
			if ($item['pic']) {
				unlink($upload_path . $item['pic']);
			}
			delete_row($module['backend'] . '-' . $language, $id);
		}
	} else {
		$item = select_row($module['backend'] . '-' . $current_language, $id);
		if ($item['pic']) {
			unlink($upload_path . $item['pic']);
		}
		delete_row($module['backend'] . '-' . $current_language, $id);
	}

	redirect(-2);
}
$smarty->assign('pages', select_rows($module['backend'] . '-' . $current_language, false, 'id'));





if ($queries[2] == 'blocks') {
	$smarty->assign('title', 'Блоки');
	$id = (int) $queries[4];
	if ($_POST['block']['name']) {
		if ($id) {
			if ($settings['default_language'] == $current_language) {
				if (!is_array($_POST['block']['to_show'])) {
					$_POST['block']['to_show'] = array();
				}
				$block = array(
					'disp' => $_POST['block']['disp'],
					'sort' => $_POST['block']['sort'],
					'disp_name' => $_POST['block']['disp_name']
				);
				if ($_POST['block']['pos']) {
					$block['pos'] = $_POST['block']['pos'];
				}
				if ($_POST['block']['show']) {
					$block['show'] = $_POST['block']['show'];
				}
				if ($_POST['block']['to_show']) {
					$block['to_show'] = $_POST['block']['to_show'];
				}
				foreach ($settings['languages'] as $language) {
					if ($language != $current_language) {
						update_row('blocks-' . $language, $block, $id);
					}
				}
			}
			update_row('blocks-' . $current_language, $_POST['block'], $id);
			redirect(-2);
		} else {
			if (!is_array($_POST['block']['to_show'])) {
				$_POST['block']['to_show'] = array();
			}
			insert_row('blocks-' . $current_language, $_POST['block']);
			redirect(-1);
		}
	}
	if (isset($_POST['blocks'])) {
		foreach ($_POST['blocks'] as $id => $values) {
			if (!empty($values['name'])) {
				update_row('blocks-' . $current_language, $values, $id);
				if ($settings['default_language'] == $current_language && $values['pos']) {
					foreach ($settings['languages'] as $language) {
						if ($language != $current_language) {
							update_row('blocks-' . $language, array(
								'pos' => $values['pos'],
									), $id);
						}
					}
				}
			}
		}
		redirect();
	}
	if ($queries[3] == 'add') {
		$smarty->assign('block', array(
			'disp' => 1,
			'sort' => 0,
			'disp_name' => 1,
			'to_show' => array()
		));
		$smarty->assign('title', 'Добавление');
	} elseif ($queries[3] == 'edit') {
		if (!select_row('blocks-' . $current_language, $id)) {
			$copy = select_row('blocks-' . $settings['default_language'], $id);
			insert_row('blocks-' . $current_language, $copy);
		}
		$smarty->assign('block', select_row('blocks-' . $current_language, $id));
		$smarty->assign('title', 'Редактирование');
	} elseif ($queries[3] == 'delete') {
		if ($settings['languages']) {
			foreach ($settings['languages'] as $language) {
				delete_row('blocks-' . $language, $id);
			}
		} else {
			delete_row('blocks-' . $current_language, $id);
		}
		redirect(-2);
	} elseif ($queries[3] == 'sort') {
		$sort = explode(',', $queries[4]);
		foreach ($sort as $key => $id) {
			update_row('blocks-' . $current_language, array('sort' => $key + 1), $id);
		}
		die();
	}
	$smarty->assign('blocks', select_rows('blocks-' . $current_language, false, '`sort`, `id` DESC'));
	//
	$file_blocks = array();
	foreach (glob(ROOT_PATH . 'design/' . $settings['design'] . '/blocks/*.tpl') as $file_path) {
		$file_name = pathinfo($file_path, PATHINFO_FILENAME);
		$mb = explode("_", $file_name);
		$file_name = (count($mb)>1) ? $mb[0] : $file_name;
		$pstfx = (count($mb)>1) ? ('_'.$mb[1]) : null;
		$row = select_rows('modules-' . $current_language, '`backend`="' . $file_name . '"');
		if (!count($row)) {
			$row[0]['name'] = $file_name . $pstfx . '.tpl';
		} else {
			$row[0]['name'] = $row[0]['name'] . $pstfx;
		}
		$row[0]['file'] = $file_name . $pstfx . '.tpl';
		$file_blocks[] = $row[0];
	}
	$smarty->assign('file_blocks', $file_blocks);
}
?>