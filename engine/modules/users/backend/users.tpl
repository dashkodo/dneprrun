{strip}
	<div class="menu-bg">
		<menu>
			<a{if empty($queries[2]) || $queries[2]=='add' || $queries[2]=='edit'} class="active"{/if} href="{$host}admin/{$module.backend}">{$module.name}</a>
			{if $module.access_module.set_groupe}
				<a{if $queries[2] == 'groups'} class="active"{/if} href="{$host}admin/{$module.backend}/groups">Группы</a>
			{/if}
		</menu>
	</div>
	<div id="main">
		<div class="block setting">
			{if empty($queries[2])}
				<div class="string">
					<span class="center">
						<a href="{$host}{$query}/add" class="button">Добавить пользователя</a>
					</span>
				</div>
				<form method="post" enctype="multipart/form-data">
					{foreach $users as $user}
						<div class="string">
							<span>
								<div class="check-box{if $user.disp} on{/if}">
									<input name="users[{$user.id}][disp]" value="{$user.disp}" type="hidden">
								</div>
								Логин: {$user.login}
								<a href="{$host}{$query}/del/{$user.id}" class="del confirm" title="Удалить"></a>
								<a href="{$host}{$query}/edit/{$user.id}" class="edit" title="Редактировать"></a>
							</span>
						</div>
					{foreachelse}
						<div class="string">
							<span class="center">
								Пользователей нет
							</span>
						</div>
					{/foreach}
					{if count($users)}
						<div class="string">
							<span class="center">
								<input class="button" type="submit" value="Сохранить">
							</span>
						</div>
					{/if}
				</form>
			{elseif $queries[2]=='add' || $queries[2]=='edit'}
				<form method="post" enctype="multipart/form-data">
					<div class="string">
						<span class="left">
							<div class="check-box{if $user.disp} on{/if}">
								<input name="user[disp]" value="{$user.disp}" type="hidden">
							</div>
							Активность
						</span>
						<span class="right">
							Группа
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<select name="user[group]" class="input" style="width:238px;">
								{foreach $groups as $group}
									<option value="{$group.id}"{if $group.id == $user.group} selected{/if}>{$group.name}</option>
								{foreachelse}
									<option value="100">Нет групп</option>
								{/foreach}
							</select>
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							Дата регистрации
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<input name="user[datetime]" value="{$user.datetime}" class="input center" type="text" style="width:240px;">
						</span>
					</div>
					<div class="string">
						<span class="left">
							Логин
						</span>
						<span class="right">
							<input name="user[login]" value="{$user.login}" class="input" type="text" placeholder="Введите логин пользователя" required>
						</span>
					</div>
					<div class="string">
						<span class="left">
							Пароль
						</span>
						<span class="right">
							<input name="user[pass]" value="{$user.pass}" class="input" type="password"{if $user.id}placeholder="Пароль скрыт в целях безопасности"{/if}>
						</span>
					</div>
					<div class="string">
						<span class="left">
							Подтвердите пароль
						</span>
						<span class="right">
							<input name="user[pass2]" value="{$user.pass2}" class="input" type="password">
						</span>
					</div>
					<div class="string">
						<span class="left">
							Имя
						</span>
						<span class="right">
							<input name="user[name]" value="{$user.name}" class="input" type="text" placeholder="Введите имя пользователя" required>
						</span>
					</div>
					<div class="string">
						<span class="left">
							E-mail
						</span>
						<span class="right">
							<input name="user[mail]" value="{$user.mail}" class="input" type="text">
						</span>
					</div>
					<div class="string">
						<span class="center">
							<input class="button" type="submit" value="Сохранить">
						</span>
					</div>
				</form>
			{elseif $queries[2]=='groups'}
				{if empty($queries[3])}
					<div class="string">
						<span class="center">
							<a href="{$host}{$query}/add" class="button">Добавить группу</a>
						</span>
					</div>
					{if count($groups)}
						<form method="post" enctype="multipart/form-data">
							<div class="dnd">
								{foreach $groups as $group}
									<div class="string" id="{$group.id}">
										<span>
											<span class="handle"></span>
											<div class="check-box{if $group.disp} on{/if}">
												<input name="groups[{$group.id}][disp]" value="{$group.disp}" type="hidden">
											</div>
											Название
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											<input name="groups[{$group.id}][name]" value="{$group.name}" class="input" type="text" style="width:440px;" required>
											{if $group.id != 1 && $group.id != 2}
												<a href="{$host}{$query}/del/{$group.id}" class="del confirm" title="Удалить"></a>
											{/if}
											<a href="{$host}{$query}/edit/{$group.id}" class="edit" title="Редактировать"></a>
										</span>
									</div>
								{/foreach}
							</div>
							<div class="string">
								<span class="center">
									<input class="button" type="submit" value="Сохранить">
								</span>
							</div>
						</form>
					{else}
						<div class="string">
							<span class="center">
								Групп нет
							</span>
						</div>
					{/if}
				{elseif $queries[3]=='add' || $queries[3]=='edit'}
					<form method="post" enctype="multipart/form-data">
						<div class="string">
							<span class="left">
								<div class="check-box{if $item.disp} on{/if}">
									<input name="item[disp]" value="{$item.disp}" type="hidden">
								</div>
								Активность
							</span>
							<span class="right">
								Сортировка
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input name="item[sort]" value="{$item.sort}" type="text" class="input" style="width:220px;">
							</span>
						</div>
						<div class="string">
							<span class="left">
								Название группы
							</span>
							<span class="right">
								<input name="item[name]" value="{$item.name}" class="input" type="text" placeholder="Введите имя группы" required>
							</span>
						</div>
						{if ($group.id == 1 || $group.id == 2) && $item.id != $group.id}
							<div class="string">
								<span>
									<span style="display: inline-block; margin: 20px;">Карта привилегий административной части</span><br>
									{foreach $permitted_modules as $permitted_module => $sub}
										<span style="display: inline-block; margin: 20px;">
											<div class="check-box{if array_key_exists($permitted_module, $item.access)} on{/if}">
												<input name="item[access][{$permitted_module}][disp]" value="{if array_key_exists($permitted_module, $item.access)}1{/if}" type="hidden">
											</div>
											{$sub.name|default:$permitted_module}
											{if $sub.access}
												{foreach $sub.access as $key => $name}
													<div>
														<div class="check-box{if $item.access.$permitted_module.$key} on{/if}">
															<input name="item[access][{$permitted_module}][{$key}]" value="{$item.access.$permitted_module.$key}" type="hidden">
														</div>
														{$name}
													</div>
												{/foreach}
											{/if}
										</span>
									{/foreach}
								</span>
							</div>
						{/if}
						<div class="string">
							<span class="center">
								<input class="button" type="submit" value="Сохранить">
							</span>
						</div>
					</form>
				{/if}
			{/if}
		</div>
		<div class="clear"></div>
	</div>
{/strip}