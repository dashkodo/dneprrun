<?php

if (!defined('PARENT_FILE')) {
	die('Running banned');
}

$upload_path = upload_path($module['backend']);

if (empty($queries[2])) {
	if (isset($_POST['users'])) {
		foreach ($_POST['users'] as $id => $values) {
			if (!empty($values['name'])) {
				update_row($module['backend'], $values, $id);
			}
		}
		redirect();
	}
	$rows = select_rows($module['backend'], false, '`datetime` DESC');
	foreach ($rows as $row) {
		if (is_root()) {
			$smarty->append('users', $row);
		} else {
			$group = group($row['group']);
			if ($group['access'] != 'root') {
				$smarty->append('users', $row);
			}
		}
	}
	$smarty->assign('groups', groups());
} elseif ($queries[2] == 'add') {
	$smarty->assign('groups', groups());
	$smarty->assign('user', array(
		'id' => 0,
		'disp' => 1,
		'login' => '',
		'pass' => '',
		'pass2' => '',
		'name' => '',
		'mail' => '',
		'datetime' => $datetime,
		'group' => 0
	));
	$smarty->assign('title', 'Добавление');
	if (isset($_POST['user'])) {
		if (!empty($_POST['user']['login']) && !empty($_POST['user']['pass'])
				&& ($_POST['user']['pass'] == $_POST['user']['pass2'])) {
			$id = insert_row($module['backend'], array('pass' => crypt($_POST['user']['pass'])));
			unset($_POST['user']['pass'], $_POST['user']['pass2']);
			update_row($module['backend'], $_POST['user'], $id);
			redirect(-1);
		}
		$smarty->assign('user', $_POST['user']);
	}
} elseif ($queries[2] == 'edit') {
	$id = (int) $queries[3];
	$row = select_row($module['backend'], $id);
	unset($row['pass']);
	$smarty->assign('user', $row);
	$smarty->assign('title', 'Редактирование');
	$smarty->assign('groups', groups());

	if (isset($_POST['user'])) {
		if (!empty($_POST['user']['login'])) {
			if (!empty($_POST['user']['pass']) && $_POST['user']['pass'] == $_POST['user']['pass2']) {
				update_row($module['backend'], array('pass' => crypt($_POST['user']['pass'])), $id);
			}
			unset($_POST['user']['pass'], $_POST['user']['pass2']);
			update_row($module['backend'], $_POST['user'], $id);
			redirect(-2);
		}
		$smarty->assign('user', $user);
	}
} elseif ($queries[2] == 'del') {
	$id = (int) $queries[3];
	delete_row($module['backend'], $id);
	redirect(-2);
} elseif ($queries[2] == 'groups') {
	$smarty->assign('title', 'Группы');
	$id = (int) $queries[4];
	if (isset($_POST['groups'])) {
		foreach ($_POST['groups'] as $id => $values) {
			if (!empty($values['name'])) {
				update_row('groups', $values, $id);
			}
		}
		redirect();
	}

	if (empty($queries[3])) {
		$smarty->assign('groups', groups());
	} elseif ($queries[3] == 'add') {
		if (isset($_POST['item'])) {
			$item = escape_array($_POST['item']);
			if (!empty($item['name'])) {
				$group_access = array();
				foreach ($item['access'] as $group_name => $access_module) {
					if ($access_module['disp']) {
						$group_access[$group_name] = $access_module;
					}
				}

				query("INSERT INTO `groups` SET
					`disp`=" . (int) $item['disp'] . ",
					`name`='" . $item['name'] . "',
					`access`='" . serialize($group_access) . "',
					`sort`=" . (int) $item['sort']);
				redirect(-1);
			}
		}
		$smarty->assign('item', array(
			'disp' => 1,
			'access' => array(),
			'sort' => 0));
		$smarty->assign('title', 'Добавление');
	} elseif ($queries[3] == 'edit') {
		if (isset($_POST['item'])) {
			$item = escape_array($_POST['item']);
			if (!empty($item['name'])) {
				query("UPDATE `groups` SET
					`disp`=" . (int) $item['disp'] . ",
					`name`='" . $item['name'] . "',
					`sort`=" . (int) $item['sort'] . "
					WHERE `id`=" . $id);
				if (isset($item['access'])) {
					$group_access = array();
					foreach ($item['access'] as $group_name => $access_module) {
						if ($access_module['disp']) {
							$group_access[$group_name] = $access_module;
						}
					}

					query("UPDATE `groups` SET
						`access`='" . serialize($group_access) . "'
						WHERE `id`=" . $id);
				}
				redirect(-2);
			}
		}
		$smarty->assign('item', group($id));
		$smarty->assign('title', 'Редактирование');
	} elseif ($queries[3] == 'del') {
		query("DELETE FROM `groups` WHERE `id`=" . $id);
		redirect(-2);
	} elseif ($queries[3] == 'sort') {
		$sort = explode(',', $queries[4]);
		foreach ($sort as $key => $id) {
			query("UPDATE `groups` SET `sort`=" . (int) $key . "+1 WHERE `id`=" . (int) $id);
		}
		die();
	}
}
?>