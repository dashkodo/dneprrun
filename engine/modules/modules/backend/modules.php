<?php

if (!defined('PARENT_FILE')) {
	die('Running banned');
}

// формируем список всех устанновленых модулей и помещаем в переменную $installed_modules
$rows = select_rows($module['backend'] . '-' . $current_language, false, '`sort`, `id` DESC');
foreach ($rows as $row) {
	if ($row['type'] == 'frontend' || $row['type'] == 'all') {
		if ($row['disp'] && !file_exists(ROOT_PATH . 'design/' . $settings['design'] . '/' . $row['backend'] . '.tpl')) {
			$row['disp'] = 0;
		}
	}
	$installed_modules[$row['backend']] = $row;
}
$smarty->assign('installed_modules', $installed_modules);
//сортировка
if (isset($queries[2]) && $queries[2] == 'sort') {
	$sort = explode(',', $queries[3]);
	if ($settings['languages']) {
		foreach ($settings['languages'] as $language) {
			foreach ($sort as $key => $id) {
				update_row($module['backend'] . '-' . $language, array('sort' => $key + 1), $id);
			}
		}
	} else {
		foreach ($sort as $key => $id) {
			update_row($module['backend'] . '-' . $current_language, array('sort' => $key + 1), $id);
		}
	}
	die();
}
//сохранение изменений
if (isset($_POST['items'])) {
	foreach ($_POST['items'] as $id => $item) {
		if ($item['disp']) {
			$module_frontend_path = MODULES_PATH . $item['backend'] . '/frontend/';
			$template_path = ROOT_PATH . 'design/' . $settings['design'] . '/';
			if (!file_exists($template_path . $item['backend'] . '.tpl')) {
				copy($module_frontend_path . $item['backend'] . '.tpl', $template_path . '/' . $item['backend'] . '.tpl');
			}
			if (!file_exists($template_path . 'blocks/')) {
				mkdir($template_path . 'blocks/', 0777);
			}
			if (!file_exists($template_path . 'blocks/' . $item['backend'] . '.tpl')) {
				foreach (glob($module_frontend_path . 'blocks/*.tpl') as $filename) {
					copy($module_frontend_path . 'blocks/' . $item['backend'] . '.tpl', $template_path . 'blocks/' . $item['backend'] . '.tpl');
				}
			}
		}
		if ($settings['languages']) {
			foreach ($settings['languages'] as $language) {
				if (select_row($module['backend'] . '-' . $language, $id)) {
					update_row($module['backend'] . '-' . $language, $item, $id);
				} elseif ($item['disp']) {
					$copy = select_row('modules-' . $settings['default_language'], $id);
					unset($copy['pic']);
					$copy = $copy + $item;
					insert_row('modules-' . $language, $copy);
				}
			}
		} else {
			update_row($module['backend'] . '-' . $current_language, $item, $id);
		}
	}
	redirect();
}


//удаление модуля
if (isset($queries[2]) && $queries[2] == 'del') {
	if ($delete_module = select_row($module['backend'] . '-' . $current_language, $queries[3])) {
		rename(MODULES_PATH . $delete_module['backend'], TEMP_PATH . 'engine-' . $delete_module['backend']);
		rename(DATA_PATH . $delete_module['backend'], TEMP_PATH . 'data-' . $delete_module['backend']);
		query("DROP TABLE IF EXISTS `" . $delete_module['backend'] . "`");
		if ($settings['languages']) {
			foreach ($settings['languages'] as $language) {
				query("DROP TABLE IF EXISTS `" . $delete_module['backend'] . '-' . $language . "`");
				query("DROP TABLE IF EXISTS `" . $delete_module['backend'] . '-categories-' . $language . "`");
			}
		} else {
			query("DROP TABLE IF EXISTS `" . $delete_module['backend'] . '-' . $current_language . "`");
			query("DROP TABLE IF EXISTS `" . $delete_module['backend'] . '-categories-' . $current_language . "`");
		}
		query("DROP TABLE IF EXISTS `" . $delete_module['backend'] . "-comments`");
		if ($settings['languages']) {
			foreach ($settings['languages'] as $language) {
				delete_row($module['backend'] . '-' . $language, $delete_module['id']);
			}
		} else {
			delete_row($module['backend'] . '-' . $current_language, $delete_module['id']);
		}
		cldir_recursive(TEMP_PATH);
	}
	redirect(-2);
}
//установка модуля
if (isset($_FILES['install_module']) && $_FILES['install_module']['size']) {
	$install_module = $_FILES['install_module'];
	if ($install_module['type'] == 'application/zip') {
		$pathinfo = pathinfo($install_module['name']);
		//upload
		$module_temp_path = TEMP_PATH . $rand;
		$module_temp_file = $module_temp_path . '.' . $pathinfo['extension'];
		move_uploaded_file($install_module['tmp_name'], $module_temp_file);
		//unzip
		$zip = new ZipArchive;
		$res = $zip->open($module_temp_file);
		if ($res === true) {
			$zip->extractTo($module_temp_path);
			$zip->close();
			//read install.ini
			$install_module = parse_ini_file($module_temp_path . '/install.ini', true);
			if ($install_module) {
				//type
				if (!file_exists($module_temp_path . '/frontend')) {
					$install_module['type'] = 'backend';
				} elseif (!file_exists($module_temp_path . '/backend')) {
					$install_module['type'] = 'frontend';
				} else {
					$install_module['type'] = 'all';
				}
				//
				$q = query("SELECT * FROM `" . $module['backend'] . '-' . $current_language . "` WHERE `backend` LIKE '" . $install_module['backend'] . "%' ORDER BY `id` DESC");
				if ($row = fetch_assoc($q)) {
					if ($install_module['backend'] == $row['backend']) {
						$install_module['new_backend'] = $install_module['backend'] . '2';
						$install_module['name'] = $install_module['name'] . '(2)';
					} else {
						$out = array();
						preg_match('/\d+/', $row['backend'], $out);
						$install_module['new_backend'] = $install_module['backend'] . ($out[0] + 1);
						$install_module['name'] = $install_module['name'] . ' ' . ($out[0] + 1);
					}
				} else {
					$install_module['new_backend'] = $install_module['backend'];
				}

				//rename
				function rename_recursive($dir, $old_name, $name) {
					$files = array_diff(scandir($dir), array('..', '.'));
					foreach (glob($dir . '/' . $old_name . '.*') as $filename) {
						rename($filename, $dir . '/' . $name . '.' . pathinfo($filename, PATHINFO_EXTENSION));
					}
					foreach ($files as $file) {
						$file = $dir . '/' . $file;
						if (is_dir($file)) {
							rename_recursive($file, $old_name, $name);
						}
					}
				}

				rename_recursive($module_temp_path, $install_module['backend'], $install_module['new_backend']);
				unlink($module_temp_path . '/install.ini');
				//relocation
				rename($module_temp_path, MODULES_PATH . $install_module['new_backend']);
				//save zip file
				//rename($module_temp_file, MODULES_PATH.$install_module['new_backend'].'.'.$pathinfo['extension']);
				//update db
				$insert_data = array(
					'disp' => 0,
					'name' => $install_module['name'],
					'frontend' => $install_module['new_backend'],
					'backend' => $install_module['new_backend'],
					'type' => $install_module['type'],
					'settings' => $install_module['settings'],
					'access' => $install_module['access']
				);
				if ($settings['languages']) {
					foreach ($settings['languages'] as $language) {
						insert_row($module['backend'] . '-' . $language, $insert_data);
					}
				} else {
					insert_row($module['backend'] . '-' . $current_language, $insert_data);
				}
			}
			cldir_recursive(TEMP_PATH);
			redirect();
		}
	}
}
?>