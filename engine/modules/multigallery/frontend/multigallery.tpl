{strip}
	{if $item}
		<div class="datetime">{$item.datetime}</div>
	{/if}
	{* Заголовок *}
	<h1>{$name}</h1>
	{* Хлебные крошки *}
	{include file="breadcrumbs.tpl"}
	{* Подробное описание элемента *}
	{if $item}
		<div class="item">
			{$item.full_cont}
		</div>
	{else}
		{if empty($queries[1])}
			{if $module.pic}
				<img class="module-pic" src="{$host}data/{$module.backend}/{$module.pic}" alt="{$module.name}">
			{/if}
			{if $module.full_cont}
				<div class="full-cont">{$module.full_cont}</div>
			{/if}
		{/if}
		{* Полное описание категории *}
		{if $current_page==1}
			<div class="full-cont">{$cat.full_cont}</div>
		{/if}
		{* Категории *}
		{if count($cats) && $current_page==1}
			<h3>{$l.category}</h3>
			<div class="categories">
				{foreach $cats as $c}
					<div>
						{if $c.pic}
							<a href="{$host}{$language_prefix}{$module.frontend}/{$c.url}">
								<img src="{$c.pic}" alt="{$c.name}">
							</a>
						{/if}
						<br>
						<br>
						<a class="center" href="{$host}{$language_prefix}{$module.frontend}/{$c.url}">{$c.name}</a>
						<div>{$c.short_cont}</div>
					</div>
				{/foreach}
			</div>
		{/if}
		{* Краткие описания элементов *}
		{if count($items)}
			<div class="categories">
				{foreach $items as $item}
					<div>
						{if $item.pic}
							<a rel="fancybox" href="{$item.mpic}" title="<strong>{$item.name}</strong><br>{$item.full_cont}">
								<img src="{$item.spic}" alt="{$item.name}">
							</a>
						{/if}
						<div class="center">{$item.name}</div>
					</div>
				{/foreach}
			</div>
			{* Пагинация *}
			{include file="paginator.tpl"}
		{else}
			{if $queries[1]}
				<center>{$l.gallery2}</center>
			{/if}
		{/if}
	{/if}
{/strip}