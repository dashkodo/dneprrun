<?php

if (!defined('PARENT_FILE')) {
	die('Running banned');
}

$items = select_rows($run_block['backend'] . '-' . $current_language, '`disp`=1', '`sort`, `id` DESC', '0, ' . $run_block['settings']['amount_to_block']);
if (count($items)) {
	foreach ($items as &$item) {
		if ($item['pic']) {
			$item['opic'] = $host . 'data/' . $run_block['backend'] . '/o-' . $item['pic'];
			$item['mpic'] = $host . 'data/' . $run_block['backend'] . '/m-' . $item['pic'];
			$item['spic'] = $host . 'data/' . $run_block['backend'] . '/s-' . $item['pic'];
		}
		$item['datetime'] = datetime($item['datetime']);
		$cat = select_row($run_block['backend'] . '-categories-' . $current_language, $item['cid']);
		$item['url'] = $cat['url'] . '/' . $item['url'];
	}
}
$smarty->assign('block_items', $items);
$smarty->assign('block_cats', get_categories($run_block['backend']));
?>