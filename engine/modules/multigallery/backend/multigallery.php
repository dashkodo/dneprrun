<?php

if (!defined('PARENT_FILE')) {
	die('Running banned');
}

// sql
query("CREATE TABLE IF NOT EXISTS `" . $module['backend'] . '-' . $current_language . "` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cid` int(11) DEFAULT '1',
  `disp` int(11) DEFAULT '1',
  `datetime` datetime DEFAULT NULL,
  `name` text,
  `url` text,
  `title` text,
  `key` text,
  `desc` text,
  `tags` text,
  `short_cont` text,
  `full_cont` text,
  `pic` text,
  `crop` text,
  `sort` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;");

query("CREATE TABLE IF NOT EXISTS `" . $module['backend'] . '-categories-' . $current_language . "` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `disp` int(11) DEFAULT '1',
  `parent` int(11) DEFAULT '0',
  `name` text,
  `url` text,
  `title` text,
  `key` text,
  `desc` text,
  `tags` text,
  `short_cont` text,
  `full_cont` text,
  `pic` text,
  `sort` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;");

if ($module['settings']['comments']) {
	query("CREATE TABLE IF NOT EXISTS `" . $module['backend'] . "-comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) DEFAULT NULL,
  `disp` int(11) DEFAULT NULL,
  `name` text,
  `email` text,
  `comment` text,
  `datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;");
}

$upload_path = upload_path($module['backend']);
include_once(CLASS_PATH . 'WideImage/WideImage.php');

$module['settings']['medium_width'] = $module['settings']['medium_width'] ? $module['settings']['medium_width'] : 220; 
$module['settings']['medium_height'] = $module['settings']['medium_height'] ? $module['settings']['medium_height'] : 165;
$module['settings']['small_width'] = $module['settings']['small_width'] ? $module['settings']['small_width'] : 50;
$module['settings']['small_height'] =$module['settings']['small_height'] ? $module['settings']['small_height'] : 50;

function del_sub_category($parent, $language) {
	global $module;
	global $upload_path;
	$cats = select_rows($module['backend'] . '-categories-' . $language, '`parent`=' . $parent);
	if (!empty($cats)) {
		foreach ($cats as $cat) {
			if ($cat['pic']) {
				unlink($upload_path . $cat['pic']);
			}
			$items = select_rows($module['backend'] . '-' . $language, 'cid=' . $cat['id']);
			foreach ($items as $item) {
				if ($item['pic']) {
					unlink($upload_path . 'o-' . $item['pic']);
					unlink($upload_path . 'm-' . $item['pic']);
					unlink($upload_path . 's-' . $item['pic']);
				}
				delete_row($module['backend'] . '-' . $language, $item['id']);
			}
			delete_row($module['backend'] . '-categories-' . $language, $cat['id']);
			del_sub_category($cat['id'], $language);
		}
	}
}

if (isset($_FILES['add_photo'])) {
	$cid = (int) $queries[3];
    $images = array(); 
        foreach($_FILES as $fieldname => $fieldvalue) 
         foreach($fieldvalue as $paramname => $paramvalue) 
             foreach((array)$paramvalue as $index => $value) 
                 $images[$fieldname][$index][$paramname] = $value;
    
    if(count($images['add_photo'])){
        foreach($images['add_photo'] as $_image){
        	$img = uniqid() . '.';
        	$img .= pathinfo($_image['name'], PATHINFO_EXTENSION);
        	$img = strtolower($img);
            if (move_uploaded_file($_image['tmp_name'], $upload_path . 'o-' . $img)) {
            	$image = WideImage::load($upload_path . 'o-' . $img);
                if($module['settings']['medium_width'] && $module['settings']['medium_height']){
                	$medium = $image->resize($module['settings']['medium_width'], $module['settings']['medium_height']);
                	$medium->saveToFile($upload_path . 'm-' . $img);
                }
                if($module['settings']['small_width'] && $module['settings']['small_height']){
                	$small = $image->resize($module['settings']['small_width'], $module['settings']['small_height']);
                	$small->saveToFile($upload_path . 's-' . $img);
                }
            	$item = array(
            		'cid' => $cid,
            		'name' => pathinfo($_image['name'], PATHINFO_BASENAME),
            		'datetime' => $datetime,
            		'pic' => $img
            	);
            	$id = insert_row($module['backend'] . '-' . $current_language, $item);
            }
        }
    }
	redirect();
}

if ($queries[2] == 'edit') {
	$cid = (int) $queries[3];
	if ($cid) {
		$cat = select_row($module['backend'] . '-categories-' . $current_language, $cid);
	} else {
		$cat = array(
			'disp' => 1,
			'sort' => 0
		);
	}
	if (isset($_POST['cat'])) {
		if (!empty($_POST['cat']['name'])) {
			if ($_FILES['cat']['size']['pic']) {
				$img = uniqid() . '.';
				$img .= pathinfo($_FILES['cat']['name']['pic'], PATHINFO_EXTENSION);
				$img = strtolower($img);
				if (move_uploaded_file($_FILES['cat']['tmp_name']['pic'], $upload_path . $img)) {
					$_POST['cat']['pic'] = $img;
				}
			}
			if ($cid) {
				update_row($module['backend'] . '-categories-' . $current_language, $_POST['cat'], $cid);
				if ($settings['default_language'] == $current_language) {
					foreach ($settings['languages'] as $language) {
						if ($language != $current_language) {
							update_row($module['backend'] . '-categories-' . $language, array(
								'url' => $_POST['cat']['url'],
								'disp' => $_POST['cat']['disp'],
								'parent' => $_POST['cat']['parent'],
								'sort' => $_POST['cat']['sort']
									), $cid);
						}
					}
				}
				redirect();
			} else {
				if ($settings['default_language'] == $current_language) {
					$cid = insert_row($module['backend'] . '-categories-' . $current_language, $_POST['cat']);
					$_POST['cat']['id'] = $cid;
					unset($_POST['cat']['pic']);
					foreach ($settings['languages'] as $language) {
						if ($language != $current_language) {
							insert_row($module['backend'] . '-categories-' . $language, $_POST['cat']);
						}
					}
				}
				redirect('/' . $cid);
			}
		} else {
			$cat = $_POST['cat'];
		}
	}
	if ($queries[4] == 'delete-pic') {
		unlink($upload_path . $queries[5]);
		update_row($module['backend'] . '-categories-' . $current_language, array('pic' => null), $cid);
		redirect(-2);
	}
	$smarty->assign('cat', $cat);
}

$smarty->assign('cats', get_categories($module['backend']));

if ($queries[2] == 'delete') {
	$cid = (int) $queries[3];
	if ($settings['languages']) {
		foreach ($settings['languages'] as $language) {
			$cat = select_row($module['backend'] . '-categories-' . $language, $cid);
			if ($cat['pic']) {
				unlink($upload_path . $cat['pic']);
			}
			$items = select_rows($module['backend'] . '-' . $language, 'cid=' . $cat['id']);
			foreach ($items as $item) {
				if ($item['pic']) {
					unlink($upload_path . 'o-' . $item['pic']);
					unlink($upload_path . 'm-' . $item['pic']);
					unlink($upload_path . 's-' . $item['pic']);
				}
				delete_row($module['backend'] . '-' . $language, $item['id']);
			}
			delete_row($module['backend'] . '-categories-' . $language, $cat['id']);
			del_sub_category($cat['id'], $language);
		}
	} else {
		$cat = select_row($module['backend'] . '-categories-' . $current_language, $cid);
		if ($cat['pic']) {
			unlink($upload_path . $cat['pic']);
		}
		$items = select_rows($module['backend'] . '-' . $current_language, 'cid=' . $cat['id']);
		foreach ($items as $item) {
			if ($item['pic']) {
				unlink($upload_path . 'o-' . $item['pic']);
					unlink($upload_path . 'm-' . $item['pic']);
					unlink($upload_path . 's-' . $item['pic']);
			}
			delete_row($module['backend'] . '-' . $current_language, $item['id']);
		}
		delete_row($module['backend'] . '-categories-' . $current_language, $cat['id']);
		del_sub_category($cat['id'], $current_language);
	}

	redirect(-2);
}

if (isset($_POST['items'])) {
	foreach ($_POST['items'] as $id => $value) {
		update_row($module['backend'] . '-' . $current_language, $value, $id);
		if ($settings['default_language'] == $current_language) {
			foreach ($settings['languages'] as $language) {
				if ($language != $current_language) {
					update_row($module['backend'] . '-' . $language, array(
						'disp' => $value['disp']
							), $id);
				}
			}
		}
	}
	redirect();
}

if ($queries[2] == 'cat') {
	$cid = (int) $queries[3];
	if ($queries[4] == 'delete') {
		$id = (int) $queries[5];
		if ($settings['languages']) {
			foreach ($settings['languages'] as $language) {
				$item = select_row($module['backend'] . '-' . $language, $id);
				if ($item['pic']) {
					unlink($upload_path . 'o-' . $item['pic']);
					unlink($upload_path . 'm-' . $item['pic']);
					unlink($upload_path . 's-' . $item['pic']);
				}
				delete_row($module['backend'] . '-' . $language, $id);
			}
		} else {
			$item = select_row($module['backend'] . '-' . $current_language, $id);
			if ($item['pic']) {
				unlink($upload_path . 'o-' . $item['pic']);
					unlink($upload_path . 'm-' . $item['pic']);
					unlink($upload_path . 's-' . $item['pic']);
			}
			delete_row($module['backend'] . '-' . $current_language, $id);
		}

		redirect(-2);
	}
	if ($queries[4] == 'edit') {
		$id = (int) $queries[5];
		if ($id) {
			if (!select_row($module['backend'] . '-' . $current_language, $id)) {
				$copy = select_row($module['backend'] . '-' . $settings['default_language'], $id);
				unset($copy['pic']);
				insert_row($module['backend'] . '-' . $current_language, $copy);
			}
			$item = select_row($module['backend'] . '-' . $current_language, $id);
			if ($_POST['item']['name']) {
				update_row($module['backend'] . '-' . $current_language, $_POST['item'], $id);
				if ($settings['default_language'] == $current_language) {
					foreach ($settings['languages'] as $language) {
						if ($language != $current_language) {
							update_row($module['backend'] . '-' . $language, array(
								'url' => $_POST['item']['url'],
								'disp' => $_POST['item']['disp'],
								'datetime' => $_POST['item']['datetime']
									), $id);
						}
					}
				}
				redirect(-2);
			}
		} else {
			$item = array(
				'disp' => 1,
				'datetime' => $datetime,
				'sort' => 0
			);
			if ($_POST['item']['name']) {
				$id = insert_row($module['backend'] . '-' . $current_language, $_POST['item']);
				redirect('/' . $id);
			}
		}
		if ($queries[6] == 'delete-pic') {
			update_row($module['backend'] . '-' . $current_language, array('pic' => null), $id);
            unlink($upload_path . 'o-' . $queries[5]);
					unlink($upload_path . 'm-' . $queries[5]);
					unlink($upload_path . 's-' . $queries[5]);
			redirect(-2);
		}
	}

	$smarty->assign('cat', select_row($module['backend'] . '-categories-' . $current_language, $cid));

	$smarty->assign('item', $item);
	$smarty->assign('items', select_rows($module['backend'] . '-' . $current_language, '`cid`=' . $cid, 'sort, id DESC'));
}

if ($queries[2] == 'settings') {
	if ($_POST['module']) {
		if ($_FILES['module']['size']['pic']) {
			if ($module['pic']) {
				unlink($upload_path . $module['pic']);
			}
			$img = uniqid() . '.';
			$img .= pathinfo($_FILES['module']['name']['pic'], PATHINFO_EXTENSION);
			$img = strtolower($img);
			if (move_uploaded_file($_FILES['module']['tmp_name']['pic'], $upload_path . $img)) {
				$_POST['module']['pic'] = $img;
			}
		}
		$data = $_POST['module'];
		if (isset($_POST['settings'])) {
			$data = array('settings' => $_POST['settings']) + $data;
		}
		update_row('modules-' . $current_language, $data, $module['id']);
		if ($settings['default_language'] == $current_language) {
			foreach ($settings['languages'] as $language) {
				if ($language != $current_language) {
					update_row('modules-' . $language, array(
						'frontend' => $_POST['module']['frontend'],
						'settings' => $_POST['settings']
							), $module['id']);
				}
			}
		}
		redirect();
	}
	if ($queries[3] == 'delete-pic') {
		unlink($upload_path . $queries[4]);
		unset($module['settings']['pic']);
		update_row('modules-' . $current_language, array('pic' => null), $module['id']);
		redirect(-2);
	}
}
?>