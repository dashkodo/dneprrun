<?php

if (!defined('PARENT_FILE')) {
	die('Running banned');
}

if (!empty($_POST['transfer'])) {
	if ($_POST['transfer']['old'] && $_POST['transfer']['new']) {
		$old = escape_string($_POST['transfer']['old']);
		$new = escape_string($_POST['transfer']['new']);
		foreach ($show_tables as $table) {
			$q = query("SHOW COLUMNS FROM `" . $table . "`;");
			while ($row = fetch_assoc($q)) {
				if ($row['Type'] == 'text') {
					query("UPDATE `" . $table . "` SET `" . $row['Field'] . "`=REPLACE(`" . $row['Field'] . "`, '" . $old . "', '" . $new . "');");
				}
			}
		}
		redirect();
	}
	$smarty->assign('transfer', $_POST['transfer']);
}

$smarty->assign('cashe_size', dir_size(CASHE_PATH));
if ($queries[2] == 'clear-cashe') {
	cldir_recursive(CASHE_PATH);
	redirect(-1);
}
?>