<?php

if (!defined('PARENT_FILE')) {
	die('Running banned');
}

// sql
query("CREATE TABLE IF NOT EXISTS `" . $module['backend'] . '-' . $current_language . "` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cid` int(11) DEFAULT '1',
  `disp` int(11) DEFAULT '1',
  `datetime` datetime DEFAULT NULL,
  `name` text,
  `url` text,
  `sort` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;");

query("CREATE TABLE IF NOT EXISTS `" . $module['backend'] . '-categories-' . $current_language . "` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `disp` int(11) DEFAULT '1',
  `parent` int(11) DEFAULT '0',
  `name` text,
  `sort` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;");

$upload_path = upload_path($module['backend']);

function del_sub_category($parent, $language) {
	global $module;
	global $upload_path;
	$cats = select_rows($module['backend'] . '-categories-' . $language, '`parent`=' . $parent);
	if (!empty($cats)) {
		foreach ($cats as $cat) {
			$items = select_rows($module['backend'] . '-' . $language, 'cid=' . $cat['id']);
			foreach ($items as $item) {
				delete_row($module['backend'] . '-' . $language, $item['id']);
			}
			delete_row($module['backend'] . '-categories-' . $language, $cat['id']);
			del_sub_category($cat['id'], $language);
		}
	}
}

if ($queries[2] == 'edit') {
	$cid = (int) $queries[3];
	if ($cid) {
		$cat = select_row($module['backend'] . '-categories-' . $current_language, $cid);
	} else {
		$cat = array(
			'disp' => 1,
			'sort' => 0
		);
	}
	if (isset($_POST['cat'])) {
		if (!empty($_POST['cat']['name'])) {
			if ($cid) {
				update_row($module['backend'] . '-categories-' . $current_language, $_POST['cat'], $cid);
				if ($settings['default_language'] == $current_language) {
					foreach ($settings['languages'] as $language) {
						if ($language != $current_language) {
							update_row($module['backend'] . '-categories-' . $language, array(
								'url' => $_POST['cat']['url'],
								'disp' => $_POST['cat']['disp'],
								'parent' => $_POST['cat']['parent'],
								'sort' => $_POST['cat']['sort']
									), $cid);
						}
					}
				}
				redirect();
			} else {
				if ($settings['default_language'] == $current_language) {
					$cid = insert_row($module['backend'] . '-categories-' . $current_language, $_POST['cat']);
					$_POST['cat']['id'] = $cid;
				}
				redirect('/' . $cid);
			}
		} else {
			$cat = $_POST['cat'];
		}
	}
	$smarty->assign('cat', $cat);
}

$smarty->assign('cats', get_categories($module['backend']));

if ($queries[2] == 'delete') {
	$cid = (int) $queries[3];
	if ($settings['languages']) {
		foreach ($settings['languages'] as $language) {
			$cat = select_row($module['backend'] . '-categories-' . $language, $cid);
			$items = select_rows($module['backend'] . '-' . $language, 'cid=' . $cat['id']);
			foreach ($items as $item) {
				delete_row($module['backend'] . '-' . $language, $item['id']);
			}
			delete_row($module['backend'] . '-categories-' . $language, $cat['id']);
			del_sub_category($cat['id'], $language);
		}
	} else {
		$cat = select_row($module['backend'] . '-categories-' . $current_language, $cid);
		$items = select_rows($module['backend'] . '-' . $current_language, 'cid=' . $cat['id']);
		foreach ($items as $item) {
			delete_row($module['backend'] . '-' . $current_language, $item['id']);
		}
		delete_row($module['backend'] . '-categories-' . $current_language, $cat['id']);
		del_sub_category($cat['id'], $current_language);
	}

	redirect(-2);
}

if (isset($_POST['items'])) {
	foreach ($_POST['items'] as $id => $value) {
		update_row($module['backend'] . '-' . $current_language, $value, $id);
		if ($settings['default_language'] == $current_language) {
			foreach ($settings['languages'] as $language) {
				if ($language != $current_language) {
					update_row($module['backend'] . '-' . $language, array(
						'disp' => $value['disp']
							), $id);
				}
			}
		}
	}
	redirect();
}

if ($queries[2] == 'cat') {
	$cid = (int) $queries[3];
	if ($queries[4] == 'delete') {
		$id = (int) $queries[5];
		if ($settings['languages']) {
			foreach ($settings['languages'] as $language) {
				$item = select_row($module['backend'] . '-' . $language, $id);
				delete_row($module['backend'] . '-' . $language, $id);
			}
		} else {
			$item = select_row($module['backend'] . '-' . $current_language, $id);
			delete_row($module['backend'] . '-' . $current_language, $id);
		}

		redirect(-2);
	}
	if ($queries[4] == 'edit') {
		$id = (int) $queries[5];
		if ($id) {
			if (!select_row($module['backend'] . '-' . $current_language, $id)) {
				$copy = select_row($module['backend'] . '-' . $settings['default_language'], $id);
				insert_row($module['backend'] . '-' . $current_language, $copy);
			}
			$item = select_row($module['backend'] . '-' . $current_language, $id);
			if ($_POST['item']['name']) {
				pdate_row($module['backend'] . '-' . $current_language, $_POST['item'], $id);
				if ($settings['default_language'] == $current_language) {
					foreach ($settings['languages'] as $language) {
						if ($language != $current_language) {
							update_row($module['backend'] . '-' . $language, array(
								'url' => $_POST['item']['url'],
								'disp' => $_POST['item']['disp'],
								'datetime' => $_POST['item']['datetime']
									), $id);
						}
					}
				}
				redirect(-2);
			}
		} else {
			$item = array(
				'disp' => 1,
				'datetime' => $datetime,
				'sort' => 0
			);
			if ($_POST['item']['name']) {
				$id = insert_row($module['backend'] . '-' . $current_language, $_POST['item']);
				redirect('/' . $id);
			}
		}
	}

	$smarty->assign('cat', select_row($module['backend'] . '-categories-' . $current_language, $cid));

	$smarty->assign('item', $item);
	$smarty->assign('items', select_rows($module['backend'] . '-' . $current_language, '`cid`=' . $cid, 'sort, id DESC'));
}

if ($queries[2] == 'settings') {
	if ($_POST['module']) {
		$data = $_POST['module'];
		if (isset($_POST['settings'])) {
			$data = array('settings' => $_POST['settings']) + $data;
		}
		update_row('modules-' . $current_language, $data, $module['id']);
		if ($settings['default_language'] == $current_language) {
			foreach ($settings['languages'] as $language) {
				if ($language != $current_language) {
					update_row('modules-' . $language, array(
						'frontend' => $_POST['module']['frontend'],
						'settings' => $_POST['settings']
							), $module['id']);
				}
			}
		}
		redirect();
	}
}
?>