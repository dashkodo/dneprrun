{strip}
	{if $item}
		<div class="datetime">{$item.datetime}</div>
	{/if}
	{* Заголовок *}
	<h1>{$name}</h1>
	{* Хлебные крошки *}
	{include file="breadcrumbs.tpl"}
	{* Подробное описание элемента *}
	{if $item}
		<div class="item">
			{$item.full_cont}
		</div>
		{* Рекомендуемые *}
		{if count($item.related)}
			<h3>{$l.news3}</h3>
			<div class="categories">
				{foreach $item.related as $related}
					<div>
						<a href="{$host}{$language_prefix}{$module.frontend}/{$related.url}">{$related.name}</a>
						<div>{$related.short_cont}</div>
					</div>
				{/foreach}
			</div>
		{/if}
	{else}
		{if empty($queries[1])}
			{if $module.pic}
				<img class="module-pic" src="{$host}data/{$module.backend}/{$module.pic}" alt="{$module.name}">
			{/if}
			{if $module.full_cont}
				<div class="full-cont">{$module.full_cont}</div>
			{/if}
		{/if}
		{* Полное описание категории *}
		{if $current_page==1}
			<div class="full-cont">{$cat.full_cont}</div>
		{/if}
		{* Категории *}
		{if count($cats) && $current_page==1}
			<h3>{$l.category}</h3>
			<div class="categories">
				{foreach $cats as $c}
					<div>
						{if $c.pic}
							<a href="{$host}{$language_prefix}{$module.frontend}/{$c.url}">
								<img src="{$c.pic}" alt="{$c.name}">
							</a>
						{/if}
						<a class="center" href="{$host}{$language_prefix}{$module.frontend}/{$c.url}">{$c.name}</a>
						<div>{$c.short_cont}</div>
					</div>
				{/foreach}
			</div>
		{/if}
		{* Краткие описания элементов *}
		{if count($items)}
			<div class="items">
				{foreach $items as $item}
					<div>
						<div class="datetime">{$item.datetime}</div>
						<a class="link" href="{$host}{$language_prefix}{$module.frontend}/{$cat.url}/{$item.url}">{$item.name}</a>
						{if $item.pic}
							<a class="pic" href="{$host}{$language_prefix}{$module.frontend}/{$cat.url}/{$item.url}">
								<img src="{$item.pic}" alt="{$item.name}">
							</a>
						{/if}
						{$item.short_cont}
						<div class="clear"></div>
					</div>
				{/foreach}
			</div>
			{* Пагинация *}
			{include file="paginator.tpl"}
		{else}
			{if $queries[1]}
				<center>{$l.news2}</center>
			{/if}
		{/if}
	{/if}
{/strip}