<?php

if (!defined('PARENT_FILE')) {
	die('Running banned');
}

$smarty->assign('captcha', true);
if (!empty($_POST['feedback'])) {
	$feedback = array_map_recursive('trim', $_POST['feedback']);
	$feedback = array_map_recursive('strip_tags', $feedback);
	if ($captcha && $feedback['f_1'] && $feedback['f_4']) {
		$massage = $l['feedback12'] . " - " . $feedback['f_1'] . "<br>";
		$massage .= $l['feedback5'] . " - " . $feedback['f_2'] . "<br>";
		$massage .= $l['feedback6'] . " - " . $feedback['f_3'] . "<br>";
		$massage .= $l['feedback7'] . " - " . $feedback['f_4'] . "<br><br>";
		$massage .= $l['feedback8'] . " - " . $feedback['f_5'] . "<br>";
		sendmail($settings['admin_mail'],false , "Обратная связь", $massage);
		$smarty->assign('ok', true);
	} else {
		$smarty->assign('feedback', $feedback);
	}
	$smarty->assign('captcha', $captcha);
}
?>