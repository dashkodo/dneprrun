{strip}
	{* Заголовок *}
	<h1>{$module.name}</h1>
	{* Хлебные крошки *}
	{include file="breadcrumbs.tpl"}
	{* Подробное описание модуля *}
	{if $module.full_cont}
		<div class="full-cont">{$module.full_cont}</div>
	{/if}
	{if $module.settings.map}
		<div class="map">{$module.settings.map}</div>
	{/if}
	<form method="post" class="feedback">
		<div class="name">{$l.feedback1}</div>
		{if $ok}<div class="ok">{$l.feedback2}</div>{/if}
		<div>*{$l.feedback3}:</div>
		<input name="feedback[f_1]" type="text" value="{$feedback.f_1}">
		<div>*{$l.feedback4}:</div>
		<input name="feedback[f_2]" type="text" value="{$feedback.f_2}">
		<div>{$l.feedback5}:</div>
		<textarea spellcheck="false" name="feedback[f_3]">{$feedback.f_3}</textarea>
		{if $settings.captcha}
			<div>*{$l.feedback6}:</div>
			<img class="captcha" src="{$host}captcha.jpg" align="middle">
			<input type="text"{if !$captcha} class="error"{/if}name="captcha">
		{/if}
		<br>
		<input type="submit" class="button" value="{$l.feedback8}">
		<span class="red"{if !$captcha} style="display: inline"{/if}>{$l.feedback7}</span>
	</form>
{/strip}
