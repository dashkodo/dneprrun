{strip}
	<div class="menu-bg">
		<menu>
			<a class="active" href="{$host}admin/{$module.backend}">{$module.name}</a>
		</menu>
	</div>
	<div id="main">
		<div class="block setting">
			<form method="post" enctype="multipart/form-data">
				<div class="string">
					<span class="left">
						Название модуля
					</span>
					<span class="right">
						<div id="module-name">
							<input class="translit input" type="text" name="module[name]" value="{$module.name}" placeholder="Название модуля">
							<div id="switch-translit">
								Обновить<br>URL
								<input type="hidden" id="url" value="">
							</div>
						</div>
					</span>
				</div>
				{*<div class="string">
					<span class="left">
						Картинка
					</span>
					<span class="right">
						<input name="module[pic]" type="file" class="input" style="width:240px;">
						{if $module.pic}
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<a href="{$host}data/{$module.backend}/{$module.pic}?{$rand}" rel="photo">
								<img src="{$host}data/{$module.backend}/{$module.pic}?{$rand}" style="max-width:58px;max-height:40px;vertical-align:middle;">
							</a>
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<a href="{$host}{$query}/delete-pic/{$module.pic}" class="del confirm" title="Удалить"></a>
						{/if}
					</span>
				</div>*}
				<div class="string">
					<span class="left">
						<a href="#" id="seo">SEO</a>
					</span>
				</div>
				<div class="seo-cont">
					{if $settings.default_language == $current_language}
						<div class="string">
							<span class="left">
								URL
							</span>
							<span class="right">
								<input name="module[frontend]" value="{$module.frontend}" type="text" class="url input">
							</span>
						</div>
					{/if}
					<div class="string">
						<span class="left">
							Title
						</span>
						<span class="right">
							<input name="module[title]" value="{$module.title}" class="input" type="text">
						</span>
					</div>
					<div class="string">
						<span class="left">
							Description
						</span>
						<span class="right">
							<textarea name="module[desc]" class="input" rows="2">{$module.desc}</textarea>
						</span>
					</div>
					<div class="string">
						<span class="left">
							Keywords
						</span>
						<span class="right">
							<textarea name="module[key]" class="input" rows="2">{$module.key}</textarea>
						</span>
					</div>
				</div>
                <div class="string">
					<span class="left">
						Телефон1
					</span>
					<span class="left">
							<input class="input" type="text" name="settings[ph1_code]" value="{$module.settings.ph1_code}" placeholder="Код">
					</span>
                    <span class="left">
							<input class="input" type="text" name="settings[ph1]" value="{$module.settings.ph1}" placeholder="Телефон">
					</span>
				</div>
                <div class="string">
					<span class="left">
						Телефон2
					</span>
					<span class="left">
							<input class="input" type="text" name="settings[ph2_code]" value="{$module.settings.ph2_code}" placeholder="Код">
					</span>
                    <span class="left">
							<input class="input" type="text" name="settings[ph2]" value="{$module.settings.ph2}" placeholder="Телефон">
					</span>
				</div>
                <div class="string">
					<span class="left">
						Телефон3
					</span>
					<span class="left">
							<input class="input" type="text" name="settings[ph3_code]" value="{$module.settings.ph3_code}" placeholder="Код">
					</span>
                    <span class="left">
							<input class="input" type="text" name="settings[ph3]" value="{$module.settings.ph3}" placeholder="Телефон">
					</span>
				</div>
                <div class="string">
					<span class="left">
						E-mail
					</span>
					<span class="left">
							<input class="input" type="text" name="settings[mail]" value="{$module.settings.mail}">
					</span>
				</div>
				<div class="string">
					<span>
						Описание<br>
						<textarea class="redactor" name="module[full_cont]">{$module.full_cont}</textarea>
					</span>
				</div>
				{*<div class="string">
					<span class="left">
						Карта
					</span>
					<span class="right">
						<textarea name="settings[map]" class="input" rows="4">{$module.settings.map}</textarea>
					</span>
				</div>*}
				<div class="string">
					<span class="center">
						<input class="button" type="submit" value="Сохранить">
					</span>
				</div>
			</form>
		</div>
		<div class="clear"></div>
	</div>
{/strip}