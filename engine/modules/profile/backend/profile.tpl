{strip}
<div class="menu-bg">
  <menu>
    <a
      {if empty($queries[2]) } class="active"{/if} href="{$host}admin/{$module.backend}">{$module.name}
    </a>
    <a
      {if $queries[2]=='members' } class="active"{/if} href="{$host}admin/{$module.backend}/members">Члены клуба
    </a>
    {if !empty($queries[2]) && $queries[2] == 'edit'}
    <a
      {if $queries[2] == 'edit'} class="active"{/if} href="{$host}admin/{$module.backend}/edit/{$user.id}">Редактирование профайла
    </a>
    {/if}

  </menu>
</div>
<div id="main">
  <div class="block setting">
    {if empty($queries[2]) || $queries[2]=='members' }
    {if $queries[2]=='members' }
    <div class="string">
      <span class="center">
        <a class="button" href="{$host}{$query}/vdot" >
          Повысить всем VDOT
        </a>
      </span>
    </div>
    {/if}
    <form method="post" enctype="multipart/form-data">
      {foreach $users as $user}
      <div class="string">
        <span>
          <div class="check-box{if $user.disp} on{/if}">
            <input name="users[{$user.id}][disp]" value="{$user.disp}" type="hidden">
          </div>
          {$user.name} ({$user.login})
          <a href="{$host}admin/{$module.backend}/del/{$user.id}" class="del confirm" title="Удалить">
          </a>
          <a href="{$host}admin/{$module.backend}/edit/{$user.id}" class="edit" title="Редактировать">
          </a>
        </span>
      </div>
      {foreachelse}
      <div class="string">
        <span class="center">
          Пользователей нет
        </span>
      </div>
      {/foreach}
      {if count($users)}
      <div class="string">
        <span class="center">
          <input class="button" type="submit" value="Сохранить">
        </span>
      </div>
      {/if}
    </form>

    {elseif  $queries[2]=='edit'}
    <div class="string">
      <span class="right">
        Профайл пользователя {$user.name} ({$user.mail})
      </span>

    </div>
    <form method="post" enctype="multipart/form-data">

      {foreach $profile as $key=>$val}
      <div class="string">
        <span class="left">
          {$key}
        </span>
        <span class="right">
          <input name="profile[{$key}]" value="{$val}" class="input" type="text" placeholder="Введите значение поля профиля пользователя" >
        </span>
      </div>
      {/foreach}
      <div class="string">
        <span class="left">
          <input name="newfield[key]" class="input" type="text" placeholder="Введите название поля профиля пользователя" >
        </span>
        <span class="right">
          <input name="newfield[value]" value="" class="input" type="text" placeholder="Введите значение поля профиля пользователя" >
        </span>
      </div>

      <div class="string">
        <span class="center">
          <input class="button" type="submit" value="Сохранить">
        </span>
      </div>
    </form>

    {/if}
  </div>
  <div class="clear">
  </div>
</div>
{/strip}