<?php

if(!defined('PARENT_FILE')){
  die('Running banned');
}

$upload_path = upload_path($module['backend']);

if(empty($queries[2])){
  
  $rows = select_rows("users", "`group` != 1 and `group` != 2", '`datetime` DESC');
  $smarty->assign('users', $rows);
    
  
  
}
elseif($queries[2] == 'members'){
  if($queries[3] == 'vdot'){
    $rows = select_rows("users", "`group` != 1 and `group` != 2 and id in (SELECT `user_id` FROM `profile` WHERE `field` = 'club' AND `value` LIKE 'DneprRun')", '`datetime` DESC');
    foreach($rows as $row){
      load_user_results($row['mail']);
    }
    redirect(-1);
  }
  
  
  $rows = select_rows("users", "`group` != 1 and `group` != 2 and id in (SELECT `user_id` FROM `profile` WHERE `field` = 'club' AND `value` LIKE 'DneprRun')", '`datetime` DESC');
  $smarty->assign('users', $rows);
  
}

elseif($queries[2] == 'edit'){
  $id = (int) $queries[3];
  
  if(isset($_POST['newfield']['key'])&& $_POST['newfield']['key']){
    set_profile_field($_POST['newfield']['key'],$_POST['newfield']['value'],$id);
  }
  
  if(isset($_POST['profile'])){
    foreach ($_POST['profile'] as $k=>$v)
    set_profile_field($k,$v,$id);
  }
  
  
  $smarty->assign('user', select_row("users",$id));
  $smarty->assign('title', 'Редактирование');
  $smarty->assign('profile', load_profile($id));
}
elseif($queries[2] == 'del'){
  $id = (int) $queries[3];
  delete_rows($module['backend'], '`user_id`='.$id);
  redirect( - 2);
}
?>