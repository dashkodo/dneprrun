<?php

if (!defined('PARENT_FILE')) {
	die('Running banned');
}

query("CREATE TABLE IF NOT EXISTS `" . $module['backend'] . '-' . $current_language . "` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `disp` int(11) DEFAULT '1',
  `name` text,
  `menu` text,
  `sort` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;");

function recursion_level($array, $parent, $level = 0) {
	if ($parent) {
		foreach ($array as $value) {
			if ($value['id'] == $parent) {
				return recursion_level($array, $value['parent'], $level += 1);
				break;
			}
		}
	} else {
		return $level;
	}
}

if (isset($_POST['items'])) {
	foreach ($_POST['items'] as $id => $item) {
		if (!empty($item['name'])) {
			update_row($module['backend'] . '-' . $current_language, $item, $id);
			if ($settings['default_language'] == $current_language) {
				foreach ($settings['languages'] as $language) {
					if ($language != $current_language) {
						update_row($module['backend'] . '-' . $language, array(
							'disp' => $item['disp']
								), $id);
					}
				}
			}
		}
	}
	redirect();
}

if ($queries[2] == 'add') {
	$smarty->assign('item', array('disp' => 1));
	if (isset($_POST['item'])) {
		if (!empty($_POST['item']['name'])) {
			$menus = array();
			foreach ($_POST['menu']['id'] as $sort => $mid) {
				if ($mid) {
					$menus[] = array(
						'id' => $mid,
						'parent' => $_POST['menu']['parent'][$sort],
						'name' => $_POST['menu']['name'][$sort],
						'link' => $_POST['menu']['link'][$sort],
					);
				}
			}
			$_POST['item']['menu'] = $menus;
			insert_row($module['backend'] . '-' . $current_language, $_POST['item']);
			redirect(-1);
		}
		$smarty->assign('user', $user);
	}
} elseif ($queries[2] == 'delete') {
	$id = (int) $queries[3];
	if ($settings['languages']) {
		foreach ($settings['languages'] as $language) {
			delete_row($module['backend'] . '-' . $language, $id);
		}
	} else {
		delete_row($module['backend'] . '-' . $current_language, $id);
	}
	redirect(-2);
} elseif ($queries[2] == 'edit') {
	$id = (int) $queries[3];

	if (!select_row($module['backend'] . '-' . $current_language, $id)) {
		$copy = select_row($module['backend'] . '-' . $settings['default_language'], $id);
		insert_row($module['backend'] . '-' . $current_language, $copy);
	}

	$item = select_row($module['backend'] . '-' . $current_language, $id);

	if ($item['menu']) {
		foreach ($item['menu'] as &$menu) {
			$menu['level'] = recursion_level($item['menu'], $menu['parent']);
		}
	}


	$smarty->assign('item', $item);

	if (isset($_POST['item'])) {
		if (!empty($_POST['item']['name'])) {
			$menus = array();
			foreach ($_POST['menu']['id'] as $sort => $mid) {
				if ($mid) {
					$menus[] = array(
						'id' => $mid,
						'parent' => $_POST['menu']['parent'][$sort],
						'name' => $_POST['menu']['name'][$sort],
						'link' => $_POST['menu']['link'][$sort],
					);
				}
			}
			$_POST['item']['menu'] = $menus;
			update_row($module['backend'] . '-' . $current_language, $_POST['item'], $id);
			if ($settings['default_language'] == $current_language) {
				foreach ($settings['languages'] as $language) {
					if ($language != $current_language) {
						update_row($module['backend'] . '-' . $language, array(
							'disp' => $_POST['item']['disp']
								), $id);
					}
				}
			}
			redirect(-2);
		}
		$smarty->assign('item', $_POST['item']);
	}
}


$smarty->assign('items', select_rows($module['backend'] . '-' . $current_language, false, 'sort, id desc'));
$smarty->assign('title', $module['name']);
?>