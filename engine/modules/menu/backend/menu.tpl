{strip}
	<div class="menu-bg">
		<menu>
			<a class="active" href="{$host}admin/{$module.backend}">{$module.name}</a>
		</menu>
	</div>
	<div id="main">
		<div class="block setting">
			{if empty($queries[2])}
				<div class="string">
					{if $settings.default_language == $current_language}
						<span class="center">
							<a href="{$host}{$query}/add" class="button">Добавить меню</a>
						</span>
					{/if}
				</div>
				<form method="post" enctype="multipart/form-data">
					{foreach $items as $item}
						<div class="string">
							<span>
								{if $settings.default_language == $current_language}
									<div class="check-box{if $item.disp} on{/if}">
										<input name="items[{$item.id}][disp]" value="{$item.disp}" type="hidden">
									</div>
									&nbsp;&nbsp;
								{/if}
								Название
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input name="items[{$item.id}][name]" value="{$item.name}" class="br4 input" type="text" style="width:420px;">
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								<input value="{ldelim}$m{$item.id}{rdelim}" class="br4 input" style="width:100px;" type="text">
								<a href="{$host}{$query}/delete/{$item.id}" class="del confirm" title="Удалить"></a>
								<a href="{$host}{$query}/edit/{$item.id}" class="edit" title="Редактировать"></a>
							</span>
						</div>
					{foreachelse}
						<div class="string">
							<span class="center">
								Меню нет
							</span>
						</div>
					{/foreach}
					{if count($items)}
						<div class="string">
							<span class="center">
								<input class="button" type="submit" value="Сохранить">
							</span>
						</div>
					{/if}
				</form>
			{elseif $queries[2]=='add' || $queries[2]=='edit'}
				<form method="post" enctype="multipart/form-data">
					<div class="string">
						<span class="left">
							{if $settings.default_language == $current_language}
								<div class="check-box{if $item.disp} on{/if}">
									<input name="item[disp]" value="{$item.disp}" type="hidden">
								</div>
								Активность
							{/if}
						</span>
						{if $item.id}
							<span class="right">
								<input value="{ldelim}$m{$item.id}{rdelim}" class="br4 input" style="width:100px;" type="text">
							</span>
						{/if}
					</div>
					<div class="string">
						<span class="left">
							Название
						</span>
						<span class="right">
							<input name="item[name]" value="{$item.name}" class="br4 input" type="text">
						</span>
					</div>
					<div class="string">
						<span class="center">
							<div id="menu-builder">
								<div class="sample">
									<div class="parent">
										<div class="item">
											<input name="menu[id][]" type="hidden" value="0">
											<input name="menu[parent][]" type="hidden" value="0">
											<span class="up-down">
												<a href="#up">▲</a>
												<a href="#down">▼</a>
											</span>
											&nbsp;&nbsp;&nbsp;
											<input name="menu[name][]" type="text" class="input" placeholder="Пункт меню">
											&nbsp;&nbsp;&nbsp;
											<input name="menu[link][]" type="text" class="input" value="#">
											<span class="del-add">
												<a href="#del">✖</a>
												<a href="#add">✚</a>
											</span>
										</div>
									</div>
								</div>
								{foreach $item.menu as $l0}
									{if isset($l0.level) && $l0.level==0}
										<div class="parent">
											<div class="item">
												<input name="menu[id][]" type="hidden" value="{$l0.id}">
												<input name="menu[parent][]" type="hidden" value="{$l0.parent}">
												<span class="up-down">
													<a href="#up">▲</a>
													<a href="#down">▼</a>
												</span>
												&nbsp;&nbsp;&nbsp;
												<input name="menu[name][]" type="text" class="input" value="{$l0.name}">
												&nbsp;&nbsp;&nbsp;
												<input name="menu[link][]" type="text" class="input" value="{$l0.link}">
												<span class="del-add">
													<a href="#del">✖</a>
													<a href="#add">✚</a>
												</span>
											</div>
											{foreach $item.menu as $l1}
												{if $l1.level==1 && $l0.id==$l1.parent}
													<div class="parent">
														<div class="item">
															<input name="menu[id][]" type="hidden" value="{$l1.id}">
															<input name="menu[parent][]" type="hidden" value="{$l1.parent}">
															<span class="up-down">
																<a href="#up">▲</a>
																<a href="#down">▼</a>
															</span>
															&nbsp;&nbsp;&nbsp;
															<input name="menu[name][]" type="text" class="input" value="{$l1.name}">
															&nbsp;&nbsp;&nbsp;
															<input name="menu[link][]" type="text" class="input" value="{$l1.link}">
															<span class="del-add">
																<a href="#del">✖</a>
																<a href="#add">✚</a>
															</span>
														</div>
														{foreach $item.menu as $l2}
															{if $l2.level==2 && $l1.id==$l2.parent}
																<div class="parent">
																	<div class="item">
																		<input name="menu[id][]" type="hidden" value="{$l2.id}">
																		<input name="menu[parent][]" type="hidden" value="{$l2.parent}">
																		<span class="up-down">
																			<a href="#up">▲</a>
																			<a href="#down">▼</a>
																		</span>
																		&nbsp;&nbsp;&nbsp;
																		<input name="menu[name][]" type="text" class="input" value="{$l2.name}">
																		&nbsp;&nbsp;&nbsp;
																		<input name="menu[link][]" type="text" class="input" value="{$l2.link}">
																		<span class="del-add">
																			<a href="#del">✖</a>
																			<a href="#add">✚</a>
																		</span>
																	</div>
																	{foreach $item.menu as $l3}
																		{if $l3.level==3 && $l2.id==$l3.parent}
																			<div class="parent">
																				<div class="item">
																					<input name="menu[id][]" type="hidden" value="{$l3.id}">
																					<input name="menu[parent][]" type="hidden" value="{$l3.parent}">
																					<span class="up-down">
																						<a href="#up">▲</a>
																						<a href="#down">▼</a>
																					</span>
																					&nbsp;&nbsp;&nbsp;
																					<input name="menu[name][]" type="text" class="input" value="{$l3.name}">
																					&nbsp;&nbsp;&nbsp;
																					<input name="menu[link][]" type="text" class="input" value="{$l3.link}">
																					<span class="del-add">
																						<a href="#del">✖</a>
																						<a href="#add">✚</a>
																					</span>
																				</div>
																				{foreach $item.menu as $l4}
																					{if $l4.level==4 && $l3.id==$l4.parent}
																						<div class="parent">
																							<div class="item">
																								<input name="menu[id][]" type="hidden" value="{$l4.id}">
																								<input name="menu[parent][]" type="hidden" value="{$l4.parent}">
																								<span class="up-down">
																									<a href="#up">▲</a>
																									<a href="#down">▼</a>
																								</span>
																								&nbsp;&nbsp;&nbsp;
																								<input name="menu[name][]" type="text" class="input" value="{$l4.name}">
																								&nbsp;&nbsp;&nbsp;
																								<input name="menu[link][]" type="text" class="input" value="{$l4.link}">
																								<span class="del-add">
																									<a href="#del">✖</a>
																									<a href="#add">✚</a>
																								</span>
																							</div>
																						</div>
																					{/if}
																				{/foreach}
																			</div>
																		{/if}
																	{/foreach}
																</div>
															{/if}
														{/foreach}
													</div>
												{/if}
											{/foreach}
										</div>
									{/if}
								{/foreach}
								<a href="#add" class="button">Добавить пункт</a>
							</div>
						</span>
					</div>
					<div class="string">
						<span class="center">
							<input class="button" type="submit" value="Сохранить">
						</span>
					</div>
				</form>
			{/if}
		</div>
		<div class="clear"></div>
	</div>
{/strip}