<?php

if (!defined('PARENT_FILE')) {
	die('Running banned');
}

if (empty($queries[1])) {
	$cat['id'] = 0;
} else {
	$cat = select_row($module['backend'] . '-categories-' . $current_language, false, 'url="' . escape_string($queries[1]) . '" AND disp=1');
	$smarty->assign('cat', $cat);
	set_seo($cat['name'], $cat['title'], $cat['desc'], $cat['key']);
}
$cats = select_rows($module['backend'] . '-categories-' . $current_language, 'parent=' . $cat['id'] . ' AND disp=1', 'sort, id');
foreach ($cats as &$c) {
	if ($c['pic']) {
		$c['pic'] = $host . 'data/' . $module['backend'] . '/' . $c['pic'];
	}
}
$smarty->assign('cats', $cats);

if (isset($queries[2])) {
	if ($item = select_row($module['backend'] . '-' . $current_language, false, '`url`="' . escape_string($queries[2]) . '" AND `disp`=1')) {
		if ($item['pic']) {
			$item['pic'] = $host . 'data/' . $module['backend'] . '/' . $item['pic'];
		}
		$item['datetime'] = datetime($item['datetime']);
		$smarty->assign('item', $item);
		set_seo($item['name'], $item['title'], $item['desc'], $item['key']);
	} elseif (intval($queries[2])) {
		$current_page = (int) $queries[2];
	} else {
		error_404();
	}
} else {
	$current_page = 1;
}

if ($current_page) {
	if ($module['settings']['amount_to_page']) {
		$from = ($current_page - 1) * $module['settings']['amount_to_page'];
		$items = select_rows($module['backend'] . '-' . $current_language, 'cid=' . $cat['id'] . ' AND disp=1', 'sort, id DESC', $from . ', ' . $module['settings']['amount_to_page']);
	} else {
		$items = select_rows($module['backend'] . '-' . $current_language, 'cid=' . $cat['id'] . ' AND disp=1', 'sort, id DESC');
	}
	if (count($items)) {
		foreach ($items as &$item) {
            preg_match('~v=([A-Za-z0-9_]+)~', $item['url'], $match); 
            $item['video'] = $match[1];
			if ($item['pic']) {
				$item['pic'] = $host . 'data/' . $module['backend'] . '/' . $item['pic'];
			}else{
                $item['pic'] = 'http://img.youtube.com/vi/' . $item['video'] . '/mqdefault.jpg';
			}
			$item['datetime'] = datetime($item['datetime']);
		}
		$smarty->assign('items', $items);
		$items_count = count_rows($module['backend'] . '-' . $current_language, 'cid=' . $cat['id'] . ' AND disp=1');
		if ($module['settings']['amount_to_page'] < $items_count) {
			$paginator = ceil($items_count / $module['settings']['amount_to_page']);
			$smarty->assign('paginator', $paginator);
		}
	} else {
		//error_404();
	}
}
$smarty->assign('current_page', $current_page);
$smarty->assign('name', $seo['name']);
?>