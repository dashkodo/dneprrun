<?php

if (!defined('PARENT_FILE')) {
	die('Running banned');
}

if ($run_block['settings']['amount_to_block']) {
	$items = select_rows($run_block['backend'] . '-' . $current_language, '`disp`=1', '`sort`, `id` DESC', '0, ' . $run_block['settings']['amount_to_block']);
} else {
	$items = select_rows($run_block['backend'] . '-' . $current_language, '`disp`=1', '`sort`, `id` DESC');
}
if (count($items)) {
	foreach ($items as &$item) {
		if ($item['pic']) {
			$item['pic'] = $host . 'data/' . $run_block['backend'] . '/' . $item['pic'];
		}
		$item['datetime'] = datetime($item['datetime']);
	}
}
$smarty->assign('block_items', $items);
?>