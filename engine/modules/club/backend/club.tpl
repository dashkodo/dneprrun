{strip}
{$tpl_path = "engine/modules/`$module.backend`/backend/"}

<div class="menu-bg">
  <menu>
    <a
      {if $queries[2]!='settings' && $queries[2]!='comments' && $queries[2]!='members'} class="active"{/if} href="{$host}admin/{$module.backend}">{$module.name}
    </a>
    {if $module.settings.comments}
    <a
      {if $queries[2]=='comments'} class="active"{/if} href="{$host}admin/{$module.backend}/comments">Коментарии{if $module.comments} ({$module.comments}){/if}
    </a>
    {/if}
    <a
      {if $queries[2]=='settings'} class="active"{/if} href="{$host}admin/{$module.backend}/settings">Настройки
    </a>
    <a
      {if $queries[2]=='members'} class="active"{/if} href="{$host}admin/{$module.backend}/members">Члены клуба
    </a>
  </menu>
</div>
<div id="main">
  {if !$queries[2]}
  <div id="sidebar">
    <div class="block pages">
      {foreach $items as $i}
      <div class="page-link{if $i.id == $item.id} active{/if}">
        <a href="{$host}admin/{$module.backend}/edit/{$i.id}">
          {if !$i.approved}
          <span class="request">
            ?
          </span>{/if}{$i.name}
        </a>
        <div class="toolbar">
          <a href="{$host}admin/{$module.backend}/delete/{$i.id}" class="confirm" title="Удалить">
            <img src="{$design}delete-page.png" alt="X" title="Удалить">
          </a>
        </div>
      </div>
      {foreachelse}
      <a>
        Доступных страниц нет
      </a>
      {/foreach}
    </div>
  </div>
  {if $settings.default_language == $current_language}
  <div id="content">
    <div class="block setting">
      <div class="string">
        <span class="center">
          <a class="button" href="{$host}{$query}/edit">
            Добавить статью
          </a>
        </span>
      </div>
    </div>
  </div>
  {/if}
  {elseif $queries[2]=='add' || $queries[2]=='edit'}
  <div id="sidebar">
    <div class="block pages">
      {foreach $items as $i}
      <div class="page-link{if $i.id == $item.id} active{/if}">
        <a href="{$host}admin/{$module.backend}/edit/{$i.id}">
          {if !$i.approved}
          <span class="request">
            ?
          </span>{/if}{$i.name}
        </a>
        <div class="toolbar">
          <a href="{$host}admin/{$module.backend}/delete/{$i.id}" class="confirm" title="Удалить">
            <img src="{$design}delete-page.png" alt="X" title="Удалить">
          </a>
        </div>
      </div>
      {foreachelse}
      <a>
        Доступных страниц нет
      </a>
      {/foreach}
    </div>
  </div>
  {if $item.id || $settings.default_language == $current_language}
  <div id="content">
    <div class="block setting">
      <form method="post" enctype="multipart/form-data">
        {if $settings.default_language == $current_language}
        <div class="string">
          <span class="left">
            <div class="check-box{if $item.disp} on{/if}">
              <input name="item[disp]" value="{$item.disp}" type="hidden">
            </div>
            Активность
          </span>
          <span class="right">
            <input class="input" type="text" name="item[datetime]" value="{$item.datetime}" style="width:240px;">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            Сортировка
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <input class="input" type="text" name="item[sort]" value="{$item.sort}" style="width:100px;">
          </span>
        </div>
        <div class="string">
          <span class="left">
            <div class="check-box{if $item.approved} on{/if}">
              <input name="item[approved]" value="{$item.approved}" type="hidden">
            </div>
            Одобрен
          </span>
        </div>
        {/if}
        <div class="string">
          <span class="left">
            ФИО
          </span>
          <span class="right">
            <input class="input" type="text" name="item[name]" value="{$item.name}" placeholder="ФИО">
          </span>
        </div>
        <div class="string">
          <span class="left">
            Изображение
          </span>
          <span class="right">
            <input name="item[pic]" type="file" class="input" style="width:240px;">
            {if $item.pic}
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <a href="{$host}data/{$module.backend}/{$item.pic}?{$rand}" rel="photo">
              <img src="{$host}data/{$module.backend}/{$item.pic}?{$rand}" style="max-width:58px;max-height:40px;vertical-align:middle;">
            </a>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <a href="{$host}{$query}/delete-pic/{$item.pic}" class="del confirm" title="Удалить">
            </a>
            {/if}
          </span>
        </div>
        <div class="string">
          <span class="left">
            Дата рождения
          </span>
          <span class="right">
            <input class="input" type="text" name="item[born]" value="{$item.born}" placeholder="Дата рождения">
          </span>
        </div>
        <div class="string">
          <span class="left">
            Город
          </span>
          <span class="right">
            <input class="input" type="text" name="item[city]" value="{$item.city}" placeholder="Город">
          </span>
        </div>
        <div class="string">
          <span class="left">
            Клуб
          </span>
          <span class="right">
            <input class="input" type="text" name="item[club]" value="{$item.club}" placeholder="Клуб">
          </span>
        </div>
        <div class="string">
          <span class="left">
            Телефон
          </span>
          <span class="right">
            <input class="input" type="text" name="item[phone]" value="{$item.phone}" placeholder="Телефон">
          </span>
        </div>
        <div class="string">
          <span class="left">
            E-mail
          </span>
          <span class="right">
            <input class="input" type="text" name="item[mail]" value="{$item.mail}" placeholder="E-mail">
          </span>
        </div>

        <div class="string">
          <span>
            Краткий текст<br>
            <textarea class="redactor" name="item[short_cont]">
              {$item.short_cont}
            </textarea>
          </span>
        </div>
        <div class="string">
          <span>
            Полный текст<br>
            <textarea class="redactor" name="item[full_cont]">
              {$item.full_cont}
            </textarea>
          </span>
        </div>

        <div class="string">
          <span class="center">
            <input class="button" type="submit" value="{if $item.id}Обновить{else}Добавить{/if}">
          </span>
        </div>
      </form>
    </div>
  </div>
  {/if}
  {elseif $queries[2]=='settings'}
   {include "`$tpl_path`settings.tpl"}
  {elseif $queries[2]=='members'}
    {include "`$tpl_path`members.tpl"}
  {elseif $queries[2]=='comments'}
  {if $queries[3]=='edit' || $queries[4]=='edit'}
  <div class="block setting">
    <form method="post" enctype="multipart/form-data">
      <div class="string">
        <span class="center">
          {foreach $items as $item}
          {if $item.id == $comment.pid}
          {$item.name}
          {/if}
          {/foreach}
        </span>
      </div>
      <div class="string">
        <span class="left">
          <div class="check-box{if $comment.disp} on{/if}">
            <input name="comment[disp]" value="{$comment.disp}" type="hidden">
          </div>
          {if $module.settings.comments_moderation}
          Одобрено
          {else}
          Просмотрено
          {/if}
        </span>
        <span class="right">
          <input class="input" type="text" name="comment[datetime]" value="{$comment.datetime}" style="width:240px;">
        </span>
      </div>
      <div class="string">
        <span class="left">
          Ник
        </span>
        <span class="right">
          <input name="comment[name]" value="{$comment.name}" class="input" type="text">
        </span>
      </div>
      <div class="string">
        <span class="left">
          E-mail
        </span>
        <span class="right">
          <input name="comment[email]" value="{$comment.email}" class="input" type="text">
        </span>
      </div>
      <div class="string">
        <span class="left">
          Коментарии
        </span>
        <span class="right">
          <textarea name="comment[comment]" class="input" rows="2">
            {$comment.comment}
          </textarea>
        </span>
      </div>
      <div class="string">
        <span class="center">
          <input class="button" type="submit" value="Сохранить">
        </span>
      </div>
    </form>
  </div>
  {else}
  <div class="block setting">
    <div class="string">
      <span class="center">
        <a class="button{if !$queries[3]} active{/if}" href="{$host}admin/{$module.backend}/comments">
          Новые
        </a>
        &nbsp;&nbsp;&nbsp;&nbsp;
        <a class="button{if $queries[3] == all} active{/if}" href="{$host}admin/{$module.backend}/comments/all">
          Показать все
        </a>
      </span>
    </div>
    {if count($comments)}
    <form method="post" enctype="multipart/form-data">
      {foreach $items as $item}
      <div class="string">
        <span class="center">
          {$item.name}
        </span>
      </div>
      {foreach $comments as $comment}
      {if $item.id == $comment.pid}
      <div class="string">
        <span class="center">
          <div class="check-box{if $comment.disp} on{/if}">
            <input name="comments[{$comment.id}][disp]" value="{$comment.disp}" type="hidden">
          </div>
          {if $module.settings.comments_moderation}
          Одобрено
          {else}
          Просмотрено
          {/if}
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          Ник
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          <input class="input" type="text" name="comments[{$comment.id}][name]" value="{$comment.name}" style="width:240px;">
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          E-mail
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          <input class="input" type="text" name="comments[{$comment.id}][email]" value="{$comment.email}" style="width:240px;">
          <a href="{$host}{$query}/delete/{$comment.id}" class="del confirm" title="Удалить">
          </a>
          <a href="{$host}{$query}/edit/{$comment.id}" class="edit" title="Редактировать"/></a>
        </span>
      </div>
      {/if}
      {/foreach}
      {/foreach}
      <div class="string">
        <span class="center">
          <input class="button" type="submit" value="Сохранить">
        </span>
      </div>
    </form>
    {else}
    <div class="string">
      <span class="center">
        Комментариев нет
      </span>
    </div>
    {/if}
  </div>
  {/if}
  {/if}
  <div class="clear">
  </div>
</div>
{/strip}