<div class="block setting">
  <form method="post" enctype="multipart/form-data">
    {foreach $members as $member}
    <div class="string" style="padding:5px 0;">
      <span class="left" style="width:7%;">
        <div class="check-box{if $member.paid} on{/if}">
          <input name="members[{$member.id}][paid]" value="{$member.paid}" type="hidden">
        </div>
        Оплатил
        <br />
        <div class="check-box{if $member.volunteer} on{/if}">
          <input name="members[{$member.id}][volunteer]" value="{$member.volunteer}" type="hidden">
        </div>
        Волонтер
      </span>
      <span class="right">
        <input type="text" name="members[{$member.id}][name]" value="{$member.name}" class="input" style="margin-left:5px;width:230px;">
        <input type="text" name="members[{$member.id}][mail]" value="{$member.mail}" class="input" style="margin-left:5px;width:230px;">
        <input type="text" name="members[{$member.id}][phone]" value="{$member.phone}" class="input" style="margin-left:5px;width:230px;">
        <br /><br />
        <input type="text" name="members[{$member.id}][city]" value="{$member.city}" class="input" style="margin-left:5px;width:230px;">
        <input type="text" name="members[{$member.id}][club]" value="{$member.club}" class="input" style="margin-left:5px;width:230px;">
        <input type="text" name="members[{$member.id}][born]" value="{$member.born}" class="input" style="margin-left:5px;width:230px;">
        {if $item.distance}
        <br /><br />
        &nbsp;&nbsp;Дистанция
        <br /><br />
        <select name="members[{$member.id}][distance]" class="input" style="width:450px;margin-left:5px;">
          <option value=" ">
            Не выбрана
          </option>
          {foreach $item.distance as $d}
          <option
            value="{$d}"{if $d == $member.distance} selected{/if}>{$d}
          </option>
          {/foreach}
        </select>
        {/if}
        <a href="{$host}{$query}/delete/{$member.id}" class="del confirm" title="Удалить">
        </a>
      </span>
    </div>
    {foreachelse}
    <div class="string">
      <span class="center">
        Нет зарегистрированых членов клуба
      </span>
    </div>
    {/foreach}
    {if count($members)}
    <div class="string">
      <span class="center">
        <input class="button" type="submit" value="Сохранить">
      </span>
    </div>
    {/if}
  </form>
</div>