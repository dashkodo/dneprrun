{strip}
<div class="menu-bg">
  <menu>
    <a
      {if empty($queries[2]) } class="active"{/if} href="{$host}admin/{$module.backend}">{$module.name}
    </a>
   
    {if !empty($queries[2]) && $queries[2] == 'edit'}
    <a
      {if $queries[2] == 'edit'} class="active"{/if} href="{$host}admin/{$module.backend}/edit/{$user.id}">Редактирование профайла
    </a>
    {/if}

  </menu>
</div>
<div id="main">
  <div class="block setting">
    {if empty($queries[2])  }
    
    <form method="post" enctype="multipart/form-data">
      {foreach $items as $item}
      <div class="string">
        <span>
          <div class="check-box{if $item.disp} on{/if}">
            <input name="items[{$item.id}][disp]" value="{$item.disp}" type="hidden">
          </div>
        </span>  
        
          <input class="input" name="items[{$item.id}][date]" value="{$item.date}" type="text">
          <input class="input" name="items[{$item.id}][name]" value="{$item.name}" type="text">
          <br/>
          <input class="input" name="items[{$item.id}][city]" value="{$item.city}" type="text">
          <input class="input" name="items[{$item.id}][place]" value="{$item.place}" type="text">
           <input class="input" name="items[{$item.id}][distance]" value="{$item.distance}" type="text">
           <input class="input" name="items[{$item.id}][owner]" value="{$item.owner}" type="text">
           <input class="input" name="items[{$item.id}][link]" value="{$item.link}" type="text">
          
          <a href="{$host}admin/{$module.backend}/del/{$item.id}" class="del confirm" title="Удалить">
          </a>
          <a href="{$host}admin/{$module.backend}/edit/{$item.id}" class="edit" title="Редактировать">
          </a>
        
      </div>
      {foreachelse}
      <div class="string">
        <span class="center">
          Событий нет
        </span>
      </div>
      {/foreach}
      {if count($items)}
      <div class="string">
        <span class="center">
          <input class="button" type="submit" value="Сохранить">
        </span>
      </div>
      {/if}
    </form>

    {elseif  $queries[2]=='edit'}
    <div class="string">
      <span class="right">
        Профайл пользователя {$user.name} ({$user.mail})
      </span>

    </div>
    <form method="post" enctype="multipart/form-data">

      {foreach $profile as $key=>$val}
      <div class="string">
        <span class="left">
          {$key}
        </span>
        <span class="right">
          <input name="profile[{$key}]" value="{$val}" class="input" type="text" placeholder="Введите значение поля профиля пользователя" >
        </span>
      </div>
      {/foreach}
      <div class="string">
        <span class="left">
          <input name="newfield[key]" class="input" type="text" placeholder="Введите название поля профиля пользователя" >
        </span>
        <span class="right">
          <input name="newfield[value]" value="" class="input" type="text" placeholder="Введите значение поля профиля пользователя" >
        </span>
      </div>

      <div class="string">
        <span class="center">
          <input class="button" type="submit" value="Сохранить">
        </span>
      </div>
    </form>

    {/if}
  </div>
  <div class="clear">
  </div>
</div>
{/strip}