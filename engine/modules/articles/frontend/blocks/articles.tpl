<div class="block">
	<div>
		{foreach $block_items as $block_item}
			<a class="link{if $run_block.active && $block_item.id == $item.id} active{/if}" href="{$host}{$language_prefix}{$run_block.frontend}/{$block_item.url}">{$block_item.name}</a>
		{/foreach}
	</div>
</div>