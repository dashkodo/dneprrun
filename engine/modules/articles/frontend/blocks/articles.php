<?php

if (!defined('PARENT_FILE')) {
	die('Running banned');
}

if ($run_block['settings']['amount_to_block']) {
	$items = select_rows($run_block['backend'] . '-' . $current_language, '`disp`=1', '`sort`, `id` DESC', '0, ' . $run_block['settings']['amount_to_block']);
} else {
	$items = select_rows($run_block['backend'] . '-' . $current_language, '`disp`=1', '`sort`, `id` DESC');
}
if (count($items)) {
	foreach ($items as &$item) {
		if ($item['pic']) {
			$item['pic'] = $host . 'data/' . $run_block['backend'] . '/' . $item['pic'];
		}
		$item['datetime'] = datetime($item['datetime']);
	}
}
$smarty->assign('block_items', $items);

$passes_item_sql = "UPDATE `" . $run_block['backend'] . '-' . $current_language . "` SET `passed`=1 WHERE `datetime`<CURRENT_TIMESTAMP";
query($passes_item_sql);
$feature_item_sql = "UPDATE `" . $run_block['backend'] . '-' . $current_language . "` SET `passed`=NULL WHERE `datetime`>CURRENT_TIMESTAMP";
query($feature_item_sql);

if($items_soon = select_rows($run_block['backend'] . '-' . $current_language, '`disp`=1 AND `on_main`=1 AND `passed` IS NULL', '`main_sort` DESC, `datetime`')){
	foreach($items_soon as &$item_soon){
	   if ($item_soon['pic']) {
    		$item_soon['pic'] = $host . 'data/' . $run_block['backend'] . '/' . $item_soon['pic'];
    	}
    	//if(intval($run_block['settings']['timeline']) && strtotime($item_soon['datetime'])>(time()+3600*intval($run_block['settings']['timeline'])))
        if(!$item_soon['dateregister'] || strtotime($item_soon['dateregister'])>time())
    		$item_soon['register'] = true;
    	$item_soon['datetime'] = datetime($item_soon['datetime']);
	}
    //$item_soon = reset($item_soon);
	//$smarty->assign('item_soon', $item_soon);
    $smarty->assign('items_soon', $items_soon);
}

?>