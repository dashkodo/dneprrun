<?php

if(!defined('PARENT_FILE')){
  die('Running banned');
}

if(!empty($_POST['event'])){
  $event = array_map_recursive('trim', $_POST['event']);
  $event = array_map_recursive('strip_tags', $event);
  $event['born'] = date('Y-m-d',strtotime($event['born']));
  if($member = select_row($module['backend'] . '-members-' . $current_language, false, '`mail`="' . escape_string($event['mail']) . '" AND `event_id`=' . intval($event['event_id']))){
    print_r('.register_event .member_exist');
  }else{
    insert_row($module['backend'] . '-members-' . $current_language,$event);
    print_r('.register_event .success');
  }
  die;
}

if(isset($queries[1])){
  if($item = select_row($module['backend'] . '-' . $current_language, false, '`url`="' . escape_string($queries[1]) . '" AND `disp`=1')){
    if($item['pic']){
      $item['pic'] = $host . 'data/' . $module['backend'] . '/' . $item['pic'];
    }
    //if(intval($module['settings']['timeline']) && strtotime($item['datetime']) > (time() + 3600 * intval($module['settings']['timeline'])))
    if($item['dateregister'] && strtotime($item['dateregister']) > time())
    $item['register'] = true;
    $item['datetime'] = datetime($item['datetime']);
    if($item['distance'])
    $item['distance'] = explode("|",$item['distance']);
   

    $smarty->assign('members', fill_members(select_rows($module['backend'] . '-members-' . $current_language, '`event_id`=' . $item['id'].' and `volunteer`=0', 'id DESC'))  );
    $smarty->assign('volunteers', fill_members(select_rows($module['backend'] . '-members-' . $current_language, '`event_id`=' . $item['id'].' and `volunteer`=1', 'id DESC')) );
    $smarty->assign('events', load_results($item['id']));
    $smarty->assign('item', $item);

    set_seo($item['name'], $item['title'], $item['desc'], $item['key']);
  } elseif(intval($queries[1])){
    $current_page = (int) $queries[1];
  } else{
    error_404();
  }
} else{
  $current_page = 1;
}


if($current_page){
  if($module['settings']['amount_to_page']){
    $from = ($current_page - 1) * $module['settings']['amount_to_page'];
    $items = select_rows($module['backend'] . '-' . $current_language, 'disp=1', 'sort, id DESC', $from . ', ' . $module['settings']['amount_to_page']);
  } else{
    $items = select_rows($module['backend'] . '-' . $current_language, 'disp=1', 'sort, id DESC');
  }
  if(count($items)){
    foreach($items as & $item){
      if($item['pic']){
        $item['pic'] = $host . 'data/' . $module['backend'] . '/' . $item['pic'];
      }
      $item['datetime'] = datetime($item['datetime']);
    }
    $smarty->assign('items', $items);
    $items_count = count_rows($module['backend'] . '-' . $current_language, 'disp=1');
    if($module['settings']['amount_to_page'] < $items_count){
      $paginator = ceil($items_count / $module['settings']['amount_to_page']);
      $smarty->assign('paginator', $paginator);
    }
  } else{
    //error_404();
  }
}
$smarty->assign('current_page', $current_page);
$smarty->assign('name', $seo['name']);
?>