{strip}
	{* Заголовок *}
	<h1>{$name}</h1>
	{* Хлебные крошки *}
	{include file="breadcrumbs.tpl"}
	{* Подробное описание элемента *}
	{if $item}
		<div class="item">
			{$item.full_cont}
		</div>
		{* Рекомендуемые *}
		{if count($item.related)}
			<h3>{$l.articles3}</h3>
			<div class="categories">
				{foreach $item.related as $related}
					<div class="related">
						<a href="{$host}{$language_prefix}{$module.frontend}/{$related.url}">{$related.name}</a>
						<div>{$related.short_cont}</div>
					</div>
				{/foreach}
			</div>
		{/if}
	{else}
		{if empty($queries[1])}
			{if $module.pic}
				<img class="module-pic" src="{$host}data/{$module.backend}/{$module.pic}" alt="{$module.name}">
			{/if}
			{if $module.full_cont}
				<div class="full-cont">{$module.full_cont}</div>
			{/if}
		{/if}
	{/if}
{/strip}