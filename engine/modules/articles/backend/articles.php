<?php

if (!defined('PARENT_FILE')) {
	die('Running banned');
}

// sql
query("CREATE TABLE IF NOT EXISTS `" . $module['backend'] . '-' . $current_language . "` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cid` int(11) DEFAULT '1',
  `disp` int(11) DEFAULT '1',
  `datetime` datetime DEFAULT NULL,
  `name` text,
  `url` text,
  `title` text,
  `key` text,
  `desc` text,
  `tags` text,
  `short_cont` text,
  `full_cont` text,
  `pic` text,
  `related` text,
  `sort` int(11) DEFAULT '0',
  PRIMARY KEY `id` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;");

/*if ($module['settings']['comments']) {
	query("CREATE TABLE IF NOT EXISTS `" . $module['backend'] . "-comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) DEFAULT '0',
  `disp` int(11) DEFAULT '0',
  `name` text,
  `email` text,
  `comment` text,
  `datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;");
}*/

$upload_path = upload_path($module['backend']);

if ($queries[2] == 'delete') {
	$id = (int) $queries[3];
	if ($settings['languages']) {
		foreach ($settings['languages'] as $language) {
			$item = select_row($module['backend'] . '-' . $language, $id);
			if ($item['pic']) {
				unlink($upload_path . $item['pic']);
			}
			delete_row($module['backend'] . '-' . $language, $id);
		}
	} else {
		$item = select_row($module['backend'] . '-' . $current_language, $id);
		if ($item['pic']) {
			unlink($upload_path . $item['pic']);
		}
		delete_row($module['backend'] . '-' . $current_language, $id);
	}
	redirect(-2);
}

if ($queries[2] == 'edit') {
	$id = $queries[3];
	if ($id) {
		if (!select_row($module['backend'] . '-' . $current_language, $id)) {
			$copy = select_row($module['backend'] . '-' . $settings['default_language'], $id);
			unset($copy['pic']);
			insert_row($module['backend'] . '-' . $current_language, $copy);
		}
		$item = select_row($module['backend'] . '-' . $current_language, $id);
		if (isset($_POST['item']) && $_POST['item']['name']) {
			if ($_FILES['item']['size']['pic']) {
				$item = select_row($module['backend'] . '-' . $current_language, $id);
				if ($item['pic']) {
					unlink($upload_path . $item['pic']);
				}
				$img = uniqid() . '.';
				$img .= pathinfo($_FILES['item']['name']['pic'], PATHINFO_EXTENSION);
				$img = strtolower($img);
				if (move_uploaded_file($_FILES['item']['tmp_name']['pic'], $upload_path . $img)) {
					$_POST['item']['pic'] = $img;
				} else {
					$_POST['item']['pic'] = '';
				}
			}
			update_row($module['backend'] . '-' . $current_language, $_POST['item'], $id);
			if ($settings['default_language'] == $current_language) {
				foreach ($settings['languages'] as $language) {
					if ($language != $current_language) {
						update_row($module['backend'] . '-' . $language, array(
							'url' => $_POST['item']['url'],
							'disp' => $_POST['item']['disp'],
							'datetime' => $_POST['item']['datetime']
								), $id);
					}
				}
			}
			redirect();
		}
	} else {
		$item = array(
			'disp' => 1,
			'datetime' => $datetime,
            'dateregister' => $datetime,
			'sort' => 0
		);
		if ($_POST['item']['name']) {
			if ($_FILES['item']['size']['pic']) {
				$img = uniqid() . '.';
				$img .= pathinfo($_FILES['item']['name']['pic'], PATHINFO_EXTENSION);
				$img = strtolower($img);
				if (move_uploaded_file($_FILES['item']['tmp_name']['pic'], $upload_path . $img)) {
					$_POST['item']['pic'] = $img;
				} else {
					$_POST['item']['pic'] = '';
				}
			}
			$id = insert_row($module['backend'] . '-' . $current_language, $_POST['item']);
			print_r($_POST);
            redirect('/' . $id);
		}
	}

	if (isset($queries[4]) && $queries[4] == 'delete-pic') {
		unlink($upload_path . $queries[5]);
		update_row($module['backend'] . '-' . $current_language, array('pic' => null), $id);
		redirect(-2);
	}
	$smarty->assign('item', $item);
}
$smarty->assign('items', select_rows($module['backend'] . '-' . $current_language, false, 'sort, id DESC'));

if($queries[2] == 'members'){
	if (isset($_POST['members'])) {
		foreach ($_POST['members'] as $mid => $values) {
			if (!empty($values['name'])) {
				update_row($module['backend'] . '-members-' . $current_language, $values, $mid);
			}
		}
		redirect();
	}
	if($queries[4] == 'delete'){
		delete_row($module['backend'] . '-members-' . $current_language, intval($queries['5']));
		redirect(-2);
	}
	$id = $queries[3];
	if(intval($id)){
	   $item = select_row($module['backend'] . '-' . $current_language, $id);
       if($item['distance'])
        $item['distance'] = explode("|",$item['distance']);
		$smarty->assign('item', $item);
		$smarty->assign('members', select_rows($module['backend'] . '-members-' . $current_language, '`event_id`=' . $id, 'id DESC'));
	}
}


if($queries[2] == 'results'){
	if (isset($_POST['result'])) {
		
		foreach ($_POST['result'] as $mid => $values) {
			 foreach((array)$values as $value){
				 
				 if($value['member_mail']!==''){
					 if(isset($value['id'])){
						 update_row('event-result',(array)$value,(int)$value['id']);
					 }
					 else{
						 insert_row('event-result',$value);
					 }
				 }
				 else{
					
				 }
			 }
			  
		}
		//redirect();
	}


	if($queries[4] == 'delete'){
		delete_row('results', intval($queries['5']));
		redirect(-2);
	}
	
  if($queries[4] == 'edit'){
		if (isset($_POST['event'])){
      foreach($_POST['event'] as $evt)
       update_row("event",$evt,$evt['id']);
    }
		if (isset($_POST['newevent'])&&!empty($_POST['newevent']['distance'])){
      insert_row("event",array('distance'=>$_POST['newevent']['distance'],'article_id'=>$_POST['newevent']['article_id']));
    }
	}
	
  $id = $queries[3];
	if(intval($id)){
	   $item = select_row($module['backend'] . '-' . $current_language, $id);
      
		$smarty->assign('item', $item);
		$smarty->assign('members', select_rows($module['backend'] . '-members-' . $current_language, '`event_id`=' . $id, 'id DESC'));
		$smarty->assign('results', load_results($id));

	}
}


if ($queries[2] == 'settings') {
	if ($_POST['module']) {
		if ($_FILES['module']['size']['pic']) {
			if ($module['pic']) {
				unlink($upload_path . $module['pic']);
			}
			$img = uniqid() . '.';
			$img .= pathinfo($_FILES['module']['name']['pic'], PATHINFO_EXTENSION);
			$img = strtolower($img);
			if (move_uploaded_file($_FILES['module']['tmp_name']['pic'], $upload_path . $img)) {
				$_POST['module']['pic'] = $img;
			}
		}
		$data = $_POST['module'];
		if (isset($_POST['settings'])) {
			$data = array('settings' => $_POST['settings']) + $data;
		}
		update_row('modules-' . $current_language, $data, $module['id']);
		if ($settings['default_language'] == $current_language) {
			foreach ($settings['languages'] as $language) {
				if ($language != $current_language) {
					update_row('modules-' . $language, array(
						'frontend' => $_POST['module']['frontend'],
						'settings' => $_POST['settings']
							), $module['id']);
				}
			}
		}
		redirect();
	}
	if ($queries[3] == 'delete-pic') {
		unlink($upload_path . $queries[4]);
		unset($module['settings']['pic']);
		update_row('modules-' . $current_language, array('pic' => null), $module['id']);
		redirect(-2);
	}
}
?>