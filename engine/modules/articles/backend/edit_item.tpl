﻿<div id="sidebar">
	<div class="block pages">
		{foreach $items as $i}
			<div class="page-link{if $i.id == $item.id} active{/if}">
				<a href="{$host}admin/{$module.backend}/edit/{$i.id}">{$i.name}</a>
				<div class="toolbar">
					<a href="{$host}admin/{$module.backend}/delete/{$i.id}" class="confirm" title="Удалить">
						<img src="{$design}delete-page.png" alt="X" title="Удалить">
					</a>
				</div>
			</div>
		{foreachelse}
			<a>Доступных страниц нет</a>
		{/foreach}
	</div>
</div>
{if $item.id || $settings.default_language == $current_language}
	<div id="content">
		<div class="block setting">
			<form method="post" enctype="multipart/form-data">
				{if $settings.default_language == $current_language}
					<div class="string">
						<span class="left">
							<div class="check-box{if $item.disp} on{/if}">
								<input name="item[disp]" value="{$item.disp}" type="hidden">
							</div>
							Активность
						</span>
						<span class="right">
							<input class="input" type="text" name="item[datetime]" value="{$item.datetime}" style="width:240px;">
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							Сортировка
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<input class="input" type="text" name="item[sort]" value="{$item.sort}" style="width:100px;">
						</span>
					</div>
                    <div class="string">
						<span class="left">
							<div class="check-box{if $item.on_main} on{/if}">
								<input name="item[on_main]" value="{$item.on_main}" type="hidden">
							</div>
							На главной
						</span>
						<span class="right">
							<input class="input" type="text" name="item[dateregister]" value="{$item.dateregister}" style="width:240px;">
                            &nbsp;-&nbsp;
							Конец регистрации
						</span>
                        <span class="right">
							<input class="input" type="text" name="item[main_sort]" value="{$item.main_sort}" style="width:100px;">
						</span>
					</div>
				{/if}
				<div class="string">
					<span class="left">
						Название
					</span>
					<span class="right">
						<div id="module-name">
							<input class="translit input" type="text" name="item[name]" value="{$item.name}" placeholder="Название">
							{if $settings.default_language == $current_language && $item.url}
								<div id="switch-translit">
									Обновить<br>URL
									<input type="hidden" id="url" value="">
								</div>
							{/if}
						</div>
					</span>
				</div>
				<div class="string">
					<span class="left">
						Изображение
					</span>
					<span class="right">
						<input name="item[pic]" type="file" class="input" style="width:240px;">
						{if $item.pic}
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<a href="{$host}data/{$module.backend}/{$item.pic}?{$rand}" rel="photo">
								<img src="{$host}data/{$module.backend}/{$item.pic}?{$rand}" style="max-width:58px;max-height:40px;vertical-align:middle;">
							</a>
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<a href="{$host}{$query}/delete-pic/{$item.pic}" class="del confirm" title="Удалить"></a>
						{/if}
					</span>
				</div>
				<div class="string">
					<span class="left">
						<a href="#" id="seo">SEO</a>
					</span>
				</div>
				<div class="seo-cont">
					{if $settings.default_language == $current_language}
						<div class="string">
							<span class="left">
								URL
							</span>
							<span class="right">
								<input name="item[url]" value="{$item.url}" type="text" class="url input"{if !$item.url} id="url"{/if}>
							</span>
						</div>
					{/if}
					<div class="string">
						<span class="left">
							Title
						</span>
						<span class="right">
							<input name="item[title]" value="{$item.title}" class="input" type="text">
						</span>
					</div>
					<div class="string">
						<span class="left">
							Description
						</span>
						<span class="right">
							<textarea name="item[desc]" class="input" rows="2">{$item.desc}</textarea>
						</span>
					</div>
					<div class="string">
						<span class="left">
							Keywords
						</span>
						<span class="right">
							<textarea name="item[key]" class="input" rows="2">{$item.key}</textarea>
						</span>
					</div>
				</div>
                <div class="string">
					<span class="left">
						Ссылка регистрации<br /> на стороннем сайте
					</span>
					<span class="right">
                        <input name="item[foreign_register]" value="{$item.foreign_register}" class="input" type="text">
					</span>
				</div>
                <div class="string">
					<span class="left">
						Город
					</span>
					<span class="right">
						<input name="item[city]" value="{$item.city}" class="input" type="text">
					</span>
				</div>
                <div class="string">
					<span class="left">
						Дистанции
					</span>
					<span class="right">
						<input id="distance" name="item[distance]" value="{$item.distance}" class="input" type="text">
					</span>
				</div>
                <div class="string">
					<span class="left">
						Стоимости
					</span>
					<span class="right">
						<input id="prices" name="item[prices]" value="{$item.prices}" class="input" type="text">
					</span>
				</div>
                {* Организатор, ссылки на трассу, ссылка на положение, вид соревнования *}
                <div class="string">
					<span class="left">
						Организатор
					</span>
					<span class="right">
						<input name="item[organizer]" value="{$item.organizer}" class="input" type="text">
					</span>
				</div>
                <div class="string">
					<span class="left">
						Ссылки на <br />трассу
					</span>
					<span class="right">
						<input id="ways" name="item[ways]" value="{$item.ways}" class="input" type="text">
					</span>
				</div>
                <div class="string">
					<span class="left">
						Ссылка на <br />положение
					</span>
					<span class="right">
						<input name="item[location]" value="{$item.location}" class="input" type="text">
					</span>
				</div>
                <div class="string">
					<span class="left">
						Вид соревнования
					</span>
					<span class="right">
						<input name="item[competition]" value="{$item.competition}" class="input" type="text">
					</span>
				</div>
                
				<div class="string">
					<span>
						Краткий текст<br>
						<textarea class="redactor" name="item[short_cont]">{$item.short_cont}</textarea>
					</span>
				</div>
				<div class="string">
					<span>
						Полный текст<br>
						<textarea class="redactor" name="item[full_cont]">{$item.full_cont}</textarea>
					</span>
				</div>
				<div class="string">
					<span class="center">
						<input class="button" type="submit" value="{if $item.id}Обновить{else}Добавить{/if}">
					</span>
					{if $item.id}
						<span class="center">
							<a class="button" href="{$host}admin/{$module.backend}/members/{$item.id}">Участники</a>
						</span>
						<span class="center">
							<a class="button" href="{$host}admin/{$module.backend}/results/{$item.id}">Результаты</a>
						</span>
					{/if}
				</div>
                <link href="{$design}inputosaurus/jquery-ui.css" rel="stylesheet">
                <script src="{$design}inputosaurus/jquery.1.11.1.min.js"></script>
                <script src="{$design}inputosaurus/jquery-ui.1.10.3.min.js"></script>
                <link rel="stylesheet" type="text/css" href="{$design}inputosaurus/inputosaurus.css" media="screen" />
                <script src="{$design}inputosaurus/inputosaurus.js"></script>
            	<script>
                    $.noConflict();
                    var tags = new Array();
                    {literal}
                    jQuery(function(){
                        jQuery('#distance,#prices,#ways').inputosaurus({
                            inputDelimiters : [',', ';', '|'],
                            outputDelimiter: '|',
                        	activateFinalResult : true,
                            autoCompleteSource : tags
                    	});
                    })
                    {/literal}
            	</script>
			</form>
		</div>
	</div>
{/if}