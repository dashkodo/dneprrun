﻿{strip}
<div class="menu-bg">
  <menu>
    <a
      {if $queries[2]!='settings' && $queries[2]!='comments'} class="active"{/if} href="{$host}admin/{$module.backend}">{$module.name}
    </a>
    <a
      {if $queries[2]=='settings'} class="active"{/if} href="{$host}admin/{$module.backend}/settings">Настройки
    </a>
  </menu>
</div>
{$tpl_path = "engine/modules/`$module.backend`/backend/"}
<div id="main">
  {if !$queries[2]}
  <div id="sidebar">
    <div class="block pages">
      {foreach $items as $i}
      <div class="page-link{if $i.id == $item.id} active{/if}">
        <a href="{$host}admin/{$module.backend}/edit/{$i.id}">
          {$i.name}
        </a>
        <div class="toolbar">
          <a href="{$host}admin/{$module.backend}/delete/{$i.id}" class="confirm" title="Удалить">
            <img src="{$design}delete-page.png" alt="X" title="Удалить">
          </a>
        </div>
      </div>
      {foreachelse}
      <a>
        Доступных страниц нет
      </a>
      {/foreach}
    </div>
  </div>
  {if $settings.default_language == $current_language}
  <div id="content">
    <div class="block setting">
      <div class="string">
        <span class="center">
          <a class="button" href="{$host}{$query}/edit">
            Добавить статью
          </a>
        </span>
      </div>
    </div>
  </div>
  {/if}
  {elseif $queries[2]=='add' || $queries[2]=='edit'}
  {include "`$tpl_path`edit_item.tpl"}
  {elseif $queries[2]=='settings'}
  {include "`$tpl_path`settings.tpl"}
  {elseif $queries[2]=='members'}
  
  {include "`$tpl_path`members.tpl"}
  {elseif $queries[2]=='results'}
  {if $queries[4]=='edit'}      
   {include "`$tpl_path`results_edit.tpl"}
  {else}
  {include "`$tpl_path`results.tpl"}
  {/if}
  {/if}
  <div class="clear">
  </div>
</div>
{/strip}