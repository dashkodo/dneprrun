<div class="block setting">
	<form method="post" enctype="multipart/form-data">
		<div class="string">
			<span class="center">{$item.name|default:"Мероприятие отсутствует"}</span>
		</div>
		<div class="string">
			<span class="center"><a class="button" href="{$host}admin/{$module.backend}/edit/{$item.id}">Вернуться к мероприятию</a></span>
		</div>
		{foreach $members as $member}
			<div class="string" style="padding:5px 0;">
				<span class="left" style="width:7%;">
					<div class="check-box{if $member.paid} on{/if}">
						<input name="members[{$member.id}][paid]" value="{$member.paid}" type="hidden">
					</div>
					Оплатил
					<br />
					<div class="check-box{if $member.volunteer} on{/if}">
						<input name="members[{$member.id}][volunteer]" value="{$member.volunteer}" type="hidden">
					</div>
					Волонтер
					<br />
					 {if $item.distance}
                        Дистанция
                        <br />
                        <select name="members[{$member.id}][distance]" class="input" style="margin-left:0px;">
                            <option value=" ">Не выбрана</option>
                            {foreach $item.distance as $d}
                                <option value="{$d}"{if $d == $member.distance} selected{/if}>{$d}</option>
                            {/foreach}
                        </select>
                    {/if}
				</span>
				<span class="right">
					<input type="text" name="members[{$member.id}][name]" value="{$member.name}" class="input" placeholder="ФИО" style="margin-left:5px;width:230px;">
					<input type="text" name="members[{$member.id}][mail]" value="{$member.mail}" class="input" placeholder="Email"  style="margin-left:5px;width:230px;">
					<input type="text" name="members[{$member.id}][phone]" value="{$member.phone}" class="input" placeholder="Телефон" style="margin-left:5px;width:230px;">
                    <br /><br />
                    <input type="text" name="members[{$member.id}][city]" value="{$member.city}" class="input" placeholder="Город" style="margin-left:5px;width:230px;">
					<input type="text" name="members[{$member.id}][club]" value="{$member.club}" class="input" placeholder="Клуб" style="margin-left:5px;width:230px;">
                    <input type="text" name="members[{$member.id}][born]" value="{$member.born}" class="input" placeholder="Дата рождения" style="margin-left:5px;width:230px;">
					<textarea name="members[{$member.id}][comment]"  class="input" style="margin:5px;width:745px;">{$member.comment}</textarea>                   
					<a href="{$host}{$query}/delete/{$member.id}" class="del confirm" title="Удалить"></a>
				</span>
			</div>
		{foreachelse}
			<div class="string">
				<span class="center">Нет зарегистрированых участников</span>
			</div>
		{/foreach}
		{if count($members)}
			<div class="string">
				<span class="center">
					<input class="button" type="submit" value="Сохранить">
				</span>
			</div>
		{/if}
	</form>
</div>