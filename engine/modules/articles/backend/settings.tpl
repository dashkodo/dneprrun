<div class="block setting">
	<form method="post" enctype="multipart/form-data">
		<div class="string">
			<span class="left">
				Название модуля
			</span>
			<span class="right">
				<div id="module-name">
					<input class="translit input" type="text" name="module[name]" value="{$module.name}" placeholder="Название модуля">
					<div id="switch-translit">
						Обновить<br>URL
						<input type="hidden" id="url" value="">
					</div>
				</div>
			</span>
		</div>
		<div class="string">
			<span class="left">
				Картинка
			</span>
			<span class="right">
				<input name="module[pic]" type="file" class="input" style="width:240px;">
				{if $module.pic}
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<a href="{$host}data/{$module.backend}/{$module.pic}?{$rand}" rel="photo">
						<img src="{$host}data/{$module.backend}/{$module.pic}?{$rand}" style="max-width:58px;max-height:40px;vertical-align:middle;">
					</a>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<a href="{$host}{$query}/delete-pic/{$module.pic}" class="del confirm" title="Удалить"></a>
				{/if}
			</span>
		</div>
		<div class="string">
			<span class="left">
				<a href="#" id="seo">SEO</a>
			</span>
		</div>
		<div class="seo-cont">
			{if $settings.default_language == $current_language}
				<div class="string">
					<span class="left">
						URL
					</span>
					<span class="right">
						<input name="module[frontend]" value="{$module.frontend}" type="text" class="url input">
					</span>
				</div>
			{/if}
			<div class="string">
				<span class="left">
					Title
				</span>
				<span class="right">
					<input name="module[title]" value="{$module.title}" class="input" type="text">
				</span>
			</div>
			<div class="string">
				<span class="left">
					Description
				</span>
				<span class="right">
					<textarea name="module[desc]" class="input" rows="2">{$module.desc}</textarea>
				</span>
			</div>
			<div class="string">
				<span class="left">
					Keywords
				</span>
				<span class="right">
					<textarea name="module[key]" class="input" rows="2">{$module.key}</textarea>
				</span>
			</div>
		</div>
		<div class="string">
			<span>
				Описание<br>
				<textarea class="redactor" name="module[full_cont]">{$module.full_cont}</textarea>
			</span>
		</div>
		{if $settings.default_language == $current_language}
			<div class="string">
				<span class="left">
					Количество элементов<br>на страницу
				</span>
				<span class="right">
					<input name="settings[amount_to_page]" value="{$module.settings.amount_to_page}" type="text" class="input" style="width:100px;">
				</span>
			</div>
			<div class="string">
				<span class="left">
					Количество элементов<br>в блоке
				</span>
				<span class="right">
					<input name="settings[amount_to_block]" value="{$module.settings.amount_to_block}" type="text" class="input" style="width:100px;">
				</span>
			</div>
			<div class="string">
				<span class="left">
					Количество часов до<br>окончания регистрации
				</span>
				<span class="right">
					<input name="settings[timeline]" value="{$module.settings.timeline}" type="text" class="input" style="width:100px;">
				</span>
			</div>
		{/if}
		<div class="string">
			<span class="center">
				<input class="button" type="submit" value="Сохранить">
			</span>
		</div>
	</form>
</div>