<div class="block setting">

  <form method="post" enctype="multipart/form-data">
    <div class="string">
      <span class="center">
        {$item.name|default:"Мероприятие отсутствует"}
      </span>
    </div>
    <div class="string">
      <span class="center">
        <a class="button" href="{$host}admin/{$module.backend}/edit/{$item.id}">
          Вернуться к мероприятию
        </a>
      </span>
    </div>

    {foreach $results as $event}
    <div class="string zebra">
      <span class="left">
      </span>
      <span class="center">
        <input type="hidden" name="event[{$event.event.id}][id]" value="{$event.event.id}" placeholder="Формат: ххх метров" class="input" >

        <input type="hidden" name="event[{$event.event.id}][article_id]" value="{$event.event.article_id}" placeholder="Формат: ххх метров" class="input" >
        <input type="text" name="event[{$event.event.id}][distance]" value="{$event.event.distance}" placeholder="Формат: ххх метров" class="input">
      </span>
    </div>

    {/foreach}

    <div class="string zebra">
      <span class="left">
      </span>
      <span class="center">
        <input type="hidden" name="newevent[article_id]" value="{$item.id}" placeholder="Формат: ххх метров" class="input" >
        <input type="text" name="newevent[distance]" value="" placeholder="Формат: ххх метров" class="input">
      </span>
    </div>

    <div class="string">
      <span class="center">
        <input class="button" type="submit" value="Сохранить">
      </span>
    </div>
  </form>
</div>