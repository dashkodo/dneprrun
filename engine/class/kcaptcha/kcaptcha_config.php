<?php
# Используемый алфавит.
$alphabet = "0123456789abcdefghijklmnopqrstuvwxyz";
# Символы используемые для рисования каптчи, без похожих символов (o => 0, 1 => l, i => j, t => f).
$allowed_symbols = "23456789abcdegikpqsvxyz";
# Абсолютный путь до директории с шрифтами.
$fontsdir = 'fonts';
# Количество символов каптчи.
//$length = mt_rand(5,7);
$length = 4;
# Ширина и высота каптчи.
$width = 160;
$height = 60;
# Вертикальная амплитуда колебания символа, разделенная на 2.
$fluctuation_amplitude = 2;
# Шум
$white_noise_density=1/6;
$black_noise_density=1/30;
# Запрет пробелов между символами.
$no_spaces = false;
#
$show_credits = false;
$credits = 'www.captcha.ru';
#  Цвет текста каптчи в формате RGB.
$foreground_color = array(mt_rand(0,80), mt_rand(0,80), mt_rand(0,80));
//$foreground_color = array(0, 0, 0);
# Цвет заднего фона каптчи в формате RGB.
$background_color = array(mt_rand(220,255), mt_rand(220,255), mt_rand(220,255));
$background_color = array(161, 217, 255);
# JPEG quality of CAPTCHA image (bigger is better quality, but larger file size)
$jpeg_quality = 75;
?>