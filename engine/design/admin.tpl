﻿{strip}
	<!DOCTYPE html>
	<html>
		<head>
			<meta charset="{$smarty.const.CHARSET}">
			<title>{$title}</title>
			<link href="{$design}favicon.ico" rel="shortcut icon">
			<link href="{$design}style.css" rel="stylesheet">
			<link href="{$design}jquery.ui.css" rel="stylesheet">
			<!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
			{if $smarty.session.user}
				<link href="{$design}fancybox/jquery.fancybox.css" rel="stylesheet">
				<link href="{$design}jcrop/jquery.Jcrop.min.css" rel="stylesheet">
				<link href="{$design}chosen/chosen.css" rel="stylesheet">
				<script>var query = '{$query}';</script>
				<script src="{$host}admin/system.js"></script>
				<script src="{$design}jquery-1.8.1.min.js"></script>
				<script src="{$design}jquery-ui-1.8.23.sortable.min.js"></script>
				<script src="{$design}fancybox/jquery.mousewheel-3.0.6.pack.js"></script>
				<script src="{$design}fancybox/jquery.fancybox.pack.js"></script>
				<script src="{$design}jcrop/jquery.Jcrop.min.js"></script>
				<script src="{$design}chosen/chosen.jquery.min.js"></script>
				<script src="{$design}flowplayer/flowplayer-3.2.6.min.js"></script>
				<script src="{$design}tiny_mce/tiny_mce.js"></script>
				<script src="{$design}jquery.plugins.js"></script>
				<script src="{$design}script.js"></script>
			{/if}
		</head>
		<body>
			{if count($permitted_modules)}

				<div class="content">
					<div class="nav-bg">
						<nav>
							<div class="list">
								{foreach $permitted_modules as $permitted_module}
									<a {if $queries[1] == $permitted_module.backend}class="a" {/if}href="{$host}admin/{$permitted_module.backend}">
										<img src="{$host}engine/modules/{$permitted_module.backend}/icon.png" alt="">
										<span class="br3">
											{$permitted_module.name}
											{if $permitted_module.comments}
												({$permitted_module.comments})
											{/if}
										</span>
									</a>
								{/foreach}
								<a href="{$host}">
									<img src="{$design}go-website.png" alt="">
									<span>Клиентская часть</span>
								</a>
								<a>
									<form method="post">
										<button name="exit" type="submit">
											<img src="{$design}exit.png" alt="">
											<span>Выйти</span>
										</button>
									</form>
								</a>
								{if $settings.languages}
									<div class="languages">
										{foreach $settings.languages as $language}
											<img src="{$host}engine/languages/{$language}.png" alt="{$language}" class="br3{if $current_language == $language} active{/if}">
										{/foreach}
									</div>
								{/if}
							</div>
							<div class="turn"></div>
						</nav>
					</div>
					{include file="$tpl"}
					<div class="clear"></div>
					<div class="empty"></div>
				</div>
				<footer>
					&nbsp;
				</footer>
			{else}
				<div id="login">
					<h2>Вход в панель администрирования</h2>
					<div class="error">{$error}</div>
					<form method="post">
						<div class="name">Логин:</div>
						<div class="field">
							<input name="login" type="text" value="{$login}">
						</div>
						<div class="name">Пароль:</div>
						<div class="field">
							<input name="pass" type="password" value="{$pass}">
						</div>
						<div class="clear"></div>
						<div class="submit">
							<input class="button" value="Войти" type="submit">
						</div>
					</form>
				</div>
			{/if}
		</body>
	</html>
{/strip}