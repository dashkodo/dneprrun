function compareNumeric(a, b) {
	return a - b;
}


var animatedTime = 500;
var delayTime = 1000;


$(document).ready(function() {
	if ($('.url').val()) {
		var url = $('.url').val();
	}

	function reserved_url(this_url) {
		if (this_url) {
			if (this_url == url) {
				return false;
			} else if ($.inArray(this_url.replace(/^\/+|\/+$/g, ''), reserved_urls) != -1) {
				return true;
			} else {
				return false;
			}
		} else {
			return true;
		}
	}
	function zebra() {
		$('.setting .string:even').removeClass('zebra');
		$('.setting .string:odd').not(':last').addClass('zebra');
	}
	zebra();
	var list = $.cookie('list');
	if(list == 1) {
		$('.list').css('display', 'none');
		$('.turn').addClass('up');
	}
	$('.turn').click(function() {
		if($('.list').css('display') == 'none') {
			$('.list').slideDown(animatedTime, function() {
				$('.turn').removeClass('up');
				$.cookie('list', 0, {
					expires: 356,
					path: path,
					domain: domain
				});
			});
		} else {
			$('.list').slideUp(animatedTime, function() {
				$('.turn').addClass('up');
				$.cookie('list', 1, {
					expires: 356,
					path: path,
					domain: domain
				});
			});
		}
	});
	$('.dnd').sortable({
		handle: '.handle',
		revert: 100,
		deactivate: function(event, ui) {
			zebra();
		},
		update: function(event, ui) {
			$.get(host+query+'/sort/'+$(this).sortable('toArray').toString(), {},
				function(data){
				//alert(data);
				});
		}
	});
	$('.handle').mousedown(function () {
		$(this).parents('.string').removeClass('zebra');
		$(this).parents('.string').addClass('drag');
	}).mouseup(function () {
		$(this).parents('.string').removeClass('drag');
		zebra();
	});
	$('.confirm').click(function() {
		return confirm($(this).attr('title')+'?');
	});
	$("a[rel=photo]").fancybox({
		'transitionIn'		: 'none',
		'transitionOut'		: 'none',
		'titlePosition' 	: 'over',
		'titleFormat'		: function(title, currentArray, currentIndex, currentOpts) {
			return '<span id="fancybox-title-over">Фото ' + (currentIndex + 1) + ' / ' + currentArray.length + (title.length ? ' &nbsp; ' + title : '') + '</span>';
		}
	});
	$("a[rel=video]").fancybox({
		'transitionIn': 'none',
		'transitionOut': 'none',
		'scrolling': false
	});
	$(".play-video").flowplayer({
		src: design+"flowplayer-3.2.7.swf",
		wmode: 'transparent'
	}, {
		clip: {
			scaling: 'fit'
		},
		plugins: {
			controls: {
				backgroundColor: '#2f5891'
			}
		}
	});
	$('a[rel=inline]').fancybox({
		type: 'inline',
		fitToView: false,
		topRatio: 0,
		aspectRatio: false,
		fixed: true,
		scrolling: false,
		arrows: false,
		closeBtn: false,
		mouseWheel: false,
		autoSize: true,
		beforeClose: function() {
			window.location.hash = '';
		},
		afterLoad: function() {
			window.location.hash = $(this).attr('href');
		}
	});
	$('input[name=add_photo]').change(function() {
		if($(this).val()) {
			$(this).parents('form').submit();
		}
	});
	$('.upload').change(function() {
		if($(this).val()) {
			$(this).parents('form').submit();
		}
	});
	$('#seo').click(function(e) {
		e.preventDefault();
		$('.seo-cont').slideToggle(animatedTime);
	});
	if ($('.url').size()) {
		$('.translit').syncTranslit({
			destination: 'url'
		});
		$('.url').keyfilter(/[a-z0-9-\/.]/i);
		$('.translit').bind('keyup', function() {
			if(reserved_url($('.url').val())) {
				$('.url').addClass('error');
				$('.seo-cont').slideDown(animatedTime);
			} else {
				$('.url').removeClass('error');
			}
			$('#switch-translit').fadeTo(animatedTime, 0.5);
		});
		$("#switch-translit").click(function() {
			$('.url').val($('input', this).val());
			$(this).hide();
			if(reserved_url($('.url').val())) {
				$('.url').addClass('error');
				$('.seo-cont').slideDown(animatedTime);
			} else {
				$('.url').removeClass('error');
			}
		});
		$('.url').keyup(function() {
			if(reserved_url($(this).val())) {
				$(this).addClass('error');
				$(this).parents('form').submit(function() {
					return false;
				});
			} else {
				$(this).removeClass('error');
			}
		});
		$('.url').parents('form').submit(function() {
			var url = $('.url');
			if(reserved_url(url.val())) {
				url.addClass('error');
				$('.seo-cont').slideDown(animatedTime);
				return false;
			} else {
				url.removeClass('error');
				$(this).submit();
			}
		});
	}
	$('.check-box').click(function() {
		if($(this).hasClass('on')) {
			$(this).removeClass('on');
			$("input", this).val(0);
		} else {
			$(this).addClass('on');
			$('input', this).val(1);
		}
	});
	$('[name="block[show]"]').change(function() {
		var show = $(this).val();
		var $to_show = $('[name="block[to_show][]"]');
		if (show == 0) {
			$to_show.attr('disabled', '');
		} else if (show == 1) {
			$to_show.removeAttr('disabled');
		} else if (show == 2) {
			$to_show.removeAttr('disabled');
		}
	});
	$('input.cont').click(function() {
		$('span.file').hide();
		$('span.cont').show();
	});
	$('input.file').click(function() {
		$('span.cont').hide();
		$('span.file').show();
	});
	// menu
	$('[href=#add]').live('click', function(e) {
		e.preventDefault();
		var $this = $(this).parents('.parent:first');
		var $append = $('.sample').clone();
		var ids = [];
		$('#menu-builder .item').each(function (i) {
			ids.push($(this).find('[name="menu[id][]"]').val());
		});
		ids.sort(compareNumeric);
		var id = parseInt(ids.pop())+1;
		var parent = $this.find('[name="menu[id][]"]').val();
		$append.find('[name="menu[id][]"]').val(id);
		if(parent) {
			$append.find('[name="menu[parent][]"]').val(parent);
		}
		if(!$this.append($append.html()).length) {
			$('#menu-builder').after($(this)).append($append.html());
		}
	});
	$('[href=#del]').live('click', function(e) {
		e.preventDefault();
		var $this = $(this).parents('.parent:first');
		if($this.children('.parent').length) {
			var message = 'Удалить элемент и всех потомков?';
		} else {
			var message = 'Удалить элемент?';
		}
		if(confirm(message)) {
			$this.remove();
		}
	});
	$('[href=#up]').live('click', function(e) {
		e.preventDefault();
		var $this = $(this).parents('.parent:first');
		$this.prev('.parent').before($this);
	});
	$('[href=#down]').live('click', function(e) {
		e.preventDefault();
		var $this = $(this).parents('.parent:first');
		$this.next('.parent').after($this);
	});
	$(".chzn-select").chosen({
		no_results_text: "Ничего не найдено по запросу"
	});
	$('.languages img').not('.active').click(function() {
		var language = $(this).attr('alt');
		$.cookie('current-language', language, {
			path: path,
			domain: domain
		});
		window.location.reload(true);
	});
});

tinyMCE.init({
	editor_selector : 'redactor',
	theme: 'advanced',
	skin: 'default',
	mode: 'specific_textareas',
	language: 'ru',
	dialog_type: 'modal',
	body_id: 'content',
	body_class: 'content',
	force_br_newlines : true,
	force_p_newlines : false,
	forced_root_block : '',
	plugins: 'advimage, advlink, advlist, contextmenu, fullscreen, iespell, inlinepopups, lists, media, paste, table',
	theme_advanced_buttons1: 'code, newdocument, |, bold, italic, underline, strikethrough, |, justifyleft, justifycenter, justifyright, justifyfull, formatselect, fontselect, fontsizeselect',
	theme_advanced_buttons2: 'cut, copy, paste, pastetext, pasteword, |, bullist, numlist, |, outdent, indent, |, undo, redo, |, link, unlink, anchor, cleanup, |, forecolor, backcolor',
	theme_advanced_buttons3: 'tablecontrols, |, hr,removeformat,visualaid,|,sub,sup,|,charmap,iespell,media, image, |,fullscreen',
	theme_advanced_toolbar_location: 'top',
	theme_advanced_toolbar_align: 'left',
	theme_advanced_statusbar_location: 'bottom',
	theme_advanced_resizing: true,
	theme_advanced_resize_horizontal: false,
	content_css: content_css,
	table_styles: 'table-1=table-1; table-2=table-2; table-3=table-3; table-4=table-4',
	advlink_styles: 'button=button',
	convert_urls: false,
	relative_urls: false,
	file_browser_callback: "openKCFinder",
	accessibility_warnings: false
});
function openKCFinder(field_name, url, type, win) {
	tinyMCE.activeEditor.windowManager.open({
		file: host+'engine/kcfinder/browse.php?opener=tinymce&lang=ru&type=' + type,
		title: 'Файловый менеджер',
		width: 700,
		height: 500,
		resizable: "yes",
		inline: true,
		close_previous: "no",
		popup_css: false
	}, {
		window: win,
		input: field_name
	});
	return false;
}