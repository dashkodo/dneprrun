<?php

if(!defined('PARENT_FILE')){
  die('Running banned');
}


/**
* Check value to find if it was serialized.
*
* If $data is not an string, then returned value will always be false.
* Serialized data is always a string.
*
* @since 2.0.5
*
* @param mixed $data Value to check to see if was serialized.
* @return bool False if not serialized and true if it was.
*/
function is_serialized($data){
  // if it isn't a string, it isn't serialized
  if(!is_string($data))
  return false;
  $data = trim($data);
  if('N;' == $data)
  return true;
  $length = strlen($data);
  if($length < 4)
  return false;
  if(':' !== $data[1])
  return false;
  $lastc = $data[$length - 1];
  if(';' !== $lastc && '}' !== $lastc)
  return false;
  $token = $data[0];
  switch($token){
    case 's' :
    if('"' !== $data[$length - 2])
    return false;
    case 'a' :
    case 'O' :
    return (bool) preg_match("/^{$token}:[0-9]+:/s", $data);
    case 'b' :
    case 'i' :
    case 'd' :
    return (bool) preg_match("/^{$token}:[0-9.E-]+;\$/", $data);
  }
  return false;
}

if(!function_exists('array_map_recursive')){

  function array_map_recursive($function, $data){
    if(is_array($data)){
      foreach($data as $i => $item){
        $data[$i] = is_array($item) ? array_map_recursive($function, $item) : $function($item);
      }
    }
    else{
      $data = array();
    }
    return $data;
  }

}

function strips( & $el){
  $el = array_map_recursive('stripslashes', $el);
}

///////////////////////////////////////
// Function redirect.
// Argument $com, value:
//    1) '' (false) - refresh this address,
//    2) 1 or - 2 (integer) - removes n level(s) or
//    3) http://my.site / request (string) - redirecting to this address and
//    if empty http://, automatically add $host and $query.
// Autor Victor Sulimov
///////////////////////////////////////
function redirect($com = false){
  global $host, $query, $queries, $language_prefix;
  if(empty($com)){
    $uri = $host . $language_prefix . $query;
  }
  elseif(is_integer($com)){
    $queries = array_slice($queries, 0, - abs($com));
    $uri     = $host . $language_prefix . implode('/', $queries);
  }
  elseif(is_string($com)){
    $parse_url = parse_url(trim($com, '/.'));
    $uri       = array_key_exists('scheme', $parse_url) ? $com : $host . $language_prefix . $query . $com;
  }
  else{
    return false;
  }
  header('location: ' . $uri, true, 301);
  print_r('<script>window.location="' . $uri . '";</script>');
  die('<meta http-equiv="refresh" content="0;url=' . $uri . '">');
}

function datetime($datetime){
  global $settings;
  return date($settings['date_format'], strtotime($datetime));
}

function escape_array($array){
  return array_map_recursive('escape_string', $array);
}

function group($gid = 0){
  if($gid){
    $row = select_row('groups', $gid);
  }
  elseif($_SESSION['user']['group']){
    $row = select_row('groups', $_SESSION['user']['group']);
  }
  else{
    return false;
  }

  if(!$row['access'])
  $row['access'] = array();
  return $row;
}

function groups(){
  return is_root() ? select_rows('groups', false, 'sort, id DESC') : select_rows('groups', "access!='root'", 'sort, id DESC');
}

function is_root(){
  $group = group();
  return ($group['access'] == 'root') ? true : false;
}


function table_exists($table_name){
  global $show_tables;
  return in_array($table_name, $show_tables) ? true : false;
}

function insert_row($table_name, $data_array){
  $sql = "INSERT INTO `" . $table_name . "` SET ";
  foreach($data_array as $key => $value){
    if(is_array($value)){
      $value = serialize($value);
    }
    $value = escape_string($value);
    $sql .= "`" . $key . "`='" . $value . "', ";
  }
  $sql = rtrim($sql, ', ');
  $sql .= ';';
  query($sql);
  return insert_id();
}



function replace_row($table_name, $data_array){
  $sql = "REPLACE INTO `" . $table_name . "` SET ";
  foreach($data_array as $key => $value){
    if(is_array($value)){
      $value = serialize($value);
    }
    $value = escape_string($value);
    $sql .= "`" . $key . "`='" . $value . "', ";
  }
  $sql = rtrim($sql, ', ');
  $sql .= ';';
  query($sql);
  return insert_id();
}

function update_row($table_name, $data_array, $id){
  $sql = "UPDATE `" . $table_name . "` SET ";
  foreach($data_array as $key => $value){
    if(is_array($value)){
      $value = serialize($value);
    }
    $value = escape_string($value);
    $sql .= "`" . $key . "`='" . $value . "', ";
  }
  $sql = rtrim($sql, ', ');
  $sql .= " WHERE id=" . (int) $id . ";";
  return query($sql);
}

function select_row($table_name, $id = false, $where = false){
  if(!table_exists($table_name))
  return false;
  $sql = "SELECT * FROM `" . $table_name . "` WHERE ";
  if(!$id){
    $sql .= $where;
  }
  elseif(!$where){
    $sql .= "id=" . (int) $id;
  }
  else{
    $sql .= "id=" . (int) $id . " AND " . $where;
  }
  $sql .= ';';
  $q = query($sql);
  if($row = fetch_assoc($q)){
    foreach($row as & $value){
      if(is_serialized($value)){
        $value = unserialize($value);
      }
    }
    return $row;
  }
  else{
    return false;
  }
}

function select_rows($table_name, $where = false, $order_by = false, $limit = false){
  if(!table_exists($table_name))
  return array();
  $sql = "SELECT * FROM `" . $table_name . "`";
  if($where){
    $sql .= " WHERE " . $where;
  }
  if($order_by){
    $sql .= " ORDER BY " . $order_by;
  }
  if($limit){
    $sql .= " LIMIT " . $limit;
  }
  $sql .= ';';
  $q    = query($sql);
  $rows = array();
  while($row = fetch_assoc($q)){
    foreach($row as $key => & $value){
      if(is_serialized($value)){
        $value = unserialize($value);
      }
      if($key == 'datetime'){

      }
    }
    $rows[] = $row;
  }
  return $rows;
}

function count_rows($table_name, $where = false){
  if(!table_exists($table_name))
  return false;
  if($where){
    $sql = "SELECT COUNT(*) FROM `" . $table_name . "` WHERE " . $where;
  }
  else{
    $sql = "SELECT COUNT(*) FROM `" . $table_name . "`";
  }
  $sql .= ';';
  $q   = query($sql);
  $row = fetch_assoc($q);
  return $row['COUNT(*)'];
}


function delete_rows($table_name, $where){
  return query("DELETE FROM `" . $table_name . "` WHERE " . $where .";");
}


function delete_row($table_name, $id){
  return query("DELETE FROM `" . $table_name . "` WHERE id=" . (int) $id . ";");
}


function upload_path($module = false){
  if(!$module)
  return false;
  if(!is_dir(DATA_PATH . $module)){
    if(!mkdir(DATA_PATH . $module, 0777, true)){
      die('Не удалось создать директорию "' . DATA_PATH . $module . '", проверьте права записи.');
    }
  }
  return DATA_PATH . $module . '/';
}

function cldir_recursive($dir){
  if(!$dir)
  return;
  foreach(scandir($dir) as $file){
    if($file == '.' || $file == '..')
    continue;
    $file = $dir . '/' . $file;
    if(is_dir($file)){
      cldir_recursive($file);
      rmdir($file);
    }
    else{
      unlink($file);
    }
  }
}

function level($table_name, $parent, $level = 0){
  global $current_language;
  if($parent){
    $level++;
    $row = select_row($table_name . '-categories-' . $current_language, $parent);
    if($row['parent']){
      $level = level($table_name, $row['parent'], $level);
    }
  }
  return $level;
}

function get_categories($table_name, $parent = 0, & $cats = array()){
  global $current_language;
  if($rows = select_rows($table_name . '-categories-' . $current_language, 'parent=' . $parent, 'sort, name DESC')){
    foreach($rows as $row){
      $row['level'] = level($table_name, $row['parent']);
      $cats[] = $row;
      get_categories($table_name, $row['id'], $cats);
    }
  }
  return $cats;
}
/* categies_tree */
function get_categories_tree($table_name, $parent = 0, $cats = array()){
  global $current_language;
  if($rows = select_rows($table_name . '-categories-' . $current_language, 'parent=' . $parent, 'sort, name DESC')){
    foreach($rows as $row){
      $row['level'] = level($table_name, $row['parent']);
      $cats[$row['id']] = $row;
      $cats[$row['id']]['subcategories'] = array();
      $cats[$row['id']]['subcategories'] = get_categories_tree($table_name, $row['id'], $cats[$row['id']]['subcategories']);
    }
  }
  return $cats;
}
function get_visible_categories_tree($table_name, $parent = 0, $cats = array(), $with_items = false){
  global $current_language;
  if($rows = select_rows($table_name . '-categories-' . $current_language, 'parent=' . $parent . ' AND disp=1', 'sort, name DESC')){
    foreach($rows as $row){
      $row['level'] = level($table_name, $row['parent']);
      $cats[$row['id']] = $row;
      if($with_items){
        $cats[$row['id']]['items'] = select_rows($table_name . '-' . $current_language, 'cid=' . $row['id'] . ' AND disp=1', 'sort');
      }
      $cats[$row['id']]['subcategories'] = array();
      $cats[$row['id']]['subcategories'] = get_visible_categories_tree($table_name, $row['id'], $cats[$row['id']]['subcategories'],$with_items);
    }
  }
  return $cats;
}
/* categies_tree /*/
function error_404(){
  global $smarty;
  header($_SERVER['SERVER_PROTOCOL'] . ' 404 Not Found');
  $smarty->assign('name', '404');
  $smarty->assign('cont', '404');
  set_seo('404', '404', '404', '404');
  if($smarty->templateExists('index.tpl')){
    if($smarty->templateExists('404.tpl')){
      $smarty->assign('tpl', '404.tpl');
    }
    else{
      die('Не найден файл 404-й ошибки - 404.tpl');
    }
  }
}


function setup_language_prefix($default_language = 'ru', & $current_language){
  // ISO 639 - 1
  global $host, $query, $queries, $settings;
  if(isset($queries[0])){
    if(array_search($queries[0], $settings['languages']) !== false){
      $current_language = array_shift($queries);
      $query            = implode('/', $queries);
      if($default_language == $current_language){
        redirect($host . $query);
      }
    }
  }
  if($current_language){
    $language_prefix = $current_language . '/';
  }
  else{
    $current_language = $default_language;
    $language_prefix  = '';
  }
  return $language_prefix;
}

function parent_categories($backend, $parent, & $cats = array()){
  global $current_language;
  if($parent){
    $cat = select_row($backend . '-categories-' . $current_language, $parent);
    $cats[] = $cat;
    parent_categories($backend, $cat['parent'], $cats);
  }
  return $cats;
}

function value_to_boolean($value){
  return (boolean) $value;
}

function value_exists($array = array()){
  return array_filter($array, 'value_to_boolean');
}

function set_seo($name, $title, $desc, $key){
  global $seo;
  $seo = array_merge(
    (array) $seo, value_exists(
      array(
        'name' => $name,
        'title'=> $title ? $title : $name,
        'desc' => $desc,
        'key'  => $key
      )
    )
  );
}

function format_bytes($size){
  $units = array(' B',' KB',' MB',' GB',' TB');
  for($i = 0; $size >= 1024 && $i < 4; $i++)
  $size /= 1024;
  return round($size, 2) . $units[$i];
}

function dir_size($directory, $format_bytes = true){
  $iterator = new RecursiveDirectoryIterator($directory);
  $iterator = new RecursiveIteratorIterator($iterator);
  $size     = 0;
  foreach($iterator as $file){
    $size += $file->getSize();
  }
  return $format_bytes ? format_bytes($size) : $size;
}

function sendmail($to, $from, $subject, $message){
  $subject = '=?' . CHARSET . '?B?' . base64_encode($subject) . '?=';
  $headers = "MIME-Version: 1.0\r\n";
  $headers .= "Content-type: text/html; charset=" . CHARSET . "\r\n";
  if($from){
    $headers .= "From: " . $from . "\r\n";
  }
  return mail($to, $subject, $message, $headers);
}

function set_profile_field($key,$val,$id){
  $field = array(
    'field'  =>$key,
    'value'  =>$val,
    'user_id'=>$id
  );

  if($q = select_row("profile",false,"`user_id`=".$id.' and `field`="'.$key.'"')){
    update_row('profile',$field, $q['id']);
  }
  else{
    insert_row('profile',$field);
  }
}


function fill_members($m_array){
  $ret = array();

  foreach($m_array as $m){
    $ret[] = $m;
  }
  return $ret;
}

function vdot($distance,$time){
  if($distance < 3000) return 0;
  $time = explode(":",$time);
  $time = $time[0] * 60 * 60 + $time[1] * 60 + $time[2];

  $vo2  = (0.8000000000 + 0.1894393 * exp( - 0.012778 * $time / 60) + 0.2989558 * exp( - 0.1932605 * $time / 60));
  $vdot = ( - 4.6 + 0.182258 * ($distance / $time / (1440 / 1440 / 60)) + 0.000104 * pow(($distance / $time / (1440 / 1440 / 60)),2)) / $vo2;

  return floor($vdot * 10) / 10;
  //return $vdot;
}


function load_profile($id){
  if(!($profile_data = select_rows('profile', "`user_id`=" . $id ))){
    return;
  }
  
  $profile = array();
  foreach($profile_data as $n){
    $key = $n['field'];
    $profile[$key] = $n['value'];
  }
  //$profile['vdot'] = get_vdot_for_user(select_row('users',$id)['mail']);
  set_profile_field('vdot',$profile['vdot'],$id);
  return $profile;


}

function select_most_filled_user($email){
  $query_str = "select * , ((CASE WHEN name IS NULL OR name = '' THEN 1 ELSE 0 END) + (CASE WHEN mail IS Null or mail = '' THEN 1 ELSE 0 END) + (CASE WHEN club IS  NULL OR club = '' THEN 1 ELSE 0 END) + (CASE WHEN phone IS NULL or phone='' THEN 1 ELSE 0 END)) as empty_cols from `articles-members-ru` where `mail` = '".$email."' order by empty_cols, id desc limit 1";

  $q         = query($query_str);
  if($row = fetch_assoc($q)){
    foreach($row as & $value){
      if(is_serialized($value)){
        $value = unserialize($value);
      }
    }


    $profile = load_profile("(select id from users where `mail` = '".$email."')");

    foreach($profile as $k=>$v)
    $row[$k] = $v;

    return $row;
  }
  else{
    return false;
  }
}



function load_results($article_id){
  $ret = array();
  if($events = select_rows('event', "`article_id`=" . $article_id )){

    foreach($events as $event){
      $data = array();
      $data['event'] = $event;
      $data['results'] = array();
      $q = select_rows('event-result', "`event_id`=" . $event['id'],'result');
      foreach($q as $entity){
        $entity['member'] = select_most_filled_user($entity['member_mail']);
        $entity['result'] = $entity['result'][0]=='-'?"DNF":$entity['result'];
        $data['results'][] = $entity;
      }
      $ret[] = $data;
    }



  }
  return $ret;
}


function load_user_results($user_mail){
  $ret = array();

  if($event_groups = select_rows('articles-ru')){
    foreach($event_groups as $event_group){
      $evt_group = array();
      $evt_group['name'] = $event_group['name'];
      $evt_group['url'] = $event_group['url'];

      if($events = select_rows('event','`article_id`='.$event_group['id'])){
        foreach($events as $event){

          $data = array();
          $data['event'] = $event;
          $data['results'] = array();
          $q = select_rows('event-result', "`event_id`=" . $event['id']." AND `member_mail`='".$user_mail."'");
          foreach($q as $entity){
            $entity['vdot'] = vdot($event['distance'],$entity['result']);
            $data['results'][] = $entity;
          }
          if($data['results']){
            $evt_group['events'][] = $data;
          }
        }
      }
      if($evt_group['events'])
      $ret[] = $evt_group;
    }
  }
  return $ret;
}

function get_vdot_for_user($email){
  $result = load_user_results($_SESSION["user"]["mail"]);

  $vdot   = 0;
  foreach($result as $evt_group){
    foreach($evt_group['events'] as $evt){
      $vdot = max($vdot,$evt['results'][0]['vdot']);
    }
  }
  return $vdot;
}

?>
