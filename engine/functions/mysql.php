<?php

if (!defined('PARENT_FILE')) {
	die('Running banned');
}

function query($sql) {
	return mysql_query($sql);
}

function fetch_assoc($res) {
	if (empty($res)) {
		error_db();
	} else {
		return mysql_fetch_assoc($res);
	}
}

function fetch_row($res) {
	if (empty($res)) {
		error_db();
	} else {
		return mysql_fetch_row($res);
	}
}

function escape_string($data) {
	if (is_array($data)) {
		$data = array_map('escape_string', $data);
	} else {
		$data = mysql_real_escape_string($data);
	}
	return $data;
}

function insert_id() {
	return mysql_insert_id();
}

function error_db() {
	global $settings;
	if ($settings['debag']) {
		print_r('#' . mysql_errno() . ': ' . mysql_error() . ' - ' . mysql_list_processes() . '<br>');
	}
}

?>