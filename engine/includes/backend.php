<?php

if(!defined('PARENT_FILE')){
  die('Running banned');
}

$smarty->assign('design', $host . ENGINE . 'design/style/');
$smarty->setTemplateDir(TPL_PATH_ADMIN);

$smarty->assign('settings', $settings);
if($queries[0] == 'admin' && isset($_SESSION['user'])){

  $datetime = date('Y-m-d H:i:s');

  // Определение языковой версии для бекенда
  if(isset($_COOKIE['current-language']) && in_array($_COOKIE['current-language'], $settings['languages'])){
    $current_language = $_COOKIE['current-language'];
  } else{
    $current_language = $settings['default_language'];
  }
  $smarty->assign('current_language', $current_language);

  query("CREATE TABLE IF NOT EXISTS `modules-" . $current_language . "` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `disp` int(11) DEFAULT '1',
    `name` text,
    `title` text,
    `desc` text,
    `key` text,
    `full_cont` text,
    `backend` text,
    `frontend` text,
    `type` text,
    `settings` longtext,
    `access` longtext,
    `sort` int(11) DEFAULT '0',
    `pic` text,
    PRIMARY KEY (`id`)
    ) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;");


  // get list all modules and return variable $mapped_modules
  $rows = select_rows('modules-' . $current_language, '`type`!="frontend"', 'sort, id DESC');
  $mapped_modules = array();
  foreach($rows as $row){
    if(file_exists(MODULES_PATH . $row['backend'] . '/frontend')){
      if($row['disp']){
        if(!file_exists(ROOT_PATH . 'design/' . $settings['design'] . '/' . $row['backend'] . '.tpl')){
          $row['disp'] = 0;
        }
      }
    }
    if($row['disp']){
      $mapped_modules[$row['backend']] = $row;
    }
  }

  // формируем список разрешённых модулей
  $permitted_modules = array();
  $group = group();
  $smarty->assign('group', $group);
  
  if(is_root()){
    $permitted_modules = $mapped_modules;
  } else{
    if($group['disp']){
      $permitted_modules = array_intersect_key($mapped_modules, $group['access']);
    }
  }

  $reserved_urls = array('admin');
  foreach($permitted_modules as & $permitted_module){
    if($permitted_module['frontend']){
      $reserved_urls[] = $permitted_module['frontend'];
    }
    
    $access_module = is_root() ? $permitted_module['access'] : $group['access'][$permitted_module['backend']];
    
  
    if(is_array($access_module)){
      foreach($access_module as $key => $value){
        $permitted_module['access_module'][$key] = is_root() ? 1 : $value;
      }
    }
    $permitted_module['comments'] = count_rows($permitted_module['backend'] . '-comments', 'disp=0');
  }
  $smarty->assign('permitted_modules', $permitted_modules);

  //после авторизации маршрутизируем на указанный модуль, если запрещено - на первый
  if(empty($queries[1])){
    if(array_key_exists($settings['admin_page'], $permitted_modules) !== false){
      redirect('/' . $settings['admin_page']);
    } else{
      if(count($permitted_modules)){
        $first_module = reset($permitted_modules);
        redirect('/' . $first_module['backend']);
      }
    }
  } elseif($queries[1] == 'system.js'){
    header('Content-Type: text/javascript; charset=' . CHARSET);
    print_r("var domain = '" . $domain . "';\r\n");
    print_r("var path = '" . $path . "';\r\n");
    print_r("var host = '" . $host . "';\r\n");
    print_r("var design = '" . $host . ENGINE . "design/style/';\r\n");
    $content_css = 'design/' . $settings['design'] . '/style/content.css';
    if(file_exists(ROOT_PATH . $content_css)){
      print_r("var content_css = '" . $host . $content_css . "';\r\n");
    } else{
      print_r("var content_css = undefined;\r\n");
    }
    foreach($show_tables as $table){
      $q = query("SHOW COLUMNS FROM `" . $table . "`;");
      while($row = fetch_assoc($q)){
        if($row['Field'] == 'url'){
          $rows = select_rows($table);
          foreach($rows as $url){
            if($url['url']){
              $reserved_urls[] = $url['url'];
            }
          }
        }
      }
    }
    $reserved_urls = array_unique($reserved_urls);
    $reserved_urls = implode("',\r\n\t'", $reserved_urls);
    if($reserved_urls){
      $reserved_urls = "'" . $reserved_urls . "'";
    }
    $reserved_urls = "var reserved_urls = [\r\n\t" . $reserved_urls . "\r\n];";
    print_r($reserved_urls);
    // Operating time
    printf("\r\n//%.3f", microtime(true) - $start_time);
    die();
  }

  $frontends = select_rows('modules-' . $current_language, '`disp`=1 AND (`type`="frontend" OR `type`="all")', '`sort`, `id` DESC');
  foreach($frontends as & $frontend){
    if(count_rows($frontend['backend'] . '-categories-' . $current_language)){
      $frontend['categories'] = get_categories($frontend['backend']);
    } else{
      $frontend['items'] = select_rows($frontend['backend'] . '-' . $current_language, '`disp`=1', 'sort, id');
    }
  }
  $smarty->assign('frontends', $frontends);
  // определяем запрошеный модуль
  if(array_key_exists($queries[1], $permitted_modules) !== false){
    $module = $permitted_modules[$queries[1]];
    if($settings['default_language'] == $current_language){
      foreach($settings['languages'] as $language){
        if($language != $current_language){
          if(!select_row('modules-' . $language, $module['id'])){
            $copy = select_row('modules-' . $settings['default_language'], $module['id']);
            unset($copy['pic']);
            insert_row('modules-' . $language, $copy);
          }
        }
      }
    }
    $smarty->assign('module', $module);
    $smarty->assign('title', $module['name']);

    $module_backend_path = MODULES_PATH . $module['backend'] . '/backend/';
    if(file_exists($module_backend_path . $module['backend'] . '.php')){
      include_once($module_backend_path . $module['backend'] . '.php');
    }
    if(file_exists($module_backend_path . $module['backend'] . '.tpl')){
      $smarty->assign('tpl', $module_backend_path . $module['backend'] . '.tpl');
    } else{
      die('Модуль "' . $module['name'] . '" повреждён!');
    }
  } else{
    $smarty->assign('tpl', 'access-denied.tpl');
  }
} else{
  $smarty->assign('title', 'Вход в панель администрирования');
}
$smarty->display('admin.tpl', '', 'admin');
?>