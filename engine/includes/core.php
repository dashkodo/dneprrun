<?php
function recursion_level($array, $parent, $level = 0){
  if($parent){
    foreach($array as $value){
      if($value['id'] == $parent){
        return recursion_level($array, $value['parent'], $level += 1);
        break;
      }
    }
  }
  else{
    return $level;
  }
}


function get_mapped_modules(){
  global $current_language, $smarty, $host;

  $rows = select_rows('modules-' . $current_language, '`type`="all" OR `type`="frontend"', '`sort`, `id` DESC');
  $mapped_modules = array();
  foreach($rows as $row){
    if(file_exists(MODULES_PATH . $row['backend'] . '/frontend')){
      if($row['disp'] && !$smarty->templateExists($row['backend'] . '.tpl')){
        $row['disp'] = 0;
      }
    }
    if($row['disp']){
      $row['dir'] = $host . 'data/' . $row['backend'] . '/';
      $mapped_modules[$row['backend']] = $row;
    }
  }
  return $mapped_modules;

}

function search_module($mapped_modules){
  $module;
  global $current_language,$queries;
  foreach($mapped_modules as $db => $mapped_module){
    if($mapped_module['frontend'] == $queries[0]){
      $module = $mapped_module;
      break;
    }

    if($row = select_row($db . '-' . $current_language, false, "url='" . escape_string($queries[0]) . "' AND `disp`=1")){
      $module = $mapped_module;
      $module['item'] = $row;
      break;
    }
    if($row = select_row($db . '-categories-' . $current_language, false, "url='" . escape_string($queries[0]) . "' AND `disp`=1")){
      $module = $mapped_module;
      $module['category'] = $row;
      break;
    }
  }
  return $module;
}
function get_block_in_file($file_name){
  global $current_language, $host, $module, $settings, $smarty;
  if($smarty->templateExists('blocks/' . $file_name)){
    $backend = pathinfo($file_name, PATHINFO_FILENAME);
    $mb      = explode("_", $backend);
    $backend = (count($mb) > 1) ? $mb[0] : $backend;
    if($run_block = select_row('modules-' . $current_language, false, 'backend="' . $backend . '"')){
      $run_block['active'] = (bool) ($run_block['id'] == $module['id']);
      $run_block['dir'] = $host . 'data/' . $run_block['backend'] . '/';
      $smarty->assign('run_block', $run_block);
      $php_block = MODULES_PATH . $backend . '/frontend/blocks/' . $backend . '.php';
      if(file_exists($php_block))
      include_once($php_block);
    }
    return $smarty->fetch('blocks/' . $file_name);
  }
  else{
    return false;
  }
}

function load_module($smarty, $module, $module_path){
  if($module && $smarty->templateExists($module['backend'] . '.tpl')){
    $smarty->assign('tpl', $module['backend'] . '.tpl');
    $smarty->assign('module', $module);
    set_seo($module['name'], $module['title'], $module['desc'], $module['key']);
    $file_path = $module_path . $module['backend'] . '/frontend/' . $module['backend'] . '.php';
    if(file_exists($file_path)){
      return $file_path;

    }
  }
  return;
}

function render_breadcrumbs(){
  global $host,$language_prefix,$smarty,$mapped_modules,$settings,$current_language,$query,$module,$queries;

  if(empty($query)){
    return;
  }

  $breadcrumbs = array();
  if(isset($module['item'])){
    $breadcrumbs[] = array(
      'url' => $module['item']['url'],
      'name'=> $module['item']['name']
    );
  }
  elseif($module['id']){

    $breadcrumbs[] = array(
      'url' => $module['frontend'],
      'name'=> $module['name']
    );
    if(isset($queries[1])){
      if($item = select_row($module['backend'] . '-' . $current_language, false, "url='" . escape_string($queries[1]) . "' AND disp=1")){
        $breadcrumbs[] = array(
          'url' => $module['frontend'] . '/' . $item['url'],
          'name'=> $item['name']
        );
        $uniqid_pages[] = $module['id'] . 'i' . $item['id'];
      }
      elseif($brand = select_row($module['backend'] . '-brands-' . $current_language, false, "url='" . escape_string($queries[2]) . "' AND disp=1")){
        $breadcrumbs[] = array(
          'url' => $module['frontend'] . '/brand/' . $brand['url'],
          'name'=> $brand['name']
        );
        $uniqid_pages[] = $module['id'] . 'i' . $brand['id'];
      }
      elseif($cat = select_row($module['backend'] . '-categories-' . $current_language, false, "url='" . escape_string($queries[1]) . "' AND disp=1")){
        $cats = parent_categories($module['backend'], $cat['parent']);
        $cats = array_reverse($cats);
        foreach($cats as $c){
          $breadcrumbs[] = array(
            'url' => $module['frontend'] . '/' . $c['url'],
            'name'=> $c['name']
          );
          $uniqid_pages[] = $module['id'] . 'c' . $c['id'];
        }
        $breadcrumbs[] = array(
          'url' => $module['frontend'] . '/' . $cat['url'],
          'name'=> $cat['name']
        );
        $uniqid_pages[] = $module['id'] . 'c' . $cat['id'];
        if(isset($queries[2])){
          if($item = select_row($module['backend'] . '-' . $current_language, false, "url='" . escape_string($queries[2]) . "' AND disp=1")){
            $breadcrumbs[] = array(
              'url' => $module['frontend'] . '/' . $cat['url'] . '/' . $item['url'],
              'name'=> $item['name']
            );
            $uniqid_pages[] = $module['id'] . 'i' . $item['id'];
          }
        }
      }
    }
  }
  $smarty->assign('breadcrumbs', $breadcrumbs);


}

function render_sitemap(){
  global $host,$language_prefix,$mapped_modules,$settings,$current_language;
  $xml    = new DomDocument('1.0', 'utf-8');
  $urlset = $xml->appendChild($xml->createElement('urlset'));
  $urlset->setAttribute('xmlns', 'http://www.sitemaps.org/schemas/sitemap/0.9');
  $links  = array($host . $language_prefix);
  $not_entry_modules = array(
    'banners',
    'content',
    'search'
  );
  foreach($mapped_modules as $db => $mapped_module){

    if(!in_array($mapped_module['backend'], $not_entry_modules)){

      $links[] = $host . $language_prefix . $mapped_module['frontend'];
      $cats = select_rows($db . '-categories-' . $current_language, '`disp`=1');

      if(count($cats)){
        foreach($cats as $cat){

          $links[] = $host . $language_prefix . $mapped_module['frontend'] . '/' . $cat['url'];
          $items = select_rows($db . '-' . $current_language, '`disp`=1 AND cid=' . $cat['id']);

          if(count($items)){
            foreach($items as $item){
              if($item['url'] != $settings['home_page']){
                $links[] = $host . $language_prefix . $mapped_module['frontend'] . '/' . $cat['url'] . '/' . $item['url'];
              }
            }
          }
        }
      }
      else{
        $items = select_rows($db . '-' . $current_language, '`disp`=1');
        if(count($items)){
          foreach($items as $item){
            if($item['url'] != $settings['home_page']){
              $links[] = $host . $language_prefix . $mapped_module['frontend'] . '/' . $item['url'];
            }
          }
        }
      }
    }
    if(in_array($mapped_module['backend'], array('content'))){
      $items = select_rows($db . '-' . $current_language, '`disp`=1');
      if(count($items)){
        foreach($items as $item){
          if($item['url'] != $settings['home_page']){
            $links[] = $host . $language_prefix . $item['url'];
          }
        }
      }
    }
  }
  foreach($links as $link){
    $url = $urlset->appendChild($xml->createElement('url'));
    $url->appendChild($xml->createElement('loc', $link));
  }
  header('Content-Type: text/xml; charset=' . CHARSET);
  die($xml->saveXML());
}

function render_captcha(){
  global $host,$language_prefix,$mapped_modules,$settings,$current_language,$_SESSION;


  if(($settings['captcha'] == '1' && !isset($_SESSION['user'])) || $settings['captcha'] == '2'){
    $settings['captcha'] == 1;
    include_once(CLASS_PATH . 'kcaptcha/kcaptcha.php');
    if($query == 'captcha.jpg'){
      $captcha = new KCAPTCHA();
      $_SESSION['captcha_keystring'] = $captcha->getKeyString();
      die();
    }
    if(isset($_POST['captcha'])){
      if(isset($_SESSION['captcha_keystring']) && $_SESSION['captcha_keystring'] === $_POST['captcha']){
        $captcha = true;
      }
      else{
        $captcha = false;
      }
      unset($_SESSION['captcha_keystring']);
    }
  }
  else{
    $captcha = true;
  }
}
?>