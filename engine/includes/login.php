<?php

if(!defined('PARENT_FILE')){
  die('Running banned');
}

session_name('uid');
session_start();

//ulogin auth

if(isset($_POST['token'])){
  $s = file_get_contents('http://ulogin.ru/token.php?token=' . $_POST['token'] . '&host=' . $_SERVER['HTTP_HOST']);
  $data = $s ? json_decode($s, true) : array();
  //проверяем, чтобы в ответе были данные, и не было ошибки
  if(!empty($data) and !isset($data['error'])){
    //проверяем, существует ли такой пользователь в базе
    if($q = select_row('users', false, "mail='" . $data['email'] . "'")){
      $_SESSION['user'] = $q;
    } else{
      $user = array(
        'pass' => 'none',
        'name' => $data['first_name'].' '.$data['last_name'],
        'login'=> $data['email'],
        'mail' => $data['email']);
      $id = insert_row('users', $user );
      $_SESSION['user'] = select_row('users', false, "mail='" . $data['email'] . "'");
      insert_row("profile", array('value'  =>$data['photo_big'],'user_id'=>$id,'field'  =>'picture'));
    }
    if(count_rows('profile',"`user_id`=" . $_SESSION['user']['id']." and `field`='picture'") > 0)
    query("UPDATE `profile` SET `value`=" . $data['photo_big'] . " WHERE `user_id`=" . $_SESSION['user']['id']." and `field`='picture'");
    else
    insert_row("profile", array('value'  =>$data['photo_big'],'user_id'=>$_SESSION['user']['id'],'field'  =>'picture'));

  }else{
  }
}

if(isset($_SESSION['user']['login']) && !empty($_SESSION['user']['login']) && isset($_SESSION['user']['pass']) && !empty($_SESSION['user']['pass'])){
  $user = select_row('users', false, "login='" . $_SESSION['user']['login'] . "'");
  if($_SESSION['user']['pass'] === $user['pass']){
    $_SESSION['user'] = $user;
    $_SESSION['KCFINDER'] = array(
      'disabled' => false,
      'uploadURL'=> $host . 'data/users/' . $_SESSION['user']['group'],
      'uploadDir'=> DATA_PATH . 'users/' . $_SESSION['user']['group']
    );
    $smarty->assign('user', $_SESSION['user']);
    if($profile = load_profile( $_SESSION['user']['id'] )){
      $_SESSION['profile'] = $profile;
    }
  } else{

    session_unset();
    session_destroy();
  }
}

if(isset($_POST['login']) && isset($_POST['pass'])){

  $smarty->assign('login', $_POST['login']);
  $smarty->assign('pass', $_POST['pass']);
  $user = select_row('users', false, "login='" . $_POST['login'] . "'");

  if(empty($user)){
    $error = 'Нет пользователя с таким логином';
  } else{
    if(!empty($_POST['pass']) && crypt($_POST['pass'], $user['pass']) == $user['pass']){
      if($user['disp']){
        $_SESSION['user']['login'] = $user['login'];
        $_SESSION['user']['pass'] = $user['pass'];
        redirect();
      } else{
        $error = 'Пользователь не активен';
      }
    } else{
      $error = 'Неверный пароль';
    }
  }
} else{
  $error = '&nbsp;';
}
$smarty->assign('error', $error);

if(isset($_POST['exit'])){

  session_unset();
  redirect();
}
?>
