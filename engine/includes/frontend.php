<?php

if(!defined('PARENT_FILE')){
  die('Running banned');
}
//die('сайт закрыт на обслуживание');

include_once(INC_PATH . 'core.php');

$language_file = parse_ini_file(LANG_PATH . $current_language . '.ini', true);
$l = $language_file['l'];
$smarty->assign('l', $l);
$smarty->assign('current_language', $current_language);

if($settings['site_close'] && !isset($_SESSION['user'])){
  $smarty->setTemplateDir(ROOT_PATH . 'design/');
  if($smarty->templateExists('site-close.tpl')){
    $smarty->display('site-close.tpl', '', $settings['design']);
  }
  else{
    print_r('Файл не найден  - site-close.tpl');
  }
  die();
}
else{
  if(file_exists(ROOT_PATH . 'design/' . $settings['design'])){
    $smarty->setTemplateDir(ROOT_PATH . 'design/' . $settings['design'] . '/');
    $smarty->assign('design', $host . 'design/' . $settings['design'] . '/style/');
  }
  else{
    die('Папка с шаблоном отсутствует! Выберите в настройках шаблон и сохраните настройки.');
  }
}
// Каптча
render_captcha();
$smarty->assign('settings', $settings);

// home page
if(empty($query)){
  $queries[0] = $settings['home_page'];
}
elseif($queries[0] == $settings['home_page']){
  redirect( - 1);
}
// get list all modules and return variable $mapped_modules
$mapped_modules = get_mapped_modules();

// Генерация sitemap.xml
if($query == 'sitemap.xml'){
  render_sitemap();
}

// search module
$module = search_module($mapped_modules);

// identifity page
$uniqid_pages = array();
$uniqid_pages[] = $module['id'];
if(isset($module['item'])){
  $uniqid_pages[] = $module['id'] . 'i' . $module['item']['id'];
}
elseif(isset($module['category'])){
  $uniqid_pages[] = $module['id'] . 'c' . $module['category']['id'];
}
// breadcrumbs
render_breadcrumbs();
set_seo($settings['sitename'], $settings['title'], $settings['desc'], $settings['key']);
// run module if found

if($module_path = load_module($smarty,$module,MODULES_PATH)){
  include_once($module_path);
}
else{
  error_404();
}

// blocks
$blocks = select_rows('blocks-' . $current_language, '`disp`=1', '`sort`, `id` DESC');
if(count($blocks)){
  foreach($blocks as $block){
    foreach($uniqid_pages as $uniqid_page){
      if($block['show'] == '0'){
        $continue = false;
        break;
      }
      elseif($block['show'] == '1' && in_array($uniqid_page, $block['to_show'])){
        $continue = false;
        break;
      }
      elseif($block['show'] == '2' && !in_array($uniqid_page, $block['to_show'])){
        if(!$continue){
          $continue = false;
        }
      }
      else{
        $continue = true;
      }
    }
    if($continue){
      continue;
    }
    if($block['type'] == 1){
      $block['full_cont'] = get_block_in_file($block['file']);
    }
    if($block['pos'] == 0){
      $smarty->append('l_blocks', $block);
    }
    elseif($block['pos'] == 1){
      $smarty->append('t_blocks', $block);
    }
    elseif($block['pos'] == 2){
      $smarty->append('b_blocks', $block);
    }
    elseif($block['pos'] == 3){
      $smarty->append('r_blocks', $block);
    }
    elseif($block['pos'] == 4){
      if($block['disp_name']){
        $smarty->assign('n' . $block['id'], $block['name']);
      }
      $smarty->assign('b' . $block['id'], $block['full_cont']);
    }
  }
}

// menu

if($menus = select_rows('menu-' . $current_language, 'disp=1', 'id DESC')){
  foreach($menus as $menu){
    if($menu['menu']){
      foreach($menu['menu'] as $key => $m){
        $menu['menu'][$key]['level'] = recursion_level($menu['menu'], $m['parent']);
        $active = false;
        if(!parse_url($m['link'], PHP_URL_SCHEME) && $m['link'][0] != '#'){
          if($m['link'] == $query || $m['link'] == $queries[0]){
            $active = true;
            if($m['parent']){
              foreach($menu['menu'] as $i => $value){
                if($value['id'] == $m['parent']){
                  $menu['menu'][$i]['active'] = true;
                }
              }
            }
          }
          $menu['menu'][$key]['link'] = $host . $language_prefix . $m['link'];
        }
        else{
          if($m['link'] == $host . $query){
            $active = true;
          }
        }
        $menu['menu'][$key]['active'] = $active;
      }
    }
    $smarty->assign('m' . $menu['id'], $menu['menu']);
  }
}

if(empty($settings['title_format'])){
  $title = $seo['title'];
}
else{
  $title_tags = array(
    'title'   => $seo['title'],
    'sitename'=> $settings['sitename']
  );
  $title = $settings['title_format'];
  foreach($title_tags as $tag => $value){
    if(strpos($title, '%' . $tag . '%') !== false){
      $title = str_ireplace('%' . $tag . '%', $value, $title);
    }
  }
}

$smarty->assign('title', $title);
$smarty->assign('desc', $seo['desc']);
$smarty->assign('key', $seo['key']);
if(isset($_SESSION['profile'])) $smarty->assign('profile', $_SESSION['profile']);
if($smarty->templateExists('index.tpl')){
  $smarty->display('index.tpl', '', $settings['design']);
}
else{
  die('Файл не найден - index.tpl');
}
?>