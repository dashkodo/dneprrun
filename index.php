<?php
//ini_set('display_errors',true);
//error_reporting(E_ALL);
// Считываем текущее время
$start_time = microtime(true);
// Установка вывода ошибок
error_reporting(0);
// Установка констант
define('PARENT_FILE', true);
// System charset
define('CHARSET', 'utf-8');
// Charset database
define('DB_CHARSET', 'utf8');
// Paths
define('ENGINE', 'engine/');
define('ROOT_PATH', dirname(__FILE__) . '/');
define('DATA_PATH', ROOT_PATH . 'data/');

define('ENGINE_PATH', ROOT_PATH . ENGINE);

define('CASHE_PATH', ENGINE_PATH . 'cache/');
define('CLASS_PATH', ENGINE_PATH . 'class/');
define('CONF_PATH', ENGINE_PATH . 'config/');
define('FUNC_PATH', ENGINE_PATH . 'functions/');
define('INC_PATH', ENGINE_PATH . 'includes/');
define('LANG_PATH', ENGINE_PATH . 'languages/');
define('MODULES_PATH', ENGINE_PATH . 'modules/');
define('SMARTY_PATH', ENGINE_PATH . 'smarty/');
define('TEMP_PATH', ENGINE_PATH . 'temp/');
define('TPL_PATH_ADMIN', ENGINE_PATH . 'design/');

include_once(FUNC_PATH . 'functions.php');
// Опредиление $host (сайт может находиться в директории)
$protocol = 'http://';
$domain = $_SERVER['SERVER_NAME'];
$script_dirname = dirname($_SERVER['SCRIPT_NAME']);
$path = ($script_dirname == '/') ? $script_dirname : $script_dirname . '/';
$host = $protocol . $domain . $path;
// Включаем zlib сжатие
if (extension_loaded('zlib')) {
	ob_start('ob_gzhandler');
}
// Устанавливаем мультибайтовую кодировку
mb_internal_encoding(CHARSET);
// Отправка хедеров
header('Content-Type: text/html; charset=' . CHARSET);

// Экранирование запросов
if (get_magic_quotes_gpc()) {
	strips($_GET);
	strips($_POST);
	strips($_COOKIE);
	strips($_REQUEST);
	strips($_SESSION);
}
// Разбор запроса
if (isset($_SERVER['REQUEST_URI'])) {
	$query = urldecode($_SERVER['REQUEST_URI']);
	$query = strtok($query, '?');
	if ($script_dirname != '/') {
		$query = substr_replace($query, '', 0, strlen($script_dirname));
	}
	$query = ltrim($query, '/');
	if (substr($query, -1) == '/') {
		redirect($host . rtrim($query, '/'));
	}
	$queries = explode('/', $query);
}

// Подкльчаем базу данных
include_once(CONF_PATH . 'connect.php');
if (defined('MYSQL_DB_HOST')) {
	//MySQL
	include_once(FUNC_PATH . 'mysql.php');
	if (!mysql_connect(MYSQL_DB_HOST, MYSQL_DB_USER, MYSQL_DB_PASS)) {
		die('Ошибка подключения к базе данных');
	}
	mysql_select_db(MYSQL_DB_NAME);
	mysql_set_charset(DB_CHARSET);
} elseif (defined('SQLITE_PATH')) {
	//SQLite
	include_once(FUNC_PATH . 'sqlite.php');
}

// Извликаем название всех таблиц
$show_tables = array();
$q = query("SHOW TABLES;");
while ($row = fetch_assoc($q)) {
	$show_tables[] = reset($row);
}

// Настройки
$rows = select_rows('settings');
$settings = array();

foreach ($rows as $row) {
	$settings[$row['name']] = $row['value'];
}

// Установка вывода ошибок при дебаге
if ($settings['debag']) {
	error_reporting(E_ALL ^ E_NOTICE);
}
// Выключение мультиязычности если в настройках один язык
if (count($settings['languages']) === 1) {
	$settings['languages'] = array();
}
// Установка языка и префикса ссылок
$current_language = '';
$language_prefix = setup_language_prefix($settings['default_language'], $current_language);

$language_settings = array(
	'sitename',
	'title',
	'desc',
	'key'
);
foreach ($language_settings as $language_setting) {
	if (isset($settings[$language_setting . '_' . $current_language])) {
		$settings[$language_setting] = $settings[$language_setting . '_' . $current_language];
	} else {
		$settings[$language_setting] = '';
	}
}
// Установка временной зоны
if ($timezone_name = timezone_name_from_abbr('', 3600 * (int) $settings['time_zone'], 0)) {
	date_default_timezone_set($timezone_name);
}

// Подключение смарти
include_once(SMARTY_PATH . 'Smarty.class.php');
$smarty = new Smarty();
$smarty->setCompileDir(CASHE_PATH);
$smarty->setCacheDir(CASHE_PATH);
$smarty->setConfigDir(CONF_PATH);

if ($settings['debag']) {
	$smarty->debugging = true;
}
// корневой узел сайта
$smarty->assign('domain', $domain);
// путь к сайту (папка)
$smarty->assign('path', $path);
// хост сайта
$smarty->assign('host', $host);
// канонический запрос
$smarty->assign('query', $query);
// разбитый на элементы запрос
$smarty->assign('queries', $queries);
// Установка префикса языка
$smarty->assign('language_prefix', $language_prefix);
// Год сервера
$smarty->assign('year', date('Y'));
// Случайное число
$rand = mt_rand(1000, 9999);
$smarty->assign('rand', $rand);


// Авторизация
include_once(INC_PATH . 'login.php');

// Запуск фронтенда или бекенда
if ($queries[0] == 'api') {
	include_once(INC_PATH . 'api.php');
} else if ($queries[0] == 'admin') {
	include_once(INC_PATH . 'backend.php');
} else {
  include_once(INC_PATH . 'frontend.php');
}


// End compressing
if (extension_loaded('zlib')) {
	ob_flush();
}
// Operating time
if ($queries[0] != 'api') {
printf("\r\n<!-- %.3f -->", microtime(true) - $start_time);
}
?>